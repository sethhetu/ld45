package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text for the character
class DescWinBall extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Current ball
	private var obj1:FlxSprite;
	private var obj2:FlxSprite;

	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines(ballName:String):Array<String>
	{
		if (ballName == "book") {
			var res = character.goal_descriptions.copy();
			res.push("");
			res.push("Free this book from nearby Actions.");
			return res;
		} else if (ballName == 'cake') {
			var res = character.goal_descriptions.copy();
			res.push("");
			res.push("Free this cupcake from nearby Actions.");
			return res;
		} else if (ballName == 'event') {
			var res = character.event_descriptions.copy();
			res.push("");
			res.push("Free this event from nearby Actions.");
			return res;
		} else if (ballName == "coin") {
			var res = character.goal_descriptions.copy();
			res.push("");
			res.push("Collect this coin with any Action.");
			return res;
		} else {
			var res = character.ball_descriptions[ballName].copy();
			res.push("");
			res.push("Connect 3 of this Action to form a Match.");
			return res;
		}
	}


	public function new(character:Character)
	{
		super();
		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		this.add(title);

		obj1 = new FlxSprite();
		obj1.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj1.animation);
		obj1.x = fullWidth/2 - 200;
		obj1.y = 16;
		this.add(obj1);

		obj2 = new FlxSprite();
		obj2.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj2.animation);
		obj2.x = fullWidth/2 + 200;
		obj2.y = 16;
		this.add(obj2);

		/*prop1 = new FlxSprite();
		prop1.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(prop1.animation);
		prop1.x = 72;
		prop1.y = 20;
		this.add(prop1);*/

		var myX = 60;
		var myY = 50;
		textLines = new Array<FlxText>();
		for (line in get_lines('red')) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}
	}

	public function setBall(ballName:String):Void
	{
		if (ballName == "book") {
			title.text = "Goal: School Year";
		} else if (ballName == "coin") {
			title.text = "Goal: Pay Bills";
		} else if (ballName == 'cake') {
			title.text = "Goal: The Holidays";
		} else if (ballName == "event") {
			title.text = "Random Event";
		} else {
			title.text = "Action: " + character.ballNames[ballName];
		}

		// Note: This is probably the worst way to do this...
		var throwAway = new FlxText();
		throwAway.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
		throwAway.text = title.text;
		var offsetX = throwAway.width/2 + 16;

		// Update Icons
		for (obj in [obj1,obj2]) {
			if (ballName == "book" || ballName == 'event' || ballName == 'coin' || ballName == 'cake') {
				obj.animation.play(ballName);
			} else {
				obj.animation.play(ballName + "_static");
			}

			obj.x = Math.floor(fullWidth/2 - offsetX);
			offsetX *= -1;
			offsetX -= 9; // Not sure why...
		}

		// Update text
		var id = 0;
		for (line in get_lines(ballName)) {
			textLines[id].text = line;
			id += 1;
		}
	}




}