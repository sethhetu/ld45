package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows our tutorial
class PopupWinChapterWon extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 404;

	// BG is shown as a child of this window
	private var bgImg:FlxSprite;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	private var face:FlxSprite;

	// Current ball
	//private var obj1:FlxSprite;
	//private var obj2:FlxSprite;


	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines(loss:Bool):Array<String>
	{
		if (loss) {
			return character.loss_lines;
		} else {
			return character.victory_lines;
		}
	}


	public function new(character:Character)
	{
		super();

		this.character = character;

        bgImg = new FlxSprite();
        bgImg.loadGraphic(AssetPaths.content_window__png);
        bgImg.origin.set(0,0);
        this.add(bgImg);

		title = new FlxText(0,20,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Chapter Complete!";
		this.add(title);

		var myX = fullWidth/2 - (4*96+3*2)/2 + 22;
		var myY = 200 - 60 + 4 - 72;
		face = new FlxSprite();
	    face.loadGraphic(AssetPaths.faces__png, true, 96, 96);
        face.animation.add("char1_neutral", [27], 5, false);
        face.animation.add("char1_older_neutral", [25], 5, false);
        face.animation.add("char2_neutral", [24], 5, false);
        face.animation.add("char3_neutral", [28], 5, false);
        face.animation.add("ambulance", [0], 5, false);
        face.animation.play(character.face_anim);
	    face.x = myX;
	    face.y = myY;
	    this.add(face);


/*		obj1 = new FlxSprite();
		obj1.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj1.animation);
		obj1.x = fullWidth/2 - 200;
		obj1.y = 16;
		this.add(obj1);

		obj2 = new FlxSprite();
		obj2.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj2.animation);
		obj2.x = fullWidth/2 + 200;
		obj2.y = 16;
		this.add(obj2);

*/

		myX = 42;
		myY = 62;
		var id = 0;
		textLines = new Array<FlxText>();
		for (line in get_lines(false)) {
			var xOff = id<6 ? 93 : 0;
			var lineTxt = new FlxText(myX+xOff,myY,fullWidth-myX-xOff);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
			id += 1;
		}
	}

	public function makeLoss():Void
	{
		title.text = "Suicide Attempt";

		face.animation.play("ambulance");

		var id = 0;
		for (line in get_lines(true)) {
			textLines[id].text = line;
			id += 1;
		}
	}

	// NOTE: This only works if we call it after new()
	/*
	public function touch():Void
	{
		// Note: This is probably the worst way to do this...
		var throwAway = new FlxText(0,100);
		throwAway.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
		throwAway.text = title.text;
		var offsetX = throwAway.width/2 + 16;

		// Update Icons
		for (obj in [obj1,obj2]) {
			obj.animation.play(character.goal_anim);

			obj.x = Math.floor(fullWidth/2 - offsetX);
			offsetX *= -1;
			offsetX -= 9; // Not sure why...
		}
	}
	*/

}
