package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.animation.FlxAnimationController;


class Ball extends FlxSprite
{
	var typ:String;
	var dying_anim:String;

	public var dying:Bool;
	public var maxSpd:Float = 400;

	public var overlapCurrFrame:Bool;
	public var overlapStreak:Int;
	public var bounceCount:Int;

	// Can we grab this?
	public var canGrab:Bool = false;

	// Did you just collide?
	public var justCollided:Bool = false;
	public var justCollectedCoin:Bool = false;

	// Helper: Have we checked this ball yet in a given search?
	public var checked:Bool = false;

	// Force color discrepancy (for, e.g., color blind)
	public static var ColorDiscrepancy = false;

	private static function tid(tx, ty)
	{
		return ty*20+tx;
	}

	public function new()
	{
		super();
		
		loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(animation);
	}

	public static function AddAnimations(animation:FlxAnimationController)
	{
		animation.add("none", [tid(0,0)], 10);

		if (ColorDiscrepancy) {
			animation.add("red", [tid(7,28),tid(8,28)], 5);
			animation.add("orange", [tid(7,29),tid(8,29)], 5);
			animation.add("yellow", [tid(7,30),tid(8,30)], 5);
			animation.add("green", [tid(7,31),tid(8,31)], 5);
			animation.add("blue", [tid(7,32),tid(8,32)], 5);
			animation.add("purple", [tid(7,33),tid(8,33)], 5);
			animation.add("pink", [tid(7,34),tid(8,34)], 5);
		} else {
			animation.add("red", [tid(10,28),tid(11,28)], 5);
			animation.add("orange", [tid(10,29),tid(11,29)], 5);
			animation.add("yellow", [tid(10,30),tid(11,30)], 5);
			animation.add("green", [tid(13,28),tid(14,28)], 5);
			animation.add("blue", [tid(13,29),tid(14,29)], 5);
			animation.add("purple", [tid(13,30),tid(14,30)], 5);
			animation.add("pink", [tid(16,28),tid(17,28)], 5);
		}

		animation.add("red_dying", [tid(5,37),tid(6,37),tid(7,37),tid(8,37),tid(9,37),tid(9,37),tid(9,37)], 8, false);
		animation.add("orange_dying", [tid(5,38),tid(6,38),tid(7,38),tid(8,38),tid(9,38),tid(9,38),tid(9,38)], 8, false);
		animation.add("yellow_dying", [tid(10,36),tid(11,36),tid(12,36),tid(13,36),tid(14,36),tid(14,36),tid(14,36)], 8, false);
		animation.add("green_dying", [tid(10,37),tid(11,37),tid(12,37),tid(13,37),tid(14,37),tid(14,37),tid(14,37)], 8, false);
		animation.add("blue_dying", [tid(10,38),tid(11,38),tid(12,38),tid(13,38),tid(14,38),tid(14,38),tid(14,38)], 8, false);
		animation.add("purple_dying", [tid(15,36),tid(16,36),tid(17,36),tid(18,36),tid(19,36),tid(19,36),tid(19,36)], 8, false);
		animation.add("pink_dying", [tid(15,37),tid(16,37),tid(17,37),tid(18,37),tid(19,37),tid(19,37),tid(19,37)], 8, false);

		// Non-animating variants for certain menu items
		if (ColorDiscrepancy) {
			animation.add("red_static", [tid(7,28)], 5, false);
			animation.add("orange_static", [tid(7,29)], 5, false);
			animation.add("yellow_static", [tid(7,30)], 5, false);
			animation.add("green_static", [tid(7,31)], 5, false);
			animation.add("blue_static", [tid(7,32)], 5, false);
			animation.add("purple_static", [tid(7,33)], 5, false);
			animation.add("pink_static", [tid(7,34)], 5, false);
		} else {
			animation.add("red_static", [tid(10,28)], 5, false);
			animation.add("orange_static", [tid(10,29)], 5, false);
			animation.add("yellow_static", [tid(10,30)], 5, false);
			animation.add("green_static", [tid(13,28)], 5, false);
			animation.add("blue_static", [tid(13,29)], 5, false);
			animation.add("purple_static", [tid(13,30)], 5, false);
			animation.add("pink_static", [tid(16,28)], 5, false);
		}

		// These act sort of like dots
		animation.add("heart", [1], 10);
		animation.add("heart_static", [1], 5, false);
		animation.add("heart_dying", [tid(5,37),tid(6,37),tid(7,37),tid(8,37),tid(9,37),tid(9,37),tid(9,37)], 8, false);
		
		animation.add("coin", [tid(1,5),tid(2,5),tid(3,5),tid(4,5),tid(5,5),tid(6,5),tid(7,5),tid(8,5)], 10);
		animation.add("coin_static", [tid(1,5)], 5, false);
		animation.add("coin_dying", [tid(1,26),tid(2,26),tid(3,26),tid(4,26),tid(5,26),tid(5,26),tid(5,26)], 8, false);

		// Misc icons with no "dying" equivalent
		animation.add("magnifying", [tid(15,22)], 5, false);
		animation.add("flower", [tid(10,26)], 5, false);
		animation.add("rainbow_star", [tid(11,11)], 5, false);
		animation.add("energy", [tid(13,11)], 5, false);
		animation.add("book", [tid(12,17)], 5, false);
		animation.add("event", [tid(16,22)], 5, false);
		animation.add("cancel", [tid(0,0)], 5, false);

		// More stuff
		animation.add("talent", [tid(2,19)], 5, false);
		animation.add("beer", [tid(18,24)], 5, false);
		animation.add("coffee", [tid(18,11)], 5, false);
		animation.add("cake", [tid(7,1)], 5, false);
		animation.add("purple_flower", [tid(16,26)], 5, false);
		animation.add("sad", [tid(9,1)], 5, false);
		animation.add("watermelon", [tid(10,1)], 5, false);
		animation.add("choccake", [10], 5, false);
		
		
		// Grr...
		animation.add("book0", [2], 5, false);
		animation.add("book1", [3], 5, false);
		animation.add("book2", [4], 5, false);
		animation.add("book3", [5], 5, false);
		animation.add("book4", [6], 5, false);

		// Same
		animation.add("coin0", [tid(1,4),tid(2,4),tid(3,4),tid(4,4),tid(5,4),tid(6,4),tid(7,4),tid(8,4)], 10);
		for (i in 1...13) {
			animation.add("coin"+i, [tid(1,5+i),tid(2,5+i),tid(3,5+i),tid(4,5+i),tid(5,5+i),tid(6,5+i),tid(7,5+i),tid(8,5+i)], 10);	
		}

		// Same
		animation.add("cake0", [7], 5, false);
		animation.add("cake1", [8], 5, false);
		animation.add("cake2", [9], 5, false);
	}

	// Special case (bad organization)
	public static function AddPointAnimations(animation:FlxAnimationController)
	{
		animation.add("red", [0], 5, false);
		animation.add("orange", [1], 5, false);
		animation.add("yellow", [2], 5, false);
		animation.add("green", [3], 5, false);
		animation.add("blue", [4], 5, false);
		animation.add("purple", [5], 5, false);
		animation.add("pink", [6], 5, false);
		animation.add("white", [7], 5, false);
	}
	
	public function init(xPos:Int, yPos:Int, typ:String):Ball
	{
		reset(xPos, yPos);
		this.dying = false;

		this.typ = typ;
		this.dying_anim = typ + '_dying';

		// May not matter, but...
		if (this.isCoin()) {
			this.immovable = true;
		}

		this.health = 1;

		this.overlapCurrFrame = false;
		this.overlapStreak = 0;
		this.bounceCount = 0;

		return this;
	}

	public function getType():String
	{
		return typ;
	}

	public function setType(typ:String):Void
	{
		this.typ = typ;
		this.animation.play(typ);
	}
	
	override public function update(elapsed:Float):Void
	{
		if (this.dying) {
			if (animation.finished) {
				this.kill();
			}
		} else {
			// Play our main anim
			animation.play(typ);
		}

		// Then call FlxSprite's update() function, to automate
		// our motion and animation and stuff.
		super.update(elapsed);
	}

	
	override public function hurt(dmg:Float):Void
	{
		// Ignore when dying
		if (this.dying) { return; }

		// TODO: SFX
		// Mirror functionality, but send off to dying anim.
		health = health - dmg;
		if (health <= 0) {
			this.dying = true;
			this.velocity.set(0,0);
			animation.play(dying_anim);
		}
	}

	public function isCoin():Bool
	{
		return this.typ == 'coin';
	}

	// Try to collect this object as a coin (on collision). 
	// Returns true if this is the case.
	public function collectCoin():Bool
	{
		if (this.isCoin()) {
			// TODO: SFX
			this.justCollectedCoin = true;
			this.hurt(1);
			return true;
		}
		return false;
	}
	
}