package;


// Class holding methods and properties about a given character.
// There are static methods for constructing each of the pre-made characters.
class Character
{
	// Name, age, pronoun
	public var name:String = "Chara";
	public var age:Int = 20;
	public var pnhe:String = "he";
	public var pnHe:String = "He";
	public var pnhis:String = "his";
	public var pnHis:String = "His";
	public var pnhim:String = "him";
	public var pnHim:String = "Him";

	// What chapter is it for this character?
	public var chapter:Int = 0;

	// Music to play in the BG
	public var level_music:String = "N/A";

	// Intro lines for this chapter:
	public var intro_lines:Array<String> = [""];

	// BG to show behind the scene for this chapter
	public var scenic_png:String = "N/A";

	// Short bio for this character.
	public var short_desc:Array<String> = ["This is a character bio."];

	// Face settings
	public var face_anim:String = "TODO";

	// Goal for this chapter
	public var goal:String = "My Goal";
	public var goal_anim:String = "TODO";
	public var goal_description:Array<String> = ["TODO"];

	// Time duration for this character's goal; e.g., "the vaction"
	public var duration:String = "the year";

	// How many turns before we drop a new line
	public var ball_advance_modulo:Int = 1;

	// Odds (of 100) of re-rolling when we get the same ball in our neighborhood, and
	//   how many times to re-roll
	public var reRollChance:Int = 50;
	public var reRollTimes:Int = 1;

	// Lifelines
	//    0 = used
	//    1 = unused
	//    2 = locked
	public var lifelines:Array<Int> = [ 1,1,1,1 ];

	// Odds of each ball. These are added up; no need to make them even
	public var ballOdds:Map<String,Int> = [
		"red" => 99,
		"green" => 101,
		"pink" => 22,
		"yellow" => 5,
	];

	// How far (y axis) to offset the intro objs
	public var intro_obj_offset:Int = 0;

	// Short name of each color.
	public var ballNames:Map<String,String> = [
		"red" => "Obligation",
		"green" => "Social",
		"pink" => "Relationship",
		"yellow" => "Casual",
	];

	// Full help descriptions for each ball
	public var ball_descriptions:Map<String,Array<String>> = [
		"red" => ["Lines"],
		"green" => ["Lines"],
		"pink" => ["Lines"],
		"yellow" => ["Lines"],
	];

	// Full help description for the goal
	public var goal_descriptions:Array<String> = ["Lines"];

	// How often does a "goal" item come up in the search?
	public var goal_frequency:Int = 100;

	// If non-zero, we swap adding + subtracting this amount to the goal_frequency
	public var modulate_goal:Int = 0;

	// Where exactly do our bad events occur?
	// Note: Second int doesn't matter
	public var bad_events:Map<Int,Int> = new Map<Int,Int>();

	// How many events have we experienced so far?
	public var event_count:Int = 0;

	// Therapist equivalent
	public var therapist:String = "therapist";

	// What we call these
	public var event_titles:Array<String> = ["Title"];
	public var event_stories:Array<Array<String>> = [["Story"]];

	public var event_descriptions:Array<String> = ["Lines"];

	// For now, we can only have "good" or "bad" events
	public var event_is_bad:Array<Bool> = [true];

	// How many "goal points" do we have?
	public var goal_count:Int = 0;

	// How many "goal points" do we need?
	public var goal_requirement:Int = 1;

	// Lines for when we reach the goal.
	public var victory_lines:Array<String> = ["Victory"];
	public var loss_lines:Array<String> = ["Loss"];

	// Short description of negative effects (up to 2 per line) of each ball color
	public var ballEffects:Map<String,Array<String>> = [
		"red" => ["No Effect"],
		"green" => ["No Effect"],
		"pink" => ["No Effect"],
		"yellow" => ["No Effect"],
	];
	
	// The three emotional states of the character we are tracking.
	public var emotions:Array<String> = [
		"Bad 1",
		"Bad 2",
		"Bad 3",
	];

	// The value of each emotion, from 0 to 200
	public var emotionalState:Array<Int> = [0,0,0];

	// Starting risk level, out of 300
	public var riskLevel:Int = 0;


	// Basic emotional response to each color 
	//   (before bonuses)
	public var basic_response:Map<String,Array<TurnEffect>> = [
		"red" => [],
		"green" => [],
		"pink" => [],
		"yellow" => [],
	];

	// Not the best way to handle this, but this makes emotions go down by 1 every turn
	public var hopeful:Bool = false;

	// Which character to play as after this?
	// NOTE: We use ints for now...
	public var nextChara:Int = 0;


	// The character's 3 properties and their associated anims (and bonuses)
	public var properties:Array<String> = ["Prop1", "Prop2", "Prop3"];
	public var property_anims:Array<String> = ["N/A","N/A","N/A"];
	public var property_bonuses:Array<Int> = [0,0,0];

	// Multiline descriptions of each property
	public var property_descriptions:Array<Array<String>> = [
		["Lines"],
		["Lines"],
		["Lines"],
	];

	// Show these balls in slot 1?
	public var property_balls:Array<String> = [
		"","",""
	];

	// Who is telling this person to seek help?
	// And what are they recommending?
	// TODO: We might want to do this dynamically later.
	public var who_suggests_help:String = "And then:";
	public var match_recommends:String = "";
	public var avoid_recommends:String = "";


	public var suicide_risk:String = "Low";


	public function new() {}


	// Some handy helper functions that just use the data type
	public function get_suicide_risk():String
	{
		// TODO: Later make this better.
		return suicide_risk;
	}

	public function get_most_negative_emotion():String
	{
		var lowest = 0;
		for (i in 1...3) {
			if (emotionalState[i] > emotionalState[lowest]) {
				lowest = i;
			}
		}
		return emotions[lowest];
	}



}
