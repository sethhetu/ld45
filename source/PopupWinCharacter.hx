package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Current character's story
class PopupWinCharacter extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 404;

	private var character:Character;

	// BG is shown as a child of this window
	private var bgImg:FlxSprite;

	// Title of this window
	private var title:FlxText;

	// Some character faces
	private var face:FlxSprite;

	// Helper objects
	private var objs:Array<FlxSprite>;


	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines(propId:Int):Array<String>
	{
		return character.intro_lines;
	}


	public function new(character:Character)
	{
		super();
		this.character = character;

        bgImg = new FlxSprite();
        if (character.name == "DONE") {
        	bgImg.loadGraphic(AssetPaths.level_select_window_done__png);
        } else {
        	bgImg.loadGraphic(AssetPaths.level_select_window__png);
        }
        bgImg.origin.set(0,0);
        this.add(bgImg);

		title = new FlxText(0,20,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = 'Chapter ${character.chapter}: ${character.name}';
		this.add(title);

		if (character.name == "DONE") {
			title.text = "The End";
		}

		var myX = fullWidth/2 - (4*96+3*2)/2 + 22;
		var myY = 200 - 60 + 4 - 72;
		face = new FlxSprite();
	    face.loadGraphic(AssetPaths.faces__png, true, 96, 96);
        face.animation.add("char1_neutral", [27], 5, false);
        face.animation.add("char1_older_neutral", [25], 5, false);
        face.animation.add("char2_neutral", [24], 5, false);
        face.animation.add("char3_neutral", [28], 5, false);
        if (character.name == "DONE") {
        	face.animation.play('char3_neutral');
        } else {
	        face.animation.play(character.face_anim);
    	}
	    face.x = myX;
	    face.y = myY;
	    this.add(face);

	    var mX = 50 + 35 + 40;
	    var mY = 382 + 11 + 5;
	    if (character.name == "DONE") {
	    	mY -= (300 - 80);
	    }
	    objs = new Array<FlxSprite>();
	    for (i in 0...4) {
			var obj1 = new FlxSprite();
			obj1.loadGraphic(AssetPaths.items__png, true, 32, 32);
			Ball.AddAnimations(obj1.animation);
			if (character.name == "DONE") {
				obj1.animation.play((i%2==0) ? 'choccake' : 'watermelon');
			} else {
				obj1.animation.play(character.goal_anim);
			}
			obj1.x = mX;
			obj1.y = mY + 2 + character.intro_obj_offset;
			mX += 48;
			this.add(obj1);
			objs.push(obj1);
		}

/*
		obj2 = new FlxSprite();
		obj2.loadGraphic(AssetPaths.win_obj_help__png);
		Ball.AddAnimations(obj2.animation);
		obj2.x = 34 + 24;
		obj2.y = 616;
		this.add(obj2);
		*/


		myX = 42;
		myY = 62;
		var id = 0;
		textLines = new Array<FlxText>();
		for (line in get_lines(0)) {
			var xOff = id<6 ? 93 : 0;
			var lineTxt = new FlxText(myX+xOff,myY,fullWidth-myX-xOff);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
			id += 1;
		}
	}

	// NOTE: This only works if we call it after new()
	/*
	public function touch():Void
	{
		// Note: This is probably the worst way to do this...
		var throwAway = new FlxText(0,100);
		throwAway.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
		throwAway.text = title.text;
		var offsetX = throwAway.width/2 + 16;

		// Update Icons
		for (obj in [obj1,obj2]) {
			obj.animation.play(character.goal_anim);

			obj.x = Math.floor(fullWidth/2 - offsetX);
			offsetX *= -1;
			offsetX -= 9; // Not sure why...
		}
	}
	*/

}
