package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text for the character
class DescWinGoal extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Current ball
	private var obj1:FlxSprite;
	private var obj2:FlxSprite;

	// Effects (property balls)
	//private var prop1:FlxSprite;

	// Which emotion are we shoing?
	//private var emotionBarId:Int = 0;

	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines(propId:Int):Array<String>
	{
		var res = character.goal_description;
		return res;
	}


	public function new(character:Character)
	{
		super();
		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Goal: " + character.goal;
		this.add(title);

		obj1 = new FlxSprite();
		obj1.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj1.animation);
		obj1.x = fullWidth/2 - 200;
		obj1.y = 16;
		this.add(obj1);

		obj2 = new FlxSprite();
		obj2.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj2.animation);
		obj2.x = fullWidth/2 + 200;
		obj2.y = 16;
		this.add(obj2);

		/*prop1 = new FlxSprite();
		prop1.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(prop1.animation);
		prop1.x = 72;
		prop1.y = 20;
		this.add(prop1);*/

		var myX = 60;
		var myY = 50;
		textLines = new Array<FlxText>();
		for (line in get_lines(0)) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}
	}

	// NOTE: This only works if we call it after new()
	public function touch():Void
	{
		// Note: This is probably the worst way to do this...
		var throwAway = new FlxText(0,100);
		throwAway.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
		throwAway.text = title.text;
		var offsetX = throwAway.width/2 + 16;

		// Update Icons
		for (obj in [obj1,obj2]) {
			obj.animation.play(character.goal_anim);

			obj.x = Math.floor(fullWidth/2 - offsetX);
			offsetX *= -1;
			offsetX -= 9; // Not sure why...
		}
	}


}
