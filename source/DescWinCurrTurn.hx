package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text for the current turn button.
class DescWinCurrTurn extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Current ball color
	private var dispObj:FlxSprite;

	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines(nextColor:String):Array<String>
	{
		if (nextColor == "heart") {
			return [
				"The next turn represents a lifeline.",
				"This will always happen when",
				"you cross a risk threshold, if",
				"you have an unlocked lifeline available.",
			];
		}


		var explainLines:Array<String> = character.ballEffects[nextColor];

		var res = [
			'Clicking the Action button will start your turn.',
			'${character.name}\'s next life event is:',
			'This is a "${character.ballNames[nextColor]}" action.',
			'If three or more balls match, they will be removed.',
			'For ${character.name}, this will have the following effect:',
		];

		for (exLine in explainLines) {
			res.push("    " + exLine);
		}
		res.push('Try to keep ${character.name}\'s mental state balanced');
		res.push('as you work towards ${character.pnhis} level goal.');
		return res;
	}

	// This makes sense here as an anti-frustration feature.
	private var lastLine = "Click \"Cancel\" to exit Inspection mode.";


	public function new(character:Character)
	{
		super();
		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Action: Take Turn";
		this.add(title);

		dispObj = new FlxSprite();
		dispObj.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(dispObj.animation);
		dispObj.x = fullWidth - dispObj.width - 220 + 70;
		dispObj.y = 50 + 20*2 - 15;
		this.add(dispObj);

		var myX = 60;
		var myY = 50;
		textLines = new Array<FlxText>();
		for (line in get_lines('red')) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}

		// Last line is centered
		var lineTxt = new FlxText(0,50+20*10,fullWidth);
		lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		lineTxt.text = lastLine;
		this.add(lineTxt);
	}


	public function setBallColor(clr:String) {
		dispObj.animation.play(clr);

		var i:Int = 0;
		for (line in get_lines(clr)) {
			textLines[i].text = line;
			i += 1;
		}
		while (i < textLines.length) {
			textLines[i].text = "";
			i += 1;
		}
	}

	
}