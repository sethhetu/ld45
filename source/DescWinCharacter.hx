package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text for the character
class DescWinCharacter extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Current goal
	private var matchObj:FlxSprite;
	private var avoidObj:FlxSprite;
	private var goalObj:FlxSprite;

	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines():Array<String>
	{
		var res = character.short_desc.copy();
		res.push('Right now, ${character.name}\'s suicide risk is: ${character.get_suicide_risk()}');
		res.push('${character.pnHis} most negative emotion is: ${character.get_most_negative_emotion()}');
		res.push(character.who_suggests_help);
		res.push('        Match         Avoid         Goal');

		return res;
	}


	public function new(character:Character)
	{
		super();
		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Character: " + character.name;
		this.add(title);

		var objY = 50 + 20*2 - 15 + 142;
		matchObj = new FlxSprite();
		matchObj.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(matchObj.animation);
		matchObj.x = fullWidth - matchObj.width - 228 + 100 - 105 - 111;
		matchObj.y = objY;
		this.add(matchObj);

		avoidObj = new FlxSprite();
		avoidObj.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(avoidObj.animation);
		avoidObj.x = fullWidth - avoidObj.width - 228 + 100 - 105;
		avoidObj.y = objY;
		this.add(avoidObj);

		goalObj = new FlxSprite();
		goalObj.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(goalObj.animation);
		goalObj.x = fullWidth - goalObj.width - 228 + 100;
		goalObj.y = objY;
		this.add(goalObj);
		goalObj.animation.play(character.goal_anim);

		var myX = 60;
		var myY = 50;
		textLines = new Array<FlxText>();
		for (line in get_lines()) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}
	}

	public function touch():Void
	{
		var matchStr = character.match_recommends;
		if (matchStr.length == 0) {
			matchObj.y = -999;
		} else {
			matchObj.animation.play(matchStr);
			matchObj.y = goalObj.y;
		}

		var avoidStr = character.avoid_recommends;
		if (avoidStr.length == 0) {
			avoidObj.y = -999;
		} else {
			avoidObj.animation.play(avoidStr);
			avoidObj.y = goalObj.y;
		}
	}


	
}