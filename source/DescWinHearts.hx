package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text for the character
class DescWinHearts extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Heart indicators
	private var hrt1:FlxSprite;
	private var hrt2:FlxSprite;
	private var hrt3:FlxSprite;

	// Heart counts
	private var amt1:FlxText;
	private var amt2:FlxText;
	private var amt3:FlxText;

	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines():Array<String>
	{
		return [
			'If ${character.name}\'s risk of suicide increases too much,',
			'${character.pnhe} may need to activate a \"Lifeline\".',
			'This happens automatically, and could represent',
			'a close friend or ${character.therapist},',
			'or a call to a suicide prevention hotline.',
			'${character.name}\'s current lifelines:',
			'     Available       Locked        Used',
		];

		// TODO: Explain if the hearts are locked due to the character's culture, circumstances, etc.

	}

	public function new(character:Character)
	{
		super();
		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Lifelines";
		this.add(title);

        hrt1 = new FlxSprite();
        hrt1.loadGraphic(AssetPaths.heart_big__png, true, 44, 44);
        hrt1.animation.add("active", [0], 5, false);
        hrt1.animation.play("active");
		hrt1.x = fullWidth - hrt1.width - 228 + 100 - 105 - 111;
		hrt1.y = 50 + 20*2 - 15 + 142 - 20;
		this.add(hrt1);

        hrt2 = new FlxSprite();
        hrt2.loadGraphic(AssetPaths.heart_big__png, true, 44, 44);
        hrt2.animation.add("used", [2], 5, false);
        hrt2.animation.play("used");
		hrt2.x = fullWidth - hrt2.width - 228 + 100 - 105;
		hrt2.y = 50 + 20*2 - 15 + 142 - 20;
		this.add(hrt2);

        hrt3 = new FlxSprite();
        hrt3.loadGraphic(AssetPaths.heart_big__png, true, 44, 44);
        hrt3.animation.add("locked", [1], 5, false);
        hrt3.animation.play("locked");
		hrt3.x = fullWidth - hrt3.width - 228 + 100;
		hrt3.y = 50 + 20*2 - 15 + 142 - 20;
		this.add(hrt3);

		var llCount = [0,0,0];
		for (ll in character.lifelines) {
			llCount[ll] += 1;
		}

		var mX = fullWidth - hrt1.width - 228 + 100 - 105 - 111 - 28;
		var mY = 50 + 20*2 - 15 + 142 + 20 - 4;
		amt1 = new FlxText(mX,mY,100);
		amt1.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		amt1.text = ""+llCount[1];
		this.add(amt1);

		mX = fullWidth - hrt1.width - 228 + 100 - 105 - 28;
		amt2 = new FlxText(mX,mY,100);
		amt2.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		amt2.text = ""+llCount[2];
		this.add(amt2);

		var mX = fullWidth - hrt1.width - 228 + 100 - 28;
		amt3 = new FlxText(mX,mY,100);
		amt3.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		amt3.text = ""+llCount[0];
		this.add(amt3);


		var myX = 60;
		var myY = 50;
		textLines = new Array<FlxText>();
		for (line in get_lines()) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}
	}


	
}