package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text for the character
class DescWinRisk extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Current goal
	private var mark1:FlxSprite;
	private var mark2:FlxSprite;
	private var mark3:FlxSprite;

	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines():Array<String>
	{
		return [
			'This meter represents ${character.name}\'s suicide risk level.',
			'Many people face the risk of suicide at some',
			'point in their life, due to a variety of factors.',
			'Actions will affect ${character.name}\'s suicide risk level based',
			'on ${character.pnhis} emotional state and character properties.',
			'Try to get through ${character.duration} safely,',
			'Risk Thresholds:',
			'   Have Suicidal     Develop a      Tried Killing',
			'     Thoughts       Suicide Plan        Self',
		];
	}

	//private var lastLine = "Click \"Cancel\" when you are done.";


	public function new(character:Character)
	{
		super();
		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Suicide Risk Meter";
		this.add(title);

		mark1 = new FlxSprite();
		mark1.loadGraphic(AssetPaths.risk_markers_big__png, true, 40, 40);
		mark1.animation.add("warn", [0], 5, false);
		mark1.animation.play('warn');
		mark1.x = fullWidth - mark1.width - 228 + 100 - 105 - 111;
		mark1.y = 50 + 20*2 - 15 + 142 + 22;
		this.add(mark1);

		mark2 = new FlxSprite();
		mark2.loadGraphic(AssetPaths.risk_markers_big__png, true, 40, 40);
		mark2.animation.add("danger", [1], 5, false);
		mark2.animation.play('danger');
		mark2.x = fullWidth - mark2.width - 228 + 100 - 105 + 20;
		mark2.y = 50 + 20*2 - 15 + 142 + 22;
		this.add(mark2);

		mark3 = new FlxSprite();
		mark3.loadGraphic(AssetPaths.risk_markers_big__png, true, 40, 40);
		mark3.animation.add("attempt", [2], 5, false);
		mark3.animation.play('attempt');
		mark3.x = fullWidth - mark3.width - 228 + 100 + 40;
		mark3.y = 50 + 20*2 - 15 + 142 + 22;
		this.add(mark3);


		var myX = 60;
		var myY = 50;
		textLines = new Array<FlxText>();
		for (line in get_lines()) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}

		// Last line is centered
		/*var lineTxt = new FlxText(0,50+20*10,fullWidth);
		lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		lineTxt.text = lastLine;
		this.add(lineTxt);
		*/
	}


	
}