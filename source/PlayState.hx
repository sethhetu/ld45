package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.math.FlxRect;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.system.FlxSound;
import flixel.tweens.FlxTween;
import flixel.group.FlxSpriteGroup;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;


class PlayState extends FlxState
{
    // Current state of play
    //  0 = Waiting for user input on the main game screen
    //  1 = Waiting for the user to fire (Action phase)
    //  2 = Waiting for the user to Inspect
    //  3 = Ball is firing; waiting to bounce, etc.
    var gameState:Int = 0;


    // How many columns do we have?
    private var cols = 15;

    // How many rows to start?
    private var rows = 11; 

    // Is the top row a "longer" one?
    private var topRowBig = false;

    // How many rows are "too many"?
    // This will cause balls to auto-drop.
    private var maxRows = 20;

    // Our grid of balls
    //   A set of 22 rows, where row (0) is above the screen (not generated yet)
    //   and the last row (21) is past the valid match point (will generate an error)
    //   Each row is either cols or cols-1 balls.
    // The grid is pushed (to the top) and popped (from the bottom) as the game goes by
    // Note that due to the nature of angles (lol) the FlxRects will overlap, so 
    //   in general we need to be careful about having multiple results
    private var grid:Array<Array<GridCell>>;


    // How many balls have we generated in total?
    private var totalBallsGenerated:Int = 0;


    // Emotion colors, to make things easier
    private var EmotionColorYellow = 1;
    private var EmotionColorGreen = 2;
    private var EmotionColorBlue = 3;
    private var EmotionColorOrange = 4;
    private var EmotionColorRed = 5;


    // Convert ball colors into "point" colors
    private var bonusColors = [
        "red" => "red",
        "orange" => "orange",
        "yellow" => "yellow",
        "green" => "green",
        "blue" => "blue",
        "purple" => "purple",
        "pink" => "pink",
    ];


    // Colors to show "good" and "bad" on the emotion bar
    private var PopUpColorGood = 0xFF009900;
    private var PopUpColorBad = 0xFF990000;


    // How many pixels to scroll the grid (down), and how many we've already scrolled.
    private var gridToScroll = 0;
    private var gridDoneScroll = 0;

    // Generic
    private var ballSize = 32;
    private var ballAdjSize = 32-4;

    // Will the next turn be a heart?
    private var nextTurnHeart:Bool = false;


    // Our scenic background
    var bgScenicSpr:FlxSprite;

    // The background sprite
    var bgSpr:FlxSprite;

    // Start off on something plausible (we'll regenerate it)
    var nextBallColor = "red";
    var character:Character;

    // BG overlay for player stats, etc. (and a group for it.)
    var bgStatsGroup:FlxSpriteGroup;
    var bgStatsSpr:FlxSprite;

    // BG overlay for the current description, etc. (and a group for it)
    var bgDescGroup:FlxSpriteGroup;
    var bgDescSpr:FlxSprite;

    // A series of special sprites for displaying descriptions. 
    // All are stored in bgDescGroup
    var winDescHelp:DescWinHelp;
    var winDescCurrTurn:DescWinCurrTurn;
    var winDescCharacter:DescWinCharacter;
    var winDescHearts:DescWinHearts;
    var winDescRisk:DescWinRisk;
    var winDescEmotionBar:DescWinEmotionBar;
    var winDescEmotionHint:DescWinEmotionHint;
    var winDescPersonalProperty:DescWinPersonalProperty;
    var winDescGoal:DescWinGoal;
    var winDescBall:DescWinBall;

    // General turn counter
    var turnCount:Int = 0;


    // Various coords
    var playArea = new FlxRect(18,18,  504,730);      // Where balls can bounce, in world (?) coords
    var leftPanel = new FlxRect(18,766,  136,176);    // The left panel (with the player's face)
    var midPanel = new FlxRect(172, 766,  268, 176);  // The center panel (with the emotions)
    var rightPanel = new FlxRect(458,766,  64,176);   // The right panel (with player properties + goal)

    // All balls/coins in play, including the current one.
    // TODO: Our safety might not remove from this set correctly.
    var balls:FlxTypedGroup<Ball>;

    // Ball currently being fired, for reference.
    var currBall:Ball;

    // How far can we drag this ball when aiming?
    var ballMaxDragRadius:Float = 100;

    // Buttons to press, and their icons (and a group)
    var btnGroup:FlxSpriteGroup;
    //
    var btnInspectBg:FlxSprite;
    var btnInspectTxt:FlxText;
    var btnInspectImg:FlxSprite;
    // 
    var btnFireBg:FlxSprite;
    var btnFireTxt:FlxText;
    var btnFireImg:FlxSprite;

    // Sprite representing the mouse, if any
    // TODO: We might want a separate target sprite (and keep the hand while dragging)
    var mouseSprite:FlxSprite;
    var targetSprite:FlxSprite;

    // Player's name + face graphic
    var playerName:FlxText;
    var faceSpr:FlxSprite;
    var hearts:Array<FlxSprite>;

    // Big risk meter
    var riskMeterBG:FlxSprite;
    var riskMeterValue:FlxSprite;
    var riskMeter:FlxSprite;
    var riskMarkers:Array<FlxSprite>;
    var riskValueStartY:Float;

    // Our 3 emotion bars
    var emotionTexts:Array<FlxText>;
    var emotionBars:Array<FlxSprite>;
    var emotionValuesBG:Array<FlxSprite>;
    var emotionValues:Array<FlxSprite>;
    var emotionValueStartY:Array<Float>;

    // The 3 rows of things affecting our emotion bars
    var emotionHints:Array<Array<BonusSprite>>;

    // Our 3 properties
    var personProps:Array<BonusSprite>;

    // Our goal 
    // TODO: need "dots" for progression
    var goalItem:FlxSprite;


    // Special popups that appear everything else
    var popupChapterWon:PopupWinChapterWon;
    var popupReceivedHeart:PopupWinGotHeart;
    var popupEventHappened:PopupWinEventHappened;
    //
    // And where does this want to go?
    var popupY:Int; // Onscreen
    var popupOffscreenY:Int; // Offscreen, top
    var popupOffscreenY2:Int; // Offscreen, bottom
    //
    // Button for pressing this popup
    var btnPopupOk:FlxSprite;
    var btnPopupOkTxt:FlxText;
    //
    // What are we currently dealing with
    var currPopup:Int = 0;   // 1 = victory/gameover, 2 = heart popup, 3 = event popup
    //
    // This makes fading easier
    var fadeToBlack:FlxSprite;


    // Was the last event bad?
    var lastEventWasBad:Bool = true;


    // Overlays for various indicators
    // TODO: Right now chevrons are added globally, so they won't scroll correctly.
    //var chevrons:FlxTypedGroup<FlxSprite>;


    // Control stuff

    // Are we dragging the current ball?
    // And if so, what point did we start from?
    var draggingBall:Bool;
    var mouseDownPos:FlxPoint;
    var ballStartPos:FlxPoint; // NOTE: Where this sprite started (TODO: can we handle it differently?)

    // Are we holding down on a button?
    var btnDown:Int = 0; // 0 = none, 1 = left, 2 = right

    // Disable input?
    var disableInput:Bool = false;

    //  Turn off collision safeties (for animating)
    var disableSafeties:Bool = false;

    // Special pause item for "cutscenes" (popups) that disables *most* input
    var disableForCutscenes:Bool = false;

    // Our clicking sound
    var clickSound:FlxSound;

    // Our coin sound
    var coinSound:FlxSound;

    // Our "match" sound
    var matchSound:FlxSound;

    // Sound for a ball landing but not matching.
    var placementSound:FlxSound;

    // Sound for bouncing off walls
    var bounceSound:FlxSound;

    // Danger type sound (low health)
    var dangerLowSound:FlxSound;

    // Draw and release
    var drawSound:FlxSound;
    var releaseSound:FlxSound;

    // On event ("danger!")
    var dangerSound:FlxSound;

    var firstTurn = true;
    


    override public function new(?character:Character)
    {
        super();
        if (character==null) {
            this.character = CharacterMaker.Mia_Story1(); // Whatever we're testing these days


// TEMP
this.character = CharacterMaker.GameDone();
//this.character = CharacterMaker3.Mia_Story2();

        } else {
            this.character = character;
        }
    }

    override public function create():Void
    {
        super.create();

        // Wheeee!!!!
        MultiPlatformUtils.ZoomToMax();

        nextBallColor = getNextBallColor(null);

        // Make this a little easier to use
        var orig = bonusColors.copy();
        for (key => val in orig) {
            bonusColors[key + "_dying"] = val;
            bonusColors[key + "_static"] = val;
        }

        this.bgColor = FlxColor.fromRGB(0x00, 0x00, 0x33);

        // Make a BG scene
        bgScenicSpr = new FlxSprite();
        bgScenicSpr.loadGraphic(character.scenic_png);
        bgScenicSpr.origin.set(0,0);
        this.add(bgScenicSpr);

        // Trying this...
        this.balls = new FlxTypedGroup<Ball>();
        this.add(balls);

        // Make a BG sprite
        // TODO: Centralize code
        bgSpr = new FlxSprite();
        bgSpr.loadGraphic(AssetPaths.level_bg_line__png, true, 540, 960);
        bgSpr.animation.add("none", [0], 5, false);
        bgSpr.animation.play("none");
        bgSpr.x = 0;
        bgSpr.y = 0;
        this.add(bgSpr);

        // The item "description" screen.
        // Has to be under the stats screen so it can slide up from behind it.
        bgDescGroup = new FlxSpriteGroup();
        this.add(bgDescGroup);

        bgDescSpr = new FlxSprite();
        bgDescSpr.loadGraphic(AssetPaths.level_bg_desc__png, true, 540, 320);
        bgDescSpr.animation.add("normal", [0], 5, false);
        bgDescSpr.animation.play("normal");
        bgDescSpr.x = 0;
        bgDescSpr.y = 446;
        bgDescGroup.add(bgDescSpr);

        // Windows for the description
        winDescHelp = new DescWinHelp(character);
        winDescHelp.x = bgDescSpr.x + 18;
        winDescHelp.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescHelp);

        winDescCurrTurn = new DescWinCurrTurn(character);
        winDescCurrTurn.x = bgDescSpr.x + 18;
        winDescCurrTurn.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescCurrTurn);

        winDescCharacter = new DescWinCharacter(character);
        winDescCharacter.x = bgDescSpr.x + 18;
        winDescCharacter.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescCharacter);

        winDescHearts = new DescWinHearts(character);
        winDescHearts.x = bgDescSpr.x + 18;
        winDescHearts.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescHearts);

        winDescRisk = new DescWinRisk(character);
        winDescRisk.x = bgDescSpr.x + 18;
        winDescRisk.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescRisk);

        winDescEmotionBar = new DescWinEmotionBar(character);
        winDescEmotionBar.x = bgDescSpr.x + 18;
        winDescEmotionBar.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescEmotionBar);

        winDescEmotionHint = new DescWinEmotionHint(character);
        winDescEmotionHint.x = bgDescSpr.x + 18;
        winDescEmotionHint.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescEmotionHint);

        winDescPersonalProperty = new DescWinPersonalProperty(character);
        winDescPersonalProperty.x = bgDescSpr.x + 18;
        winDescPersonalProperty.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescPersonalProperty);

        winDescGoal = new DescWinGoal(character);
        winDescGoal.x = bgDescSpr.x + 18;
        winDescGoal.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescGoal);

        winDescBall = new DescWinBall(character);
        winDescBall.x = bgDescSpr.x + 18;
        winDescBall.y = bgDescSpr.y + 18;
        bgDescGroup.add(winDescBall);
        

        // We need to update this last, since it affects the value returned by "x" and "y", which may be surprising.
        bgDescGroup.y = 302; // Note: This group starts "hidden" behind the main panel
        bgDescGroup.visible = false;

        // The player "stats" screen.
        // The group is located at Y=0 due to historical reasons.
        bgStatsGroup = new FlxSpriteGroup();
        this.add(bgStatsGroup);

        bgStatsSpr = new FlxSprite();
        bgStatsSpr.loadGraphic(AssetPaths.level_bg_stats__png, true, 540, 212);
        bgStatsSpr.animation.add("normal", [0], 5, false);
        bgStatsSpr.animation.play("normal");
        bgStatsSpr.x = 0;
        bgStatsSpr.y = 748;
        bgStatsGroup.add(bgStatsSpr);

        btnGroup = new FlxSpriteGroup();
        this.add(btnGroup);

        // Stuff for Inspect button
        btnInspectBg = new FlxSprite();
        btnInspectBg.loadGraphic(AssetPaths.buttons__png, true, 181, 64);
        btnInspectBg.animation.add("up", [0], 5, false);
        btnInspectBg.animation.add("down", [1], 5, false);
        btnInspectBg.animation.play("up");
        var mx = playArea.x - 2;
        var my = playArea.y+playArea.height - btnInspectBg.height + 2;
        btnInspectBg.x = mx;
        btnInspectBg.y = my;
        btnGroup.add(btnInspectBg);
        //
        btnInspectTxt = new FlxText();
        btnInspectTxt.x = mx + 18;
        btnInspectTxt.y = my + 21 - 32/2;
        btnInspectTxt.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnInspectTxt.text = "Inspect";
        btnGroup.add(btnInspectTxt);
        //
        btnInspectImg = new FlxSprite();
        btnInspectImg.loadGraphic(AssetPaths.items__png, true, 32, 32);
        Ball.AddAnimations(btnInspectImg.animation);
        btnInspectImg.animation.play("magnifying");
        btnInspectImg.x = mx + btnInspectBg.width - btnInspectImg.width - 18 - 2;
        btnInspectImg.y = my + 15 + 2;
        btnGroup.add(btnInspectImg);

        // Stuff for Fire button
        btnFireBg = new FlxSprite();
        btnFireBg.loadGraphic(AssetPaths.buttons_2__png, true, 166, 64);
        btnFireBg.animation.add("up", [0], 5, false);
        btnFireBg.animation.add("down", [1], 5, false);
        btnFireBg.animation.play("up");
        mx = playArea.x+playArea.width - btnFireBg.width + 2;
        my = playArea.y+playArea.height - btnFireBg.height + 2;
        btnFireBg.x = mx;
        btnFireBg.y = my;
        btnGroup.add(btnFireBg);
        //
        btnFireTxt = new FlxText(0,0,btnFireBg.width);
        btnFireTxt.x = mx - 18;
        btnFireTxt.y = my + 21 - 32/2;
        btnFireTxt.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.RIGHT, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnFireTxt.text = "Action";
        btnGroup.add(btnFireTxt);
        //
        btnFireImg = new FlxSprite();
        btnFireImg.loadGraphic(AssetPaths.items__png, true, 32, 32);
        Ball.AddAnimations(btnFireImg.animation);
        btnFireImg.animation.play(nextBallColor);
        btnFireImg.x = mx + 18;
        btnFireImg.y = my + 15 + 2;
        btnGroup.add(btnFireImg);

        // Semi-complicated
        generateBalls();

        targetSprite = new FlxSprite();
        targetSprite.loadGraphic(AssetPaths.crosshair__png, true, 64, 64);
        targetSprite.animation.add("target", [0], 5, false);
        targetSprite.visible = false;
        this.add(targetSprite);

        resetCurrBall();

        // Face sprite
        faceSpr = new FlxSprite();
        faceSpr.loadGraphic(AssetPaths.faces__png, true, 96, 96);
        faceSpr.animation.add("char1_neutral", [27], 5, false);
        faceSpr.animation.add("char1_older_neutral", [25], 5, false);
        faceSpr.animation.add("char2_neutral", [24], 5, false);
        faceSpr.animation.add("char3_neutral", [28], 5, false);
        faceSpr.animation.play(character.face_anim);
        faceSpr.x = leftPanel.x + leftPanel.width/2 - faceSpr.width/2;
        faceSpr.y = leftPanel.y + 8 + 10;
        bgStatsGroup.add(faceSpr);

        // Player name
        playerName = new FlxText(0, 0, leftPanel.width-20);
        playerName.x = leftPanel.x + 10;
        playerName.y = leftPanel.y + 8 + faceSpr.height - 5 + 10 - 2;
        playerName.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
        playerName.text = character.name;
        bgStatsGroup.add(playerName);

        // Hearts left
        var hrtX = leftPanel.x + leftPanel.width - 3 - 24*character.lifelines.length;
        var hrtY = leftPanel.y + 3;
        hearts = new Array<FlxSprite>();
        for (ll in character.lifelines) {
            var hrt = new FlxSprite();
            hrt.loadGraphic(AssetPaths.heart__png, true, 22, 22);
            hrt.animation.add("active", [0], 5, false);
            hrt.animation.add("used", [1], 5, false);
            hrt.animation.add("locked", [2], 5, false);
            if (ll == 0) {
                hrt.animation.play("used");
            } else if (ll == 1) {
                hrt.animation.play("active");
            } else if (ll == 2) {
                hrt.animation.play("locked");
            }
            hrt.x = hrtX;
            hrt.y = hrtY;
            hrtX += 24;
            hearts.push(hrt);
            bgStatsGroup.add(hrt);
        }

        // Risk meter & markers
        riskMeter = new FlxSprite();
        riskMeter.loadGraphic(AssetPaths.big_meter__png, true, 115, 32);
        riskMeter.animation.add("normal", [0], 5, false);
        riskMeter.animation.play("normal");
        mx = leftPanel.x + leftPanel.width/2 - riskMeter.width/2;
        my = leftPanel.y + leftPanel.height - riskMeter.height - 4;
        riskMeter.x = mx - 1;
        riskMeter.y = my + 1 - 9;
        // Make the BG first, then add both so the BG appears under it.
        riskMeterBG = new FlxSprite();
        riskMeterBG.loadGraphic(AssetPaths.big_meter_values__png);
        riskMeterBG.origin.set(0,0);
        riskMeterBG.clipRect = new FlxRect(0, 0, 111, 28);
        riskMeterBG.x = riskMeter.x + 3;
        riskMeterBG.y = riskMeter.y + 2;
        // Same for value
        riskMeterValue = new FlxSprite();
        riskMeterValue.loadGraphic(AssetPaths.big_meter_values__png);
        riskMeterValue.origin.set(0,0);
        riskMeterValue.clipRect = new FlxRect(0, 0, 111, 28);
        riskValueStartY = riskMeter.y + 2;
        riskMeterValue.x = riskMeter.x + 3;
        riskMeterValue.y = riskValueStartY;
        //
        bgStatsGroup.add(riskMeterBG);
        bgStatsGroup.add(riskMeterValue);
        bgStatsGroup.add(riskMeter);
        //
        riskMarkers = new Array<FlxSprite>();
        var markersX = [37, 74, 111];
        for (i in 0...3) {
            var mark = new FlxSprite();
            mark.loadGraphic(AssetPaths.risk_markers__png, true, 20, 20);
            mark.animation.add("active", [5-i], 5, false);
            mark.animation.add("grayed", [2-i], 5, false);
            mark.animation.play("grayed");
            mark.x = riskMeter.x + markersX[i] - 20/2 + 1;
            mark.y = riskMeter.y - 8 + riskMeter.height - 5;
            riskMarkers.push(mark);
            bgStatsGroup.add(mark);
        }
        updateRiskLevel();

        // Emotion bars for this person
        var barY = 4;
        var remRects:Array<FlxRect> = new Array<FlxRect>();
        emotionTexts = new Array<FlxText>();
        emotionBars = new Array<FlxSprite>();
        emotionValuesBG =  new Array<FlxSprite>();
        emotionValues = new Array<FlxSprite>();
        emotionValueStartY = new Array<Float>();
        for (i in 0...3) {
            var bar = new FlxSprite();
            bar.loadGraphic(AssetPaths.emotion_bar__png, true, 135, 32);
            bar.animation.add("normal", [0], 5, false);
            bar.animation.play("normal");
            bar.x = midPanel.x + 16;
            bar.y = midPanel.y + barY + 18;
            barY += Math.floor(bar.height) + 24;
            // Make the bar BG first, so that it appears under this.
            var obj = new FlxSprite();
            obj.loadGraphic(AssetPaths.emotion_values__png);
            obj.origin.set(0,0);
            obj.clipRect = new FlxRect(0, 0, 132, 20);
            obj.x = bar.x + 1;
            obj.y = bar.y + 2;
            emotionValuesBG.push(obj);
            bgStatsGroup.add(obj);
            // Then the overlay:
            obj = new FlxSprite();
            obj.loadGraphic(AssetPaths.emotion_values__png);
            obj.origin.set(0,0);
            obj.clipRect = new FlxRect(0, 20, 132, 20);
            obj.x = bar.x + 1;
            obj.y = bar.y + 2;
            emotionValues.push(obj);
            emotionValueStartY.push(obj.y);
            bgStatsGroup.add(obj);
            // Now push the bar, so it's over the BG
            emotionBars.push(bar);
            bgStatsGroup.add(bar);
            var sx = bar.x+bar.width+16;
            var rr = new FlxRect(sx, bar.y-18, midPanel.width-sx, bar.height+18);
            remRects.push(rr);
            //
            var txt = new FlxText(0,0,bar.width);
            txt.x = bar.x;
            txt.y = bar.y - 19 - 4;
            txt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
            emotionTexts.push(txt);
            bgStatsGroup.add(txt);
        }
        updateEmotions();

        // Things affecting that emtion need their own rectangle
        var sampleHints = new Array<Array<String>>();
        var sampleHintBonuses = new Array<Array<Int>>();
        for (i in 0...3) {
            sampleHints.push(new Array<String>());
            sampleHintBonuses.push(new Array<Int>());
        }
        var maxLen = 2;
        for (color=>effects in character.basic_response) {
            for (eff in effects) {
                if (eff.target=='emotion') {
                    var i = eff.id;
                    if (sampleHints[i].length<3) {
                        sampleHints[i].push(color);
                        sampleHintBonuses[i].push(-1*eff.amt);
                    }
                    if (sampleHintBonuses[i].length >= 3) {
                        maxLen = 3;
                    }
                }
            }
        }
        emotionHints = new Array<Array<BonusSprite>>();
        for (i in 0...3) {
            var newArr = new Array<BonusSprite>();
            emotionHints.push(newArr);

            var tmp = 0;
            var xOff = 8;
            var xDelt = 0;
            if (maxLen == 3) {
                xOff -= 14;
                xDelt = -7;
            }
            for (row in sampleHints[i]) {
                var sp = new BonusSprite();
                bgStatsGroup.add(sp);
                sp.loadGraphic(AssetPaths.items__png, true, 32, 32);
                Ball.AddAnimations(sp.animation);
                sp.animation.play(row + "_static");
                sp.setBonus(bgStatsGroup, sampleHintBonuses[i][tmp]);
                tmp += 1;
                sp.x = remRects[i].x + xOff;
                xOff += 42 + xDelt;
                sp.y = remRects[i].y + 13;
                newArr.push(sp);
            }
        }


        // Properties of this character
        personProps = new Array<BonusSprite>();
        var propsY = [0, 44, 88, 132];
        for (i in 0...3) {
            var prp = new BonusSprite();
            prp.loadGraphic(AssetPaths.items__png, true, 32, 32);
            Ball.AddAnimations(prp.animation);
            prp.animation.play(character.property_anims[i]);
            prp.x = rightPanel.x + rightPanel.width/2 - prp.width/2;
            prp.y = rightPanel.y + propsY[i] + (44-32)/2;
            prp.setBonus(bgStatsGroup, character.property_bonuses[i]);
            personProps.push(prp);
            bgStatsGroup.add(prp);
        }

        // Chevrons in their own environment
        //this.add(chevrons);

        // Goal item
        goalItem = new FlxSprite();
        goalItem.loadGraphic(AssetPaths.items__png, true, 32, 32);
        Ball.AddAnimations(goalItem.animation);
        goalItem.animation.play(character.goal_anim);
        goalItem.x = rightPanel.x + rightPanel.width/2 - goalItem.width/2;
        goalItem.y = rightPanel.y + propsY[3] + (44-32)/2;
        bgStatsGroup.add(goalItem);
        updateGoal();



        // Popups appear over everything else.
        popupChapterWon = new PopupWinChapterWon(character);
        popupOffscreenY = Math.floor(-2 - popupChapterWon.height);
        popupOffscreenY2 = 960 + 64;
        popupChapterWon.x = 540/2 - popupChapterWon.width/2;
        popupChapterWon.y = popupOffscreenY;
        popupY = Math.floor(960/2 - popupChapterWon.height/2) - 110;
        this.add(popupChapterWon);
        //
        popupReceivedHeart = new PopupWinGotHeart(character);
        popupReceivedHeart.x = 540/2 - popupReceivedHeart.width/2;
        popupReceivedHeart.y = popupOffscreenY;
        this.add(popupReceivedHeart);
        //
        popupEventHappened = new PopupWinEventHappened(character);
        popupEventHappened.x = 540/2 - popupEventHappened.width/2;
        popupEventHappened.y = popupOffscreenY;
        this.add(popupEventHappened);


        // Same with the popup button
        btnPopupOk = new FlxSprite();
        btnPopupOk.loadGraphic(AssetPaths.title_buttons_big_2__png, true, 310, 84);
        btnPopupOk.animation.add("up", [0], 5, false);
        btnPopupOk.animation.add("down", [1], 5, false);
        btnPopupOk.animation.play("up");
        var mx = 540/2 - btnPopupOk.width/2;
        var my = 100 + 500 + 80 - 5 - 110 - 95;
        btnPopupOk.x = mx;
        btnPopupOk.y = my;
        this.add(btnPopupOk);
        //
        btnPopupOkTxt = new FlxText(0,0,310);
        btnPopupOkTxt.x = mx + 18 - 2 - 20;
        btnPopupOkTxt.y = my + 21 - 32/2 - 16 + 7 + 2;
        btnPopupOkTxt.setFormat("assets/kanit-regular.ttf", 52, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnPopupOkTxt.text = 'Next Story';
        this.add(btnPopupOkTxt);
        btnPopupOk.alpha = 0;
        btnPopupOkTxt.alpha = 0;


        //
        fadeToBlack = new FlxSprite();
        fadeToBlack.loadGraphic(AssetPaths.fade_to_black__png);
        fadeToBlack.origin.set(0,0);
        this.add(fadeToBlack);
        fadeToBlack.alpha = 0;


        // TODO: Encapsulate into a class that is context-sensitive.
        // Mouse always goes on top
        mouseSprite = new FlxSprite();
        MultiPlatformUtils.InitMouse(mouseSprite);
        this.add(mouseSprite);

        // Load our sound effects
        clickSound = FlxG.sound.load(AssetPaths.press_button__ogg);
        coinSound = FlxG.sound.load(AssetPaths.sfx_coin_double4__ogg);
        matchSound = FlxG.sound.load(AssetPaths.sfx_sounds_powerup10__ogg);
        placementSound = FlxG.sound.load(AssetPaths.sfx_sounds_Blip4__ogg);
        bounceSound = FlxG.sound.load(AssetPaths.sfx_sound_neutral1__ogg);
        dangerLowSound = FlxG.sound.load(AssetPaths.sfx_lowhealth_alarmloop4__ogg);
        drawSound = FlxG.sound.load(AssetPaths.sfx_sounds_powerup16__ogg);
        releaseSound = FlxG.sound.load(AssetPaths.sfx_sounds_impact13__ogg);
        dangerSound = FlxG.sound.load(AssetPaths.sfx_lowhealth_alarmloop7__ogg);
    

        // Play BG music
        FlxG.sound.playMusic(character.level_music, 1, true);
    }


    // Retrieve the IntPoint representing the given GridCell
    private function ballToIntPoint(source:Ball):IntPoint
    {
        for (y in 0...grid.length) {
            var row = grid[y];
            for (x in 0...row.length) {
                var cell = row[x];
                if (cell.ball == source) {
                    return new IntPoint(x,y);
                }
            }
        }
        return new IntPoint(-9,-9);
    }

    // Retrieve the IntPoint representing the given GridCell
    private function gridToIntPoint(source:GridCell):IntPoint
    {
        for (y in 0...grid.length) {
            var row = grid[y];
            for (x in 0...row.length) {
                var cell = row[x];
                if (cell == source) {
                    return new IntPoint(x,y);
                }
            }
        }
        return new IntPoint(-9,-9);
    }


    // Return a list of IntPoints for up to six neighbors of this point
    private function computeNeighbors(source:IntPoint):Array<IntPoint>
    {
        var res = new Array<IntPoint>();

        var minY:Int = IntPoint.Max(0, source.y-1);
        var maxY:Int = IntPoint.Min(grid.length-1, source.y+1);
        for (y in minY...maxY+1) {
            // This is SUPER annoying
            var minX:Int = source.x-1;
            var maxX:Int = source.x+1;
            if (y != source.y) {
                var isBig = grid[y].length==cols;
                if (isBig) {
                    minX += 1;
                } else {
                    maxX -= 1;
                }
            }
            minX = IntPoint.Max(0, minX);
            maxX = IntPoint.Min(grid[y].length-1, maxX);
            for (x in minX...maxX+1) {
                if (x!=source.x || y!=source.y) {
                    res.push(new IntPoint(x,y));
                }
            }
        }

        return res;
    }


    // Scans all balls, and retrieves a list of those disconnected to the top of the screen.
    private function dropDisconnectedBalls():Array<Ball>
    {
        // Reset flags
        for (row in grid) {
            for (cell in row) {
                if (cell.ball != null) {
                    cell.ball.checked = false;
                }
            }
        }

        var connected = new Map<Ball,Int>();

        // Which cells are left to check?
        var next = new Array<IntPoint>();
        for (x in 0...grid[1].length) {
            next.push(new IntPoint(x, 1));
        }

        // Keep scanning while the "next" array is non-empty
        while (next.length > 0) {
            // Get the next candidate and mark it checked.
            // TODO: We are double-checking, but I don't *think* we have to.
            var currPt = next.shift();
            var currCell = grid[currPt.y][currPt.x];
            var currBall = currCell.ball;
            if (currBall == null) { continue; }
            if (currBall.dying) { continue; }
            if (!currBall.alive) { continue; }
            currBall.checked = true;

            // Does this match the color check?
            //if (currBall.animation.name != ballColor) { continue; }

            // Save it, add its neighbors as candidates
            connected[currBall] = 1;

            var neighbors = computeNeighbors(currPt);
            for (neighPt in neighbors) {
                var neighCell = grid[neighPt.y][neighPt.x];
                var neighBall = neighCell.ball;
                if (neighBall == null) { continue; }
                if (neighBall.checked) { continue; }
                neighBall.checked = true;
                //if (neighBall.animation.name != ballColor) { continue; }
                next.push(neighPt);
            }
        }

        // Now build res based on the connected list
        var res = new Array<Ball>();
        for (row in grid) {
            for (cell in row) {
                if (cell.ball != null) {
                    if (!connected.exists(cell.ball)) {
                        if (cell.ball.dying) { continue; }
                        if (!cell.ball.alive) { continue; }
                        res.push(cell.ball);
                    }
                }
            }
        }

        return res;
    }


    // Retrieve a list of balls that match the color of the one in the current grid cell 
    // This match includes the current.
    private function computeLongestMatch(source:GridCell, ballColor:String):Array<Ball>
    {
        // Get this cell as a point
        var sourcePt = gridToIntPoint(source);
        if (sourcePt.x < -1) {
            return new Array<Ball>(); // Should be impossible
        }

        // Reset flags
        for (row in grid) {
            for (cell in row) {
                if (cell.ball != null) {
                    cell.ball.checked = false;
                }
            }
        }

        var res = new Array<Ball>();

        // Which cells are left to check?
        var next = new Array<IntPoint>();
        next.push(sourcePt);

        // Keep scanning while the "next" array is non-empty
        while (next.length > 0) {
            // Get the next candidate and mark it checked.
            // TODO: We are double-checking, but I don't *think* we have to.
            var currPt = next.shift();
            var currCell = grid[currPt.y][currPt.x];
            var currBall = currCell.ball;
            if (currBall == null) { continue; }
            currBall.checked = true;

            // Does this match the color check?
            if (currBall.animation.name != ballColor) { continue; }

            // Save it, add its neighbors as candidates
            res.push(currBall);

            var neighbors = computeNeighbors(currPt);
            for (neighPt in neighbors) {
                var neighCell = grid[neighPt.y][neighPt.x];
                var neighBall = neighCell.ball;
                if (neighBall == null) { continue; }
                if (neighBall.checked) { continue; }
                neighBall.checked = true;
                if (neighBall.animation.name != ballColor) { continue; }
                next.push(neighPt);
            }
        }

        return res;
    }



    // RNG time
    // Pass in the grid you're generating for, or "null" if you are generating the ball that will be fired
    private function getNextBallColor(gridPos:IntPoint):String
    {
        // Override for the given scenario 
        if (gridPos != null) {
            totalBallsGenerated += 1;

            // Goals
            if (totalBallsGenerated%character.goal_frequency == 0) {
                // Modulate?
                if (character.modulate_goal != 0) {
                    character.goal_frequency += character.modulate_goal;
                    character.modulate_goal *= -1;
                }

                return character.goal_anim;
            }

            // Make our negative events
            if (character.bad_events.exists(totalBallsGenerated)) {
                return 'event';
            }
        }

//return "pink";

        var colors = [];
        var odds = [];   // thresholds
        var maxOdd = 0;  // sum, and last entry in odds

        // Will we re-roll?
        var doReRoll = Std.random(100) < character.reRollChance;
        var maxRolls = doReRoll ? character.reRollTimes+1 : 1;

        // TODO: We might want to check the balls around us, and de-preference those.
        // 5 point penalty


        // Safety
        var res = 'red';

        // Build a lookup
        var ballOdds = character.ballOdds;
        for (clr => amt in ballOdds) {
            colors.push(clr);
            maxOdd += amt;
            odds.push(maxOdd);
        }


        for (i in 0...maxRolls) {
            // Iterate
            var rngVal = Std.random(maxOdd);
            for (i in 0...odds.length) {
                if (rngVal < odds[i]) {
                    res = colors[i];
                    break;
                }
            }

            // Same neighborhood?
            var conflict = false;
            if (gridPos != null) {
                var neighbors = computeNeighbors(gridPos);
                for (neigh in neighbors) {
                    var ball = grid[neigh.y][neigh.x].ball;
                    if (ball != null && ball.getType() == res) {
                        conflict = true;
                        break;
                    }
                }
            }

            if (!conflict) {
                break;
            }
        }

        return res;
    }


    private function generateBalls():Void
    {
        // First, generate our grid.
        var startX = Math.floor(playArea.x + playArea.width/2 - (cols/2) * ballSize);
        var startY = Math.floor(playArea.y) - ballAdjSize;

        var currY = startY;
        var big = topRowBig;
        topRowBig = !topRowBig;
        grid = new Array<Array<GridCell>>();
        for (i in 0...maxRows+2) {
            var row = new Array<GridCell>();
            grid.push(row);

            // Account for big rows
            var count = big ? cols : cols-1;
            var offX = big ? 0 : Math.floor(ballSize/2);

            // Generate row
            var currX = startX + offX;
            for (x in 0...count) {
                var rect = new FlxRect(currX, currY, ballSize, ballSize);
                row.push(new GridCell(rect));
                currX += ballSize;
            }

            // Increment
            currY += ballAdjSize;
            big = !big;
        }


        // Now, use the grid to generate the balls.
        for (y in 1...rows+1) {
            var row = grid[y];
            for (x in 0...row.length) {
                var cell = row[x];
                var rect = cell.rect;
                var ball = new Ball().init(Math.floor(rect.x), Math.floor(rect.y), getNextBallColor(new IntPoint(x,y)));
                cell.ball = ball;
                this.balls.add(ball);
            }
        }

    }


    // Generate X number of rows and scroll them in
    private function generateAndScroll(num:Int)
    {
        disableInput = true;
        disableSafeties = true;

        // TODO: Centralize
        var row = grid[0];
        for (x in 0...row.length) {
            var cell = row[x];
            var rect = cell.rect;
            var ball = new Ball().init(Math.floor(rect.x), Math.floor(rect.y), getNextBallColor(new IntPoint(x,0)));
            cell.ball = ball;
            this.balls.add(ball);
        }

        // Now scroll
        gridToScroll = ballAdjSize;
        gridDoneScroll = 0;
    }



    // What is the effect of the given match on this character?
    private function getCharacterEffect(ballColor:String, matchCount:Int):Array<TurnEffect>
    {
        // Get the basic set of emotional effects.
        var res = new Array<TurnEffect>();

        // Risk is mostly a function of the negative/positive emotions, and may increase over time.
        var riskAmt:Float = 2;

        // Adjust each one based on the "good" or "bad" value of the associated emotion
        for (item in character.basic_response[ballColor]) {
            if (item.target == 'emotion') {
                var oldAmt = item.amt * matchCount;

                // Positive == Bad
                var modAmt:Float = item.amt;
                var modColor = get_emotion_color(character.emotionalState[item.id]);
                if (modColor == EmotionColorRed) {
                    modAmt += 0.5;
                    riskAmt += 2;
                } else if (modColor == EmotionColorOrange) {
                    modAmt += 0.25;
                    riskAmt += 1;
                } else if (modColor == EmotionColorGreen) {
                    modAmt -= 0.25;
                    riskAmt -= 0.5;
                } else if (modColor == EmotionColorBlue) {
                    modAmt -= 0.5;
                    riskAmt -= 1;
                }

                // Finally, affect by number of balls
                var newItemAmt = Math.round(modAmt * matchCount);

                res.push(new TurnEffect("emotion", item.id, newItemAmt));
            }
        }

        // Evaluate risk
        var riskLevel:Int = Math.floor(Math.round(riskAmt));
        if (riskLevel != 0) {
            res.push(new TurnEffect("risk", 0, riskLevel));
        }

        return res;
    }


    // This takes each ball and turns it into a small version, then tweens that to the 
    // emotion bar at the bottom of the screen.
    private function setupPointsToEmotionBars(match:Array<Ball>):Void
    {
        if (match.length==0) { return; }
        // TODO: Split evenly between emotions
        // TODO: But what about ones that have no effect?

        // Make sure this is a supported bonus color
        var ballColor = bonusColors[match[0].animation.name];
        if (ballColor == null) { return; }

        // Compute the effects (as a number)
        var effect = getCharacterEffect(ballColor, match.length);
        var riskEffect = null;

        // Make a map of which bars to send to
        var barIds = new Map<Int,Int>(); // Second int starts at 0, is 1 once we make one anim
        for (item in effect) {
            if (item.target == 'emotion') {
                barIds[item.id] = 0;
            } else if (item.target == 'risk') {
                riskEffect = item;
            }
        }
        var barIdsArray:Array<Int> = new Array<Int>();
        for (key=>val in barIds) {
            barIdsArray.push(key);
        }

        // Ok, make them
        var bar_round = 0;
        var lowestOrig:FlxSprite = null; // ok, highest makes more sense
        for (orig in match) {
            var spr = new FlxSprite();
            spr.loadGraphic(AssetPaths.small_bubbles__png, true, 16, 16);
            Ball.AddPointAnimations(spr.animation);
            spr.animation.play(ballColor);
            spr.x = orig.x+8;
            spr.y = orig.y+8;
            this.add(spr);

            // Save for later
            if (lowestOrig == null || orig.y<lowestOrig.y) {
                lowestOrig = orig;
            }

            // Set up a tween to the right emotion bar
            var emotionId = barIdsArray[bar_round%barIdsArray.length];
            bar_round += 1;
            var destX = emotionBars[emotionId].x + emotionBars[emotionId].width/2 - 8;
            var destY = emotionBars[emotionId].y + emotionBars[emotionId].height/2 - 12;
            if (barIds[emotionId] == 0) {
                // We only fade one because we can't have too many overlapping (or the fade won't work).
                FlxTween.tween(spr, {x:destX, y:destY }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: tweenDoneFadeDeleteObj.bind(spr) });
                barIds[emotionId] = 1;
            } else {
                FlxTween.tween(spr, {x:destX, y:destY }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: tweenDoneDeleteObj.bind(spr) });
            }
        }

        // Make them for risk
        if (riskEffect != null) {
            var spr = new FlxSprite();
            spr.loadGraphic(AssetPaths.small_bubbles__png, true, 16, 16);
            Ball.AddPointAnimations(spr.animation);
            spr.animation.play('white');
            spr.x = lowestOrig.x+8;
            spr.y = lowestOrig.y+8;
            this.add(spr);

            // Set up a tween to the risk meter
            var destX = riskMeter.x + riskMeter.width/2 - 8;
            var destY = riskMeter.y + riskMeter.height/2 - 12 + 3;
            FlxTween.tween(spr, {x:destX, y:destY }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: tweenDoneFadeDeleteObj.bind(spr) });

            // Calculate nextTurnHeart early.
            var oldRiskLevel = character.riskLevel;
            var newRiskLevel = Math.floor(Math.max(0, Math.min(300, oldRiskLevel+riskEffect.amt)));

            // We need to check if we've crossed a new boundary.
            var thresh = [100, 200, 300];
            for (i in 0...3) {
                if (newRiskLevel>=thresh[i]) {
                    var mark = riskMarkers[i];
                    if (mark.animation.name == 'grayed' || i==2) { // Last one always gets a chance
                        // This deserves the bad sound either way
                        dangerLowSound.play();

                        // Does the player have at least one heart ready
                        // (We'll remove it later)
                        for (opt in character.lifelines) {
                            if (opt == 1) {
                                nextTurnHeart = true;
                                break;
                            }
                        }
                    }
                }
                // Break early if we get a lifeline
                if (nextTurnHeart) {
                    break;
                }
            }

            // Detect game over (chapter over)
            if (newRiskLevel >= 300 && !nextTurnHeart) {
                // Fade out our music
                FlxG.sound.music.fadeOut(1.0);

                currPopup = 1;
                btnPopupOkTxt.text = 'Next Story';
                FlxTween.tween(btnPopupOk, { alpha:1 }, 0.5, { type: FlxTweenType.ONESHOT, startDelay: 2.0 });
                FlxTween.tween(btnPopupOkTxt, { alpha:1 }, 0.5, { type: FlxTweenType.ONESHOT, startDelay: 2.0 });

                // Tween in our popup window
                popupChapterWon.makeLoss();
                popupChapterWon.y = popupOffscreenY;
                FlxTween.tween(popupChapterWon, { y:popupChapterWon.y }, 1.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: continuePopupAnim.bind(false) });

                // Keep input disabled.
                disableInput = true;
                disableForCutscenes = true;
            }
        }

        //Delay a popup for that
        // TODO: This way of doing it is silly & wasteful --maybe we can attach it to the effect (a no-op first)?
        var spr = new FlxSprite();
        spr.x = -999;
        FlxTween.tween(spr, {x:-999 }, 1.0, { type: FlxTweenType.ONESHOT, onComplete: popUpEffects.bind(effect) });
    }


    // Tween callback that fades then deletes the object in question
    private function tweenDoneFadeDeleteObj(toDelete:FlxSprite, tween:FlxTween):Void
    {
        // Note: This isn't working for some reason, but it still provides a delay (which we need).
        FlxTween.tween(toDelete, {alpha:0.0 }, 0.4, { type: FlxTweenType.ONESHOT, onComplete: tweenDoneDeleteObj.bind(toDelete) });
    }

    // Tween callback that deletes the object in question (assume added to "this")
    private function tweenDoneDeleteObj(toDelete:FlxSprite, tween:FlxTween):Void
    {
        toDelete.kill();
        this.remove(toDelete);

        // NOTE: This is not the right place, but...
        updateGoal();
    }


    private function popUpEffects(effect:Array<TurnEffect>, tween:FlxTween):Void
    {
        // Show counts on each emotion meter
        for (item in effect) {
            if (item.target == 'emotion') {
                var txt = (item.amt>0 ? ">> " : "<< ") + Math.floor(Math.abs(item.amt));
                var clr = item.amt>0 ? PopUpColorBad : PopUpColorGood;
                var spr = new PopUpText(txt, clr);
                spr.x = emotionBars[item.id].x + emotionBars[item.id].width/2 - spr.width/2;
                spr.y = emotionBars[item.id].y + emotionBars[item.id].height/2 - spr.height/2 - 10;
                this.add(spr);
                spr.alpha = 0;

                FlxTween.tween(spr, {alpha:1, }, 0.4, { type: FlxTweenType.ONESHOT });
                FlxTween.tween(spr, {y:spr.y-30 }, 0.8, { type: FlxTweenType.ONESHOT, onComplete: popUpMakeEffectHappen.bind(item) });
                FlxTween.tween(spr, {x:spr.x }, 1.5, { type: FlxTweenType.ONESHOT, onComplete: tweenDoneDeleteObj.bind(spr) }); // Grumble
            } else if (item.target == 'risk') {
                var txt = (item.amt>0 ? ">> " : "<< ") + Math.floor(Math.abs(item.amt));
                var clr = item.amt>0 ? PopUpColorBad : PopUpColorGood;
                var spr = new PopUpText(txt, clr);
                spr.x = riskMeter.x + riskMeter.width/2 - spr.width/2;
                spr.y = riskMeter.y + riskMeter.height/2 - spr.height/2 - 10;
                this.add(spr);
                spr.alpha = 0;

                FlxTween.tween(spr, {alpha:1, }, 0.4, { type: FlxTweenType.ONESHOT });
                FlxTween.tween(spr, {y:spr.y-30 }, 0.8, { type: FlxTweenType.ONESHOT, onComplete: popUpMakeEffectHappen.bind(item) });
                FlxTween.tween(spr, {x:spr.x }, 1.5, { type: FlxTweenType.ONESHOT, onComplete: tweenDoneDeleteObj.bind(spr) }); // Grumble
            }
        }


        // TODO: Also show change in risk
    }


    private function popUpMakeEffectHappen(item:TurnEffect, tween:FlxTween):Void
    {
        if (item.target == 'emotion') {
            character.emotionalState[item.id] = Math.floor(Math.max(0, Math.min(200, character.emotionalState[item.id]+item.amt)));
            if (gameState == 0 || gameState==2) {
                updateEmotions();
            }
        } else if (item.target == 'risk') {
            var oldRiskLevel = character.riskLevel;
            character.riskLevel = Math.floor(Math.max(0, Math.min(300, character.riskLevel+item.amt)));

            if (gameState == 0 || gameState==2) {
                updateRiskLevel();
            }
        }
    }


    private function moveGoalToPointsSection(goal:Ball):Void
    {
        var spr = new FlxSprite();
        spr.loadGraphic(AssetPaths.small_bubbles__png, true, 16, 16);
        Ball.AddPointAnimations(spr.animation);
        if (goal.getType() == 'book') {
            spr.animation.play('green');
        } else if (goal.getType() == 'coin') {
            spr.animation.play('yellow');
        } else if (goal.getType() == 'cake') {
            spr.animation.play('purple');
        } else {
            spr.animation.play('white'); // Just guess
        }
        spr.x = goal.x+8;
        spr.y = goal.y+8;
        this.add(spr);

        // Set up a tween to the goal icon
        var destX = goalItem.x + goalItem.width/2 - 8 + 2;
        var destY = goalItem.y + goalItem.height/2 - 12 + 4;
        FlxTween.tween(spr, {x:destX, y:destY }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: tweenDoneFadeDeleteObj.bind(spr) });
    }



    private function onHeartFullyZoomed(tween:FlxTween):Void
    {
        // Fade and remove the heart
        FlxTween.tween(currBall, { alpha:0 }, 0.5, { type: FlxTweenType.ONESHOT, onComplete: removeCurrHeartBall });

        // Use it up
        for (i in 0...character.lifelines.length) {
            if (character.lifelines[i] == 1) {
                character.lifelines[i] = 0;
                hearts[i].animation.play('used');
                break;
            }
        }

        // Reduce risk
        character.riskLevel = Math.floor(Math.max(0, character.riskLevel - 50));
        updateRiskLevel();

        // Drop down our cutscene (popup) window
        currPopup = 2;
        btnPopupOkTxt.text = "Confirm";
        FlxTween.tween(btnPopupOk, { alpha:1 }, 0.5, { type: FlxTweenType.ONESHOT, startDelay: 1.0 });
        FlxTween.tween(btnPopupOkTxt, { alpha:1 }, 0.5, { type: FlxTweenType.ONESHOT, startDelay: 1.0 });

        // Tween in our popup window
        popupReceivedHeart.y = popupOffscreenY;
        FlxTween.tween(popupReceivedHeart, { y:popupReceivedHeart.y }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: continueHeartZoomAnim });
    }

    private function continueHeartZoomAnim(tween:FlxTween):Void
    {
        FlxTween.tween(popupReceivedHeart, { y: popupY }, 1.0, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: showPopupAnimDone });        
    }


    private function checkScreenAdvancedTooFar():Array<Ball>
    {
        var myMaxRows = maxRows;

        // First see if any ball is past the line
        var tooFar = false;
        for (y in myMaxRows+1...grid.length) {
            for (x in 0...grid[y].length) {
                if (grid[y][x].ball != null) {
                    tooFar = true;
                    break;
                }
            }
            if (tooFar) { break; }
        }
        if (!tooFar) { return new Array<Ball>(); }

        // Now, form a set of balls to remove (via drop)
        var res = new Array<Ball>();
        var minY = Math.floor(Math.max(1, myMaxRows-5));
        for (y in minY...grid.length) {
            for (x in 0...grid[y].length) {
                if (grid[y][x].ball != null) {
                    res.push(grid[y][x].ball);
                }
            }
        }

        return res;
    }


    private function removeCurrHeartBall(tween:FlxTween):Void
    {
        currBall.kill();
        this.remove(currBall);
        nextBallColor = getNextBallColor(null);
        resetCurrBall();
        disableSafeties = false;

        // Might as well do this now.
        btnInspectBg.visible = true;
        btnInspectImg.visible = true;
        btnInspectTxt.visible = true;
        btnFireBg.visible = true;
        btnFireImg.visible = true;
        btnFireTxt.visible = true;
        btnFireTxt.text = "Action";
        btnFireImg.animation.play(nextBallColor);
        gameState = 0;
    }

    // This only happens if a safety kicks in
    private function restoreUserInput():Void
    {
        trace("WARNING: Safety kicked in");

        // Either way, we need a new currBall
        nextBallColor = getNextBallColor(null);
        resetCurrBall();

        btnInspectBg.visible = true;
        btnInspectImg.visible = true;
        btnInspectTxt.visible = true;
        btnFireBg.visible = true;
        btnFireImg.visible = true;
        btnFireTxt.visible = true;
        btnFireTxt.text = "Action";
        btnFireImg.animation.play(nextBallColor);
        gameState = 0;
    }

    private function snapCurrBallToGrid():Void
    {
        // Stop its movement.
        currBall.velocity.set(0,0);

        // Special case for hearts
        if (currBall.animation.name == 'heart') {
            disableSafeties = true;
            disableInput = true;
            disableForCutscenes = true;

            // WARNING: Make sure currBall isn't destroyed before the tween stops
            FlxTween.tween(currBall.scale, { x:100, y:100 }, 1.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: onHeartFullyZoomed });

            return; // TODO: What else   
        }

        // Fix it to the nearest grid location
        var best:GridCell = null;
        var bestAmt:Float = 0;
        for (row in grid) {
            for (col in row) {
                // TODO: Do we need to check there's no ball in this square?
                // 
                // NOTE: Since these are all the same size, we don't have to re-center them
                if (col.ball == null || col.ball.getType() == 'coin') { // Make sure no ball exists in this rect; otherwise, we can't use it.
                    var rect = col.rect;
                    var overlap = rawCircleOverlap(rect.x,rect.y,16,currBall.x,currBall.y,16);
                    //if (overlap > 0) {  // Normally we'd enforce this, but it's probably better to snap to *something*
                        if (overlap > bestAmt) {
                            best = col;
                            bestAmt = overlap;
                        }
                    //}
                }
            }
        }

        // Find the grid
        if (best == null) {
            // TODO: What in this case? Maybe kill the ball?
            trace("ERROR; can't snap to grid");
            currBall.kill();
            this.balls.remove(currBall);
        } else {
            currBall.x = best.rect.x;
            currBall.y = best.rect.y;
            best.ball = currBall;
        }

        // Either way, we need a new currBall
        nextBallColor = getNextBallColor(null);
        resetCurrBall();

        //////////////////
        // Reaction 
        //////////////////

        // Does this character have any bonuses?
        if (character.hopeful) {
            for (i in 0...3) {
                var hopeAmt = 1;
                character.emotionalState[i] = Math.floor(Math.max(0, character.emotionalState[i]-hopeAmt));
            }
        }

        // First, see if we've made a match of length 3+
        var matched3 = false;
        if (best!=null && best.ball != null) {
            var match = computeLongestMatch(best, best.ball.animation.name);
            if (match.length >= 3) {
                matchSound.play();
                matched3 = true;
                setupPointsToEmotionBars(match);
                for (ball in match) {
                    ball.hurt(1);
                }
            } else {
                // Play "put in place" sound
                placementSound.play();
            }
        }

        // Increase turn count
        turnCount += 1;

        // Second, see if balls have to drop as a result of being disconnected
        var toAdvance = (turnCount%character.ball_advance_modulo == 0);
        if (matched3) {
            var toDrop = dropDisconnectedBalls();
            dropBallSet(toDrop);
        } else {
            // If we didn't match anything, did we advance the screen too far?
            var res = checkScreenAdvancedTooFar();
            if (res.length > 0) {
                makeDropPenalty();
                dropBallSet(res);
                toAdvance = false; // They've suffered enough
            }

            if (character.riskLevel < 300) { character.riskLevel += 1; } // TODO: This seems right, but???

            updateEmotions(); // Ticks the "hopeful" changes
            updateRiskLevel(); // Same, if we had it.
        }

        // Check for victory in the event of coins, which are special
        if (!disableForCutscenes) {
            if (character.goal_count >= character.goal_requirement) {
                doAVictory();
            }
        }

        // Advance, unless they already had a penalty this turn
        if (toAdvance) {
            this.generateAndScroll(1);
        }

        // Override
        if (nextTurnHeart) {
            nextBallColor = 'heart';
            currBall.setType(nextBallColor);
            nextTurnHeart = false;
        }


        // Now re-enable our UI again
        // TODO: We *might* mess up the UI if we fire too fast when the balls are low?
        // (But we might have a popup at that point anyway)
        btnInspectBg.visible = true;
        btnInspectImg.visible = true;
        btnInspectTxt.visible = true;
        btnFireBg.visible = true;
        btnFireImg.visible = true;
        btnFireTxt.visible = true;
        btnFireTxt.text = "Action";
        btnFireImg.animation.play(nextBallColor);
        gameState = 0;
    }


    private function makeDropPenalty():Void
    {
        // Also give a big risk penalty
        // Right now we don't check lifelines or game overs (since we bound the damage)
        dangerLowSound.play();
        var spr = new FlxSprite();
        spr.loadGraphic(AssetPaths.small_bubbles__png, true, 16, 16);
        Ball.AddPointAnimations(spr.animation);
        spr.animation.play('white');
        spr.x = currBall.x+8;
        spr.y = currBall.y+8;
        this.add(spr);

        // Set up a tween to the risk meter
        var destX = riskMeter.x + riskMeter.width/2 - 8;
        var destY = riskMeter.y + riskMeter.height/2 - 12 + 3;
        FlxTween.tween(spr, {x:destX, y:destY }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: tweenDoneFadeDeleteObj.bind(spr) });

        var amt = 25;

        // Try to avoid crossing a boundary
        if (character.riskLevel < 100) {
            amt = Math.floor(Math.min(99 - character.riskLevel, amt));
        } else if (character.riskLevel < 200) {
            amt = Math.floor(Math.min(199 - character.riskLevel, amt));
        } else if (character.riskLevel < 300) {
            amt = Math.floor(Math.min(299 - character.riskLevel, amt));
        }

        // Deal the damage
        if (amt > 0) {
            var effect = [
                new TurnEffect('risk', 0, amt)
            ];

            //Delay a popup for that
            // TODO: This way of doing it is silly & wasteful --maybe we can attach it to the effect (a no-op first)?
            var spr = new FlxSprite();
            spr.x = -999;
            FlxTween.tween(spr, {x:-999 }, 1.0, { type: FlxTweenType.ONESHOT, onComplete: popUpEffects.bind(effect) });
        }
    }


    // Can also be good
    private function makeEventPenalty(amt:Int):Void
    {
        // Also give a big risk penalty
        // Right now we don't check lifelines or game overs (since we bound the damage)
        // TODO: SFX
        var spr = new FlxSprite();
        spr.loadGraphic(AssetPaths.small_bubbles__png, true, 16, 16);
        Ball.AddPointAnimations(spr.animation);
        spr.animation.play('white');
        spr.x = btnPopupOk.x+btnPopupOk.width/2-8;
        spr.y = btnPopupOk.y+btnPopupOk.height/2-8;
        this.add(spr);

        // Set up a tween to the risk meter
        var destX = riskMeter.x + riskMeter.width/2 - 8;
        var destY = riskMeter.y + riskMeter.height/2 - 12 + 3;
        FlxTween.tween(spr, {x:destX, y:destY }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: tweenDoneFadeDeleteObj.bind(spr) });

        // Deal the damage
        var effect = [
            new TurnEffect('risk', 0, amt)
        ];

        //Delay a popup for that
        // TODO: This way of doing it is silly & wasteful --maybe we can attach it to the effect (a no-op first)?
        var spr = new FlxSprite();
        spr.x = -999;
        FlxTween.tween(spr, {x:-999 }, 1.0, { type: FlxTweenType.ONESHOT, onComplete: popUpEffects.bind(effect) });
    }


    private function dropBallSet(toDrop:Array<Ball>):Void
    {
        // Try to stagger drops
        var maxY:Float = 0;
        for (ball in toDrop) {
            if (ball.y > maxY) {
                maxY = ball.y;
            }
        }

        var doVictory = false;
        var doEvent = false;
        for (ball in toDrop) {
            if (ball.getType() == 'book' || ball.getType() == 'cake') {
                // This one goes to the victory pile
                coinSound.play();
                character.goal_count += 1;
                moveGoalToPointsSection(ball);
                ball.kill();

                // Chapter over?
                if (character.goal_count >= character.goal_requirement) {
                    doVictory = true;
                }
            } else if (ball.animation.name == 'event') {
                // TODO: This becomes a cutscene popup
                dangerSound.play();
                ball.kill();
                doEvent = true;
            } else {
                // Try to figure out what row we're in, roughly
                var sd = 0.02 * Math.max(1, Math.min(8, Math.abs(ball.y-maxY)/ballAdjSize));
                FlxTween.tween(ball, { y:ball.y+500, alpha:0 }, 0.8+sd, { type: FlxTweenType.ONESHOT, onComplete: tweenDoneDeleteObj.bind(ball), startDelay:sd }); // ease: FlxEase.backIn,
            }
        }

        // Prefer victory
        if (doVictory) {
            doAVictory();
        } else if (doEvent) {
            currPopup = 3;
            btnPopupOkTxt.text = 'Confirm';
            FlxTween.tween(btnPopupOk, { alpha:1 }, 0.5, { type: FlxTweenType.ONESHOT, startDelay: 2.0 });
            FlxTween.tween(btnPopupOkTxt, { alpha:1 }, 0.5, { type: FlxTweenType.ONESHOT, startDelay: 2.0 });

            // Tween in our popup window
            lastEventWasBad = character.event_is_bad[character.event_count%character.event_is_bad.length];
            popupEventHappened.touch();
            popupEventHappened.y = popupOffscreenY;
            FlxTween.tween(popupEventHappened, { y:popupEventHappened.y }, 1.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: continueEventZoomAnim });

            character.event_count += 1;

            // Keep input disabled.
            disableInput = true;
            disableForCutscenes = true;
        }
    }

    private function doAVictory():Void
    {
        // Fade out our music
        FlxG.sound.music.fadeOut(1.0);

        currPopup = 1;
        btnPopupOkTxt.text = 'Next Story';
        FlxTween.tween(btnPopupOk, { alpha:1 }, 0.5, { type: FlxTweenType.ONESHOT, startDelay: 2.0 });
        FlxTween.tween(btnPopupOkTxt, { alpha:1 }, 0.5, { type: FlxTweenType.ONESHOT, startDelay: 2.0 });

        // Tween in our popup window
        popupChapterWon.y = popupOffscreenY;
        FlxTween.tween(popupChapterWon, { y:popupChapterWon.y }, 1.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: continuePopupAnim.bind(true) });

        // Keep input disabled.
        disableInput = true;
        disableForCutscenes = true;
    }


    private function continueEventZoomAnim(tween:FlxTween):Void
    {
        FlxTween.tween(popupEventHappened, { y: popupY }, 1.0, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: showPopupAnimDone });        
    }


    private function continuePopupAnim(victory:Bool, tween:FlxTween):Void
    {
        if (victory) {
            FlxG.sound.playMusic(AssetPaths.Victory__ogg, 1, true);
        } else {
            FlxG.sound.playMusic(AssetPaths.StoryOver__ogg, 1, true);
        }
        FlxTween.tween(popupChapterWon, { y: popupY }, 1.0, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: showPopupAnimDone });
        
    }

    private function showPopupAnimDone(tween:FlxTween):Void
    {
        disableInput = false;
    }

    private function resetCurrBall():Void
    {
        var ballSprSize = 32;
        var sX = playArea.x+playArea.width/2 - ballSprSize/2;
        var sY = playArea.y+playArea.height-ballMaxDragRadius - ballSprSize;
        currBall = new Ball().init(Math.floor(sX), Math.ceil(sY), nextBallColor);
        currBall.canGrab = true;
        this.balls.add(currBall);
        currBall.visible = false;
    }


    private function get_emotion_color(val:Int):Int
    {
        if (val<=130 && val>=70) {
            return EmotionColorYellow;
        } else if (val<=160 && val>=40) {
            return val < 100 ? EmotionColorGreen : EmotionColorOrange;
        } else {
            return val < 100 ? EmotionColorBlue : EmotionColorRed;
        }
        return EmotionColorYellow; // Shouldn't happen
    }

    private function get_risk_color(val:Int):Int
    {
        if (val < 100) {
            return val < 50 ? return EmotionColorBlue : EmotionColorGreen;
        } else if (val < 200) {
            return EmotionColorYellow;
        } else {
            return val < 250 ? return EmotionColorOrange : EmotionColorRed;
        }
    }


    private function updateEmotions():Void 
    {
        for (i in 0...3) {
            var val = character.emotionalState[i];

            // Update the value of each bar
            var obj = emotionValues[i];
            obj.visible = (val != 0);

            // Draw color and bar, but only if visible
            if (obj.visible) {
                // Which color do we want?
                var clr = get_emotion_color(val);

                // Now get the clip bounds
                var amt = Math.max(3, (val/200.0)*132); // Always show something.
                var startX = 0.0;
                var endX = amt;
                startX = Math.max(0, Math.min(132, startX));
                endX = Math.max(0, Math.min(132, endX));

                // NOTE: I feel like I don't understand offsets correctly, so this will have to do for now.
                obj.y = emotionValueStartY[i] - 20*clr;
                obj.clipRect = new FlxRect(startX, 20*clr, endX-startX, 20);
            }

            // Update the text
            var txt = emotionTexts[i];
            txt.text = character.emotions[i];
        }
    }


    private function updateRiskLevel():Void
    {
        var val = character.riskLevel;

        // Update the bar's value
        var obj = riskMeterValue;
        obj.visible = (val != 0);

        // Draw color and bar, but only if visible
        if (obj.visible) {
            // Which color do we want?
            var clr = get_risk_color(val);

            // Now get the clip bounds
            var amt = Math.max(3, (val/300.0)*111); // Always show something.
            var startX = 0.0;
            var endX = amt;
            startX = Math.max(0, Math.min(11, startX));
            endX = Math.max(0, Math.min(111, endX));

            // NOTE: I feel like I don't understand offsets correctly, so this will have to do for now.
            obj.y = riskValueStartY - 28*clr;
            obj.clipRect = new FlxRect(startX, 28*clr, endX-startX, 28);
        }

        // Markers update once we pass them *once*. 
        // This does trigger an event next turn (but we do that elsewhere)
        var thresh = [100, 200, 300];
        for (i in 0...3) {
            var mark = riskMarkers[i];
            if (val >= thresh[i]) {
                mark.animation.play('active');
            }
        }
    }

    private function updateGoal():Void
    {
        goalItem.animation.play(character.goal_anim+character.goal_count);
    }
                    

    public static function Dist(x1:Float, y1:Float, x2:Float, y2:Float):Float 
    {
        var dX = (x2-x1);
        var dY = (y2-y1);
        return Math.sqrt(dX*dX + dY*dY);
    }

    public static function computeCircleOverlap(obj1:FlxObject, obj2:FlxObject):Float
    {
        // Some assumptions
        if (obj1.width!=obj1.height || obj2.width!=obj2.height) {
            throw "computeCircleOverlap() requires width==height for each object";
        }

        // We don't want items in a queue (moving at the same speed) to trigger collisions.
        var o1dx:Float = obj1.x - obj1.last.x;
        var o2dx:Float = obj2.x - obj2.last.x;
        var o1dy:Float = obj1.y - obj1.last.y;
        var o2dy:Float = obj2.y - obj2.last.y;
        if (o1dx==o2dx && o1dy==o2dy) { return 0; }

        // Note: For now we just do a simple overlap calculation that doesn't take edges into account.
        // At this point, the Quads test is already filtering out bad options, so we don't need another bounding box calculation.
        var c1x = obj1.x + obj1.width/2;
        var c1y = obj1.y + obj1.height/2;
        var c2x = obj2.x + obj2.width/2;
        var c2y = obj2.y + obj2.height/2;
        return rawCircleOverlap(c1x,c1y,obj1.width/2,c2x,c2y,obj2.width/2);
    }

    public static function rawCircleOverlap(c1x:Float, c1y:Float, c1Radius:Float, c2x:Float, c2y:Float, c2Radius:Float):Float
    {
        var dist = Dist(c1x, c1y, c2x, c2y);
        var res = Math.max(0, c1Radius + c2Radius - dist);
        return res;
    }

    // "radius" just means that 2x radius is assumed to be the width/height of the point in space
    public static function rectangleCursorOverlayRaw(rectSpr:FlxRect, pos:FlxPoint, radius:Float) {
        var r2 = new FlxRect(pos.x-radius, pos.y-radius, 2*radius, 2*radius);
        return rectSpr.overlaps(r2);
    }
    public static function rectangleCursorOverlay(rectSpr:FlxSprite, pos:FlxPoint, radius:Float) {
        var r1 = new FlxRect(rectSpr.x, rectSpr.y, rectSpr.width, rectSpr.height);
        return rectangleCursorOverlayRaw(r1, pos, radius);
    }

    public static function circle_separate(obj1:Ball, obj2:Ball):Bool
    {
        // can't separate two immovable objects
        if (obj1.immovable && obj2.immovable) {
            return false;
        }

        // Can't deal with objects that are dead
        if (obj1.dying || obj2.dying) {
            return false;
        }

        var o1dx:Float = obj1.x - obj1.last.x;
        var o2dx:Float = obj2.x - obj2.last.x;
        var o1dy:Float = obj1.y - obj1.last.y;
        var o2dy:Float = obj2.y - obj2.last.y;

        // Calculate and deal with overlap.
        // NOTE: We're assuming 100% elasticity, and both movable (for simplicity)
        // For now, just move things back by 1/4 steps until they stop touching.
        //
        // TODO: This can jam up easily!
        //
        var overlapOnce = false;
        var amt = 4;
        for (i in 0...amt) {
            var overlap:Float = computeCircleOverlap(obj1, obj2);
            if (overlap == 0) { break; }
            overlapOnce = true;

            // Don't move back for coins
            if (obj1.isCoin() || obj2.isCoin()) { break; }

            // NOTE: We deal with overlaps by forcing to a grid
            if (true) { break; }

            // Increment
            obj1.x -= o1dx/amt;
            obj1.y -= o1dy/amt;
            obj2.x -= o2dx/amt;
            obj2.y -= o2dy/amt;
        }

        // Now, transfer velocities and set angles.
        if (overlapOnce) {
            if (false) { // NOTE: We don't flip velocities any more, but here's the code if you want it.
                if (!obj1.isCoin() && !obj2.isCoin()) {
                    // Give a "boost" on hitting a stationary object.
                    if (obj1.velocity.x==0 && obj1.velocity.y==0) {
                        obj1.velocity.set(-1*obj2.velocity.x, -1*obj2.velocity.y);
                    }
                    if (obj2.velocity.x==0 && obj2.velocity.y==0) {
                        obj2.velocity.set(-1*obj1.velocity.x, -1*obj1.velocity.y);
                    }

                    // Now flip velocities.
                    var o1x = obj1.velocity.x;
                    var o1y = obj1.velocity.y;
                    obj1.velocity.set(obj2.velocity.x, obj2.velocity.y);
                    obj2.velocity.set(o1x, o1y);
                }
            }

            return true;
        }

        return false;
    }

    public static function circle_notify(obj1:Ball, obj2:Ball):Void
    {
        // Note; Ignore dying objects (shouldn't happen)
        if (obj1.dying || obj2.dying) { return; }

        // Safety: count overlap frames, and kill in the event of multi-frame overlap.
        obj1.overlapCurrFrame = true;
        obj1.overlapStreak += 1;
        if (obj1.overlapStreak >= 30) {
            /*if (obj1 == currBall || obj2 == currBall) {
                restoreUserInput();
            }*/ // We'll have to do without this for now (unlikely to take effect)

            obj1.hurt(1);
            obj2.hurt(1);
        }

        // Deal with coins
        // NOTE: This sets "just collected coin"
        if (obj1.collectCoin() || obj2.collectCoin()) {
            return;
        }

        // Note: this is kind of a hack
        obj1.justCollided = true;
        obj2.justCollided = true;


        // NOTE: This will sticky them, but we probably want to put them in a grid.
        //obj1.velocity.set(0,0);
        //obj2.velocity.set(0,0);

        //trace("OVERLAP: " + obj1.overlapStreak);
    }

    override public function update(elapsed:Float):Void
    {
        if (firstTurn) {
            firstTurn = false;
            MultiPlatformUtils.CenterFullscreenMobile(this);
        }

    
        var minX = playArea.x;
        var maxX = minX+playArea.width;
        var minY = playArea.y;
        var maxY = minY+playArea.height;

        // Bounce off walls
        // TODO: Better way to get frame bounds?
        if (!disableSafeties) {
            var playBounce = false;
            for (ball in balls) {
                if (ball.alive) {
                    if (!ball.overlapCurrFrame) {
                        ball.overlapStreak = 0;
                    }
                    ball.overlapCurrFrame = false;

                    if (ball.x<minX || ball.x+ball.width>=maxX) {
                        if (ball == currBall) {
                            playBounce = true;
                        }
                        ball.x = Math.max(ball.x, minX);
                        ball.x = Math.min(ball.x, maxX-ball.width);
                        ball.velocity.set(ball.velocity.x*-1, ball.velocity.y);
                        ball.bounceCount += 1;
                    }
                    if (ball.y<minY || ball.y+ball.height>=maxY) {
                        if (ball == currBall) {
                            playBounce = true;
                        }
                        ball.y = Math.max(ball.y, minY);
                        ball.y = Math.min(ball.y, maxY-ball.height);
                        ball.velocity.set(ball.velocity.x, ball.velocity.y*-1);
                        ball.bounceCount += 1;
                    }

                    // Collisions don't count if you're dragging.
                    if (draggingBall) {
                        if (ball == currBall) {
                            playBounce = false;
                        }
                        ball.bounceCount = 0;
                    }

                    // Another infinite loop safety
                    if (ball.bounceCount>5) {
                        ball.kill();
                        this.balls.remove(ball);
                        if (ball == currBall) {
                            restoreUserInput();
                        }
                    }
                }
            }
            if (playBounce) {
                //bounceSound.play();
                placementSound.play(); // Less annoying
            }
        }

        // Move the balls and the grid
        if (gridToScroll > 0) {
            var incr = 1; // Note: This works for any speed (but it's best to use multiples of ~30)
            var nextAmt = Math.min(incr, gridToScroll-gridDoneScroll); // Let's say
            gridDoneScroll += Math.floor(nextAmt);

            // Update: Grid
            for (row in grid) {
                for (col in row) {
                    col.rect.y += nextAmt;

                    // Update the balls via their "holder" grid cells to make certain errors more obvious
                    if (col.ball != null) {
                        col.ball.y += nextAmt;
                    }
                }
            }

            // Done!
            if (gridDoneScroll >= gridToScroll) {
                // One-time
                gridToScroll = 0;
                gridDoneScroll = 0;

                // Pop the old row and push a new one
                var lastRow = grid.pop();

                // TODO: Centralize
                var nextRowBig = grid[0].length == cols;
                var count = nextRowBig ? cols -1 : cols;
                var currX = grid[0][0].rect.x + (nextRowBig ? 1 : -1) * Math.floor(ballSize/2);
                var currY = grid[0][0].rect.y - ballAdjSize;
                var row = new Array<GridCell>();
                grid.unshift(row);

                // Generate row
                for (x in 0...count) {
                    var rect = new FlxRect(currX, currY, ballSize, ballSize);
                    row.push(new GridCell(rect));
                    currX += ballSize;
                }
                // END TODO

                var penaltyBalls = new Array<Ball>();
                for (cell in lastRow) {
                    if (cell.ball != null) {
                        penaltyBalls.push(cell.ball);
                    }
                }
                if (penaltyBalls.length>0) {
                    var res = checkScreenAdvancedTooFar();
                    for (ball in res) {
                        penaltyBalls.push(ball);
                    }

                    // Ok, react
                    makeDropPenalty();
                    dropBallSet(penaltyBalls);

                    // Wait 1 second, just in case
                    var spr = new FlxSprite();
                    spr.y = -999;
                    FlxTween.tween(spr, {y:-999 }, 1.0, { type: FlxTweenType.ONESHOT, onComplete: reEnableInputAfterDrop });
                } else {
                    disableInput = false;
                    disableSafeties = false;
                }
                // TODO: Detect if we popped a row that extended into the lower area
                // TODO: lastRow

// TODO: Keep input disabled a little longer in case of penalty
            }
        }

        // Make sure our grid is kep up-to-date w.r.t. dying/dead balls
        // This also removes them from the "balls" list, which is our form of GC
        for (row in grid) {
            for (cell in row) {
                if (cell.ball != null) {
                    if (!cell.ball.alive) {
                        this.balls.remove(cell.ball);
                        cell.ball = null;
                    }
                }
            }
        }


// DEBUG
//this.handleDebugInput();
// END DEBUG


        this.handleMouseInput();
        super.update(elapsed);

        // Do object-object collisions
        if (!disableSafeties) {
            FlxG.overlap(balls, balls, PlayState.circle_notify, PlayState.circle_separate);

            if (currBall.justCollided) {
                snapCurrBallToGrid();
                currBall.justCollided = false;
            }
        }

        // Spawn goal icons for each coin we collected (can be multiple)
        var playCoinSound = false;
        for (row in grid) {
            for (cell in row) {
                if (cell.ball != null) {
                    if (cell.ball.justCollectedCoin) {
                        playCoinSound = true;
                        character.goal_count += 1;
                        moveGoalToPointsSection(cell.ball);
                        cell.ball.justCollectedCoin = false;
                    }
                }
            }
        }
        if (playCoinSound) {
            coinSound.play();
        }
    }

    private function reEnableInputAfterDrop(tween:FlxTween):Void
    {
        disableInput = false;
        disableSafeties = false;
    }


    private function handleDebugInput():Void
    {
        if (disableInput) { return; }
        if (disableForCutscenes) { return; }

#if FLX_NO_KEYBOARD
#else
        // Scroll rows
        if (FlxG.keys.justPressed.T) {
            this.generateAndScroll(1);
        }
#end
    }

    private function gameEventPopupDone(tween:FlxTween):Void 
    {
        disableInput = false;
        disableForCutscenes = false;
    }

    private function gamePopupDone(tween:FlxTween):Void
    {
        disableInput = false;
        disableForCutscenes = false;
    }


    private function handleMouseInput():Void
    {
        var inputStuff = MultiPlatformUtils.ComputePseudoMouse();

        var mousePos:FlxPoint = inputStuff.mousePos;
        var wasPressed:Bool = inputStuff.justPressed;
        var wasReleased:Bool = inputStuff.justReleased;

        // Avoid nulls; just put the mouse offscreen in that case and be done with it.
        if (mousePos == null) {
                mouseSprite.setPosition(-999,-999);

                // Default rotate
                if (targetSprite.visible) {
                    targetSprite.scale.set(4,4);
                    targetSprite.angle = (targetSprite.angle + 1)%360;
                }

                return;
        }

        // Keep the mouse in line.
        mouseSprite.setPosition(mousePos.x-19, mousePos.y-19);

        // Skip the rest if input is disabled
        // TODO: It seems that mouse input should really only be disabled on "release".
        if (disableInput) { return; }

        // TODO: Check a single button in this case.
        if (disableForCutscenes) {
            if (wasPressed) {
                if (btnPopupOk.visible && rectangleCursorOverlay(btnPopupOk, mousePos, 2)) {
                    btnDown = 99;
                    btnPopupOk.animation.play("down");
                    btnPopupOkTxt.scale.set(0.9, 0.9);
                    btnPopupOkTxt.alpha = 0.5;
                }
                if (btnDown != 0) {
                    clickSound.play();
                }
            }

            if (wasReleased) {
                if (btnDown == 99) {
                    btnPopupOk.animation.play("up");
                    btnPopupOkTxt.scale.set(1, 1);
                    btnPopupOkTxt.alpha = 1;

                    if (rectangleCursorOverlay(btnPopupOkTxt, mousePos, 2)) {
                        if (currPopup == 1) {
                            // Fade out the BG music
                            FlxG.sound.music.fadeOut(1.0);

                            // Fade (in a black rectangle) so this looks nicer
                            FlxTween.tween(fadeToBlack, { alpha:1 }, 1.0, { type: FlxTweenType.ONESHOT, onComplete: prepareForNextLevel });
                        } else if (currPopup == 2) {
                            // Move the window down
                            disableInput = true;

                            // Put the window back
                            FlxTween.tween(popupReceivedHeart, { y: popupOffscreenY2 }, 0.7, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: gamePopupDone });
                            currPopup = 0;

                            // And fade
                            FlxTween.tween(btnPopupOk, { alpha: 0.0 }, 0.2, { type: FlxTweenType.ONESHOT});
                            FlxTween.tween(btnPopupOkTxt, { alpha: 0.0 }, 0.2, { type: FlxTweenType.ONESHOT});
                        } else if (currPopup == 3) {
                            // Move the window down
                            disableInput = true;

                            // Put the window back
                            FlxTween.tween(popupEventHappened, { y: popupOffscreenY2 }, 0.7, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: gameEventPopupDone });
                            currPopup = 0;

                            // And fade
                            FlxTween.tween(btnPopupOk, { alpha: 0.0 }, 0.2, { type: FlxTweenType.ONESHOT});
                            FlxTween.tween(btnPopupOkTxt, { alpha: 0.0 }, 0.2, { type: FlxTweenType.ONESHOT});

                            // Also, show the effect on our risk
                            var riskAmt = 0;
                            if (lastEventWasBad) {
                                if (character.riskLevel < 100) {
                                    riskAmt = 99 - character.riskLevel;
                                } else if (character.riskLevel < 200) {
                                    riskAmt = 199 - character.riskLevel;
                                } else if (character.riskLevel < 300) {
                                    riskAmt = 299 - character.riskLevel;
                                }
                            } else {
                                riskAmt = -50;
                            }
                            if (riskAmt != 0) {
                                makeEventPenalty(riskAmt);
                            }
                        }
                    }
                } 
            }
            
            // Either way, eat the input
            return;
        }

        // Update our highlight of "what can we ask about?"
        if (gameState == 2) {
            if (handleInspectClick(mousePos, false)) {
                mouseSprite.animation.play("mouse_help_info");
            } else {
                mouseSprite.animation.play("mouse_help");
            }
        }


        if (wasPressed) {
            // Special case: If the "Inspect" window takes precedence, then skip further processing
            if (gameState == 2) {
                if (!rectangleCursorOverlay(btnInspectBg, mousePos, 2)) { // Need to always allow them to cancel out.
                    if (handleInspectClick(mousePos, true)) {
                        clickSound.play();
                        return;
                    }
                }
            }

            // Detect if we're pressing on the current ball.
            mouseDownPos = mousePos;
            if (currBall.canGrab) {
                ballStartPos = new FlxPoint(currBall.x, currBall.y);
                if (!currBall.dying && currBall.visible) { // TODO: Eventually we don't want to allow mid-air grabs, so this is easier.
                    if (rawCircleOverlap(mouseDownPos.x,mouseDownPos.y,20,currBall.x+currBall.width/2,currBall.y+currBall.height/2,currBall.width/2) > 0) {
                        drawSound.play();
                        currBall.velocity.set(0,0);
                        mouseSprite.animation.play("mouse_grab");
                        draggingBall = true;
                    }
                }
            }

            // Check our buttons
            if (btnInspectBg.visible && rectangleCursorOverlay(btnInspectBg, mouseDownPos, 2)) {
                btnDown = 1;
                btnInspectBg.animation.play("down");
                btnInspectTxt.scale.set(0.9, 0.9);
                btnInspectTxt.alpha = 0.5;
                btnInspectImg.scale.set(0.9, 0.9);
                btnInspectImg.alpha = 0.5;
            } else if (btnFireBg.visible && rectangleCursorOverlay(btnFireBg, mouseDownPos, 2)) {
                btnDown = 2;
                btnFireBg.animation.play("down");
                btnFireTxt.scale.set(0.9, 0.9);
                btnFireTxt.alpha = 0.5;
                btnFireImg.scale.set(0.9, 0.9);
                btnFireImg.alpha = 0.5;
                if (gameState != 1) {
                    btnFireImg.animation.play(nextBallColor + "_static");
                }
            } else {
                btnDown = 0;
            }

            if (btnDown != 0) {
                clickSound.play();
            }
        }

        if (wasReleased) {
            if (draggingBall) {
                // Scale max speed to components
                // TODO: Centralize in a class (is there already a vector class?)
                var ballDiffX:Float = mouseDownPos.x-mousePos.x;
                var ballDiffY:Float = mouseDownPos.y-mousePos.y;
                var ballDiffMagnitude:Float = Dist(0,0,ballDiffX,ballDiffY);
                var scale:Float = currBall.maxSpd/ballDiffMagnitude;
                ballDiffX *= scale;
                ballDiffY *= scale;

                // This always happens regardless
                mouseSprite.animation.play("mouse");
                draggingBall = false;

                // No effect if too small
                if (ballDiffMagnitude <= 20) {
                    currBall.x = ballStartPos.x;
                    currBall.y = ballStartPos.y;
                    targetSprite.setPosition(currBall.x+currBall.width/2-targetSprite.width/2, currBall.y+currBall.height/2-targetSprite.height/2);
                    return;
                } else {
                    releaseSound.play();
                    currBall.velocity.set(ballDiffX, ballDiffY);
                    currBall.canGrab = false;
                    startRestoreFireAction();
                    targetSprite.visible = false;
                }                
            }

            // Make sure we're still over the button on release.
            if (btnDown == 1) {
                btnInspectBg.animation.play("up");
                btnInspectTxt.scale.set(1, 1);
                btnInspectTxt.alpha = 1;
                btnInspectImg.scale.set(1, 1);
                btnInspectImg.alpha = 1;

                if (rectangleCursorOverlay(btnInspectBg, mousePos, 2)) {
                    if (gameState == 0) {
                        startInspect();
                    } else if (gameState == 2) {
                        startCancelInspect();
                    }
                }
            } else if (btnDown == 2) {
                btnFireBg.animation.play("up");
                btnFireTxt.scale.set(1, 1);
                btnFireTxt.alpha = 1;
                btnFireImg.scale.set(1, 1);
                btnFireImg.alpha = 1;

                if (rectangleCursorOverlay(btnFireBg, mousePos, 2)) {
                    if (gameState == 0) {
                        startAction();
                    } else if (gameState == 1) {
                        startCancelAction();
                    }
                }
            }

            btnDown = 0;
        }

        // Keep the dragged ball in line.
        // NOTE: This also moves the target cursor
        if (draggingBall) {
            // TODO: Bound to a circle.
            var ballDiffX:Float = mousePos.x-mouseDownPos.x;
            var ballDiffY:Float = mousePos.y-mouseDownPos.y;
            var ballDiffMagnitude:Float = Dist(0,0,ballDiffX,ballDiffY);
            if (ballDiffMagnitude > ballMaxDragRadius) {
                var scale:Float = ballMaxDragRadius/ballDiffMagnitude;
                ballDiffX *= scale;
                ballDiffY *= scale;
            }

            currBall.setPosition(ballStartPos.x+ballDiffX, ballStartPos.y+ballDiffY);
            targetSprite.setPosition(mouseDownPos.x-ballDiffX-targetSprite.width/2, mouseDownPos.y-ballDiffY-targetSprite.height/2);

            // Special rotate!
            if (targetSprite.visible) {
                var dst = Dist(0,0,ballDiffX, ballDiffY) / 100.0;

                // Use this to rotate faster as we are farther away 
                var newScale = (1-dst)*3+1;
                targetSprite.scale.set(newScale, newScale);
                targetSprite.angle = (targetSprite.angle + 2 + dst*2)%360;
            }

        } else {
            // Default rotate
            if (targetSprite.visible) {
                targetSprite.scale.set(4,4);
                targetSprite.angle = (targetSprite.angle + 1)%360;
            }
        }

    }


    private function prepareForNextLevel(tween:FlxTween):Void
    {
        FlxTween.tween(fadeToBlack, { alpha:1 }, 0.5, { type: FlxTweenType.ONESHOT, onComplete: transferToNextLevel });
    }

    private function transferToNextLevel(tween:FlxTween):Void
    {
        if (character.nextChara == 1) {
            FlxG.switchState(new LevelSelect(CharacterMaker.Mia_Story1()));
        } else if (character.nextChara == 2) {
            FlxG.switchState(new LevelSelect(CharacterMaker2.Francis_Story1()));
        } else if (character.nextChara == 3) {
            FlxG.switchState(new LevelSelect(CharacterMaker3.Mia_Story2()));
        } else {
            FlxG.switchState(new LevelSelect(CharacterMaker.GameDone()));
        }
    }


    // Helper: Hides windows correctly for showing the current help one.
    private function showHelpWindow(win:FlxSpriteGroup)
    {
        bgDescGroup.forEach(function(spr:FlxSprite) { spr.visible = false; } );
        bgDescSpr.visible = true;
        win.visible = true;
    }


    // Update our help text based on what was clicked.
    // Returns true if something was processed
    // "Do Action" does not perform any processing if false
    private function handleInspectClick(pos:FlxPoint, doAction:Bool):Bool
    {
        // Check the fire button
        // NOTE: We *must not* check the inspect button, since we need it for the "cancel" action.
        if (rectangleCursorOverlay(btnFireBg, pos, 2)) {
            if (doAction) {
                winDescCurrTurn.setBallColor(nextBallColor);
                showHelpWindow(winDescCurrTurn);
            }
            return true;
        }

        // Do hearts here, since they overlap the face
        var r1 = new FlxRect(hearts[0].x, hearts[0].y, hearts[hearts.length-1].x+hearts[hearts.length-1].width-hearts[0].x, hearts[0].height);
        if (rectangleCursorOverlayRaw(r1, pos, 2)) {
            if (doAction) {
                showHelpWindow(winDescHearts);
            }
            return true;
        }

        // Check the suicide risk bar, it technically overlaps the chara name
        r1 = new FlxRect(riskMeter.x, riskMeter.y+4, riskMeter.width, riskMarkers[0].y+riskMarkers[0].height-riskMeter.y-4);
        if (rectangleCursorOverlayRaw(r1, pos, 2)) {
            if (doAction) {
                showHelpWindow(winDescRisk);
            }
            return true;
        }

        // Check the character's "face" icon
        if (rectangleCursorOverlay(faceSpr, pos, 2) || rectangleCursorOverlay(playerName, pos, 2)) {
            if (doAction) {
                winDescCharacter.touch();
                showHelpWindow(winDescCharacter);
            }
            return true;
        }

        // Check the emotion bars
        for (i in 0...3) {
            var bar = emotionBars[i];
            r1 = new FlxRect(bar.x, bar.y-16, bar.width, bar.height+16+10);
            if (rectangleCursorOverlayRaw(r1, pos, 2)) {
                if (doAction) {
                    winDescEmotionBar.setBarId(i);
                    showHelpWindow(winDescEmotionBar);
                }
                return true;
            }
        }

        // Check the emotion hints
        for (i in 0...3) {
            for (ball in emotionHints[i]) {
                r1 = new FlxRect(ball.x, ball.y, ball.width+3, ball.height+1);
                if (rectangleCursorOverlayRaw(r1, pos, 2)) {
                    if (doAction) {
                        var ballColor = ball.animation.name.split("_")[0];
                        var bonus = ball.getBonus();
                        winDescEmotionHint.setBallAndBonus(ballColor, bonus);
                        showHelpWindow(winDescEmotionHint);
                    }
                    return true;
                }
            }
        }

        // Check their properties
        for (i in 0...3) {
            var prp = personProps[i];
            var bonus = prp.getBonus();
            r1 = new FlxRect(prp.x-6, prp.y-6, prp.width+6+6, prp.height+6+6);
            if (rectangleCursorOverlayRaw(r1, pos, 2)) {
                if (doAction) {
                    winDescPersonalProperty.setPropVal(bgStatsGroup, i); // WHY??? isn't this bgDescGroup? (but it doesn't show then)
                    showHelpWindow(winDescPersonalProperty);
                }
                return true;
            }
        }

        // Check the goal
        if (rectangleCursorOverlay(goalItem, pos, 2)) {
            if (doAction) {
                winDescGoal.touch();
                showHelpWindow(winDescGoal);
            }
            return true;
        }

        // Check each ball; we want to be exact with this, so we track overlap amount
        var bestBall:Ball = null;
        var bestBallOverlap:Float = 0;
        balls.forEachAlive(function(ball:Ball) {
            if (!doAction && bestBall != null) { return; }

            var overlap = rawCircleOverlap(pos.x,pos.y,1,ball.x+ball.width/2,ball.y+ball.height/2,ball.width/2);
            if (overlap > 0) {
                if (doAction) {
                    if (overlap > bestBallOverlap) {
                        bestBall = ball;
                        bestBallOverlap = overlap;
                    }
                } else {
                    // If we're not acting, we can exit early
                    bestBall = ball;
                }
            }
        } );
        if (bestBall != null) {
            if (doAction) {
                winDescBall.setBall(bestBall.animation.name);
                showHelpWindow(winDescBall);
            }
            return true;
        }
        

        return false;
    }




    // Kick off the "inspect" animation, etc.
    private function startInspect():Void
    {
        disableInput = true;
        gameState = 2;
        mouseSprite.animation.play("mouse_help");

        // Move the new middle bar into place
        bgDescGroup.visible = true;
        showHelpWindow(winDescHelp);
        var destY = 302;
        FlxTween.tween(btnGroup, { y: -destY }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        FlxTween.tween(bgDescGroup, { y: 0 }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: startInspectAnimDone });
    }

    private function startInspectAnimDone(tween:FlxTween):Void
    {
        // Make this a cancel button
        btnInspectTxt.text = "Cancel";
        btnInspectImg.animation.play("cancel");

        disableInput = false;
    }

    private function startCancelInspect():Void
    {
        disableInput = true;
        gameState = 0;
        mouseSprite.animation.play("mouse");

        var destY = 302;
        FlxTween.tween(btnGroup, { y: 0 }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut });
        FlxTween.tween(bgDescGroup, { y: destY }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: startCancelInspectAnimDone });
    }

    private function startCancelInspectAnimDone(tween:FlxTween):Void
    {
        btnInspectTxt.text = "Inspect";
        btnInspectImg.animation.play("magnifying");
        bgDescGroup.visible = false;

        // Since it might have updated in the mean time
        //updateEmotions();

        disableInput = false;
    }



    private function startAction():Void
    {
        disableInput = true;
        gameState = 1;

        // Hide the other action
        btnInspectBg.visible = false;
        btnInspectImg.visible = false;
        btnInspectTxt.visible = false;

        var destY = 194;
        FlxTween.tween(btnGroup, { y:destY }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut });
        FlxTween.tween(bgStatsGroup, { y:destY }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: startActionAnimDone });
    }

    private function startActionAnimDone(tween:FlxTween):Void
    {
        // Make this a cancel button
        btnFireTxt.text = "Cancel";
        btnFireImg.animation.play("cancel");

        targetSprite.setPosition(currBall.x+currBall.width/2-targetSprite.width/2, currBall.y+currBall.height/2-targetSprite.height/2);
        targetSprite.visible = true;
        currBall.visible = true;

        // TESTING:
        targetSprite.scale.set(4,4);

        disableInput = false;
    }


    private function startCancelAction():Void
    {
        disableInput = true;
        gameState = 0;
        currBall.visible = false;
        targetSprite.visible = false;

        var destY = 0;
        FlxTween.tween(btnGroup, { y:destY }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        FlxTween.tween(bgStatsGroup, { y:destY }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: startCancelActionAnimDone });
    }

    private function startCancelActionAnimDone(tween:FlxTween):Void
    {
        btnFireTxt.text = "Action";
        btnFireImg.animation.play(nextBallColor);

        // Show the other action
        btnInspectBg.visible = true;
        btnInspectImg.visible = true;
        btnInspectTxt.visible = true;

        // Since it might have updated in the mean time
        updateEmotions();

        disableInput = false;
    }

    // This one's used for when we are firing
    private function startRestoreFireAction():Void
    {
        disableInput = true;
        gameState = 3;

        // Hide *both* actions (even though inspect should already be hidden)
        btnInspectBg.visible = false;
        btnInspectImg.visible = false;
        btnInspectTxt.visible = false;
        btnFireBg.visible = false;
        btnFireImg.visible = false;
        btnFireTxt.visible = false;

        var destY = 0;
        FlxTween.tween(btnGroup, { y:destY }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        FlxTween.tween(bgStatsGroup, { y:destY }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn, onComplete: startRestoreFireActionAnimDone });
    }

    private function startRestoreFireActionAnimDone(tween:FlxTween):Void
    {
        btnFireTxt.text = "Action";
        btnFireImg.animation.play(nextBallColor);

        // Since it might have updated in the mean time
        updateEmotions();

        disableInput = false;
    }



}


