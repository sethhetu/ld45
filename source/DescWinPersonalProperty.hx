package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text for the character
class DescWinPersonalProperty extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Current ball
	private var obj1:BonusSprite;
	private var obj2:BonusSprite;

	// Effects (property balls)
	private var prop1:FlxSprite;

	// Which emotion are we shoing?
	//private var emotionBarId:Int = 0;

	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines(propId:Int):Array<String>
	{
		var res = character.property_descriptions[propId];
		return res;
	}


	public function new(character:Character)
	{
		super();
		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		this.add(title);

		obj1 = new BonusSprite();
		obj1.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj1.animation);
		obj1.x = fullWidth/2 - 200;
		obj1.y = 16;
		this.add(obj1);

		obj2 = new BonusSprite();
		obj2.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj2.animation);
		obj2.x = fullWidth/2 + 200;
		obj2.y = 16;
		this.add(obj2);

		prop1 = new FlxSprite();
		prop1.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(prop1.animation);
		prop1.x = 72;
		prop1.y = 20;
		this.add(prop1);

		var myX = 60;
		var myY = 50;
		textLines = new Array<FlxText>();
		for (line in get_lines(0)) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}
	}

	public function setPropVal(chevrons:FlxSpriteGroup, propId:Int):Void
	{
		// Update the title text 
		title.text = character.properties[propId];

		// Note: This is probably the worst way to do this...
		var throwAway = new FlxText();
		throwAway.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
		throwAway.text = title.text;
		var offsetX = throwAway.width/2 + 16;

		// Update Icons
		for (obj in [obj1,obj2]) {
			obj.animation.play(character.property_anims[propId]);

			// TODO: For some reason, this isn't displaying correctly... see comments in PlayState about the right chevron group
			//       It's something weird to do with the group, and it doesn't affec the other (bg) group for some reason.
			//obj.setBonus(chevrons, character.property_bonuses[propId]);

			obj.x = Math.floor(fullWidth/2 - offsetX);
			offsetX *= -1;
			offsetX -= 9; // Not sure why...
		}

		// Update the example text
		var finalY = 0;
		var i = 0;
		for (line in get_lines(propId)) {
			textLines[i].text = line;
			if (line.length>0) {
				finalY = Math.floor(textLines[i].y);
			}
			i += 1;
		}

		// Update property balls
		var propBall = character.property_balls[propId];
		prop1.y = finalY + 4;
		if (propBall.length > 0) {
			prop1.animation.play(propBall);
			prop1.x = 92;
		} else {
			prop1.x = -999;
		}
	}

}