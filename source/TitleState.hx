package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.math.FlxRect;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.system.FlxSound;
import flixel.tweens.FlxTween;
import flixel.group.FlxSpriteGroup;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;


class TitleState extends FlxState
{
    // Our scenic background
    var bgScenicSpr:FlxSprite;

    // Our backgrounds sprite (overlay on scenic)
    var bgSpr:FlxSprite;

    // Our title
    var titleSpr:FlxSprite;

    // Our 3 buttons
    var btnStart:FlxSprite;
    var btnStartTxt:FlxText;
    var btnHowTo:FlxSprite;
    var btnHowToTxt:FlxText;
    var btnContent:FlxSprite;
    var btnContentTxt:FlxText;
    //4?
    var btnCredits:FlxSprite;
    var btnCreditsTxt:FlxText;

    // Where do they belong (after tweening, etc).
    var btn1X:Int;
    var btn2X:Int;
    var btn3X:Int;
    var btn4X:Int;
    var btn1XTxt:Int;
    var btn2XTxt:Int;
    var btn3XTxt:Int;
    var btn4XTxt:Int;

    // Control for them
    var btnDown:Int = 0;


    // Our tutorial and content warning windows
    var tutorialWindow:PopupWinTutorial;

    // And where does this want to go?
    var tutY:Int; // Onscreen
    var tutOffscreenY:Int; // Offscreen, top
    var tutOffscreenY2:Int; // Offscreen, bottom


    // Same for Content Warning
    var contentWindow:PopupWinContentWarning;

    // And where does this want to go?
    var contentY:Int; // Onscreen
    var contentOffscreenY:Int; // Offscreen, top
    var contentOffscreenY2:Int; // Offscreen, bottom


    // Same for Credits
    var creditsWindow:PopupWinCredits;
    var creditsY:Int; // Onscreen
    var creditsOffscreenY:Int; // Offscreen, top
    var creditsOffscreenY2:Int; // Offscreen, bottom


    // Close the tutorial window
    var btnClose:FlxSprite;
    var btnCloseImg:FlxSprite;

    // 3 page windows
    var btnCredPage1:FlxSprite;
    var btnCredPage1Txt:FlxText;
    var btnCredPage2:FlxSprite;
    var btnCredPage2Txt:FlxText;
    var btnCredPage3:FlxSprite;
    var btnCredPage3Txt:FlxText;

    // Where to put the numbered points
    var credPage1Pt:FlxPoint;
    var credPage2Pt:FlxPoint;
    var credPage3Pt:FlxPoint;


    // Where to put the close button for each window
    var closeTutorialPt:FlxPoint;
    var closeTutorialImgPt:FlxPoint;
    var closeContentPt:FlxPoint;
    var closeContentImgPt:FlxPoint;

    var currentWin:Int = 0;  // 2 for How To, 3 for Content Warning

    // And of course...
    var mouseSprite:FlxSprite;

    // Disable input?
    var disableInput:Bool = false;

    // Our clicking sound
    var clickSound:FlxSound;

    // Special debug mode & enablers
    var debugBoxes:Array<DebugOverlay>;
    var debugScreenWidth:Int;
    var debugScreenHeight:Int;
    var debugCameraWidth:Int;
    var debugCameraHeight:Int;
    var debugCameraZoom:Float;
    var numClicks:Int = 0; // How many clicks in a row on nothing?

    var firstTurn = true;


    override public function create():Void
    {
        super.create();

        // Need to save these before we set them.
        debugScreenWidth = FlxG.stage.stageWidth;
        debugScreenHeight = FlxG.stage.stageHeight;
        debugCameraWidth = FlxG.camera.width;
        debugCameraHeight = FlxG.camera.height;
        debugCameraZoom = FlxG.camera.zoom;
        debugBoxes = new Array<DebugOverlay>();

	   // Wheeee!!!!
	   MultiPlatformUtils.ZoomToMax();

        this.bgColor = FlxColor.fromRGB(0x00, 0x00, 0x00);

        // Make a BG scene
        bgScenicSpr = new FlxSprite();
        bgScenicSpr.loadGraphic(AssetPaths.title_scenic__png);
        bgScenicSpr.origin.set(0,0);
        this.add(bgScenicSpr);

        // Make a BG sprite
        bgSpr = new FlxSprite();
        bgSpr.loadGraphic(AssetPaths.level_bg__png);
        bgSpr.origin.set(0,0);
        this.add(bgSpr);

        // Make our title
        titleSpr = new FlxSprite();
        titleSpr.loadGraphic(AssetPaths.title_text__png);
        titleSpr.origin.set(0,0);
        titleSpr.x = 60 + 20 + 20;
        titleSpr.y = 690 - 30 - 8 - 20;
        this.add(titleSpr);

        // Make our buttons: 1
        btnStart = new FlxSprite();
        btnStart.loadGraphic(AssetPaths.title_buttons_big__png, true, 298, 84);
        btnStart.animation.add("up", [0], 5, false);
        btnStart.animation.add("down", [1], 5, false);
        btnStart.animation.play("up");
        var mx = 224;
        var my = 100;
        btn1X = mx;
        btnStart.x = mx;
        btnStart.y = my;
        this.add(btnStart);
        //
        btn1XTxt = mx + 18 - 2;
        btnStartTxt = new FlxText();
        btnStartTxt.x = btn1XTxt;
        btnStartTxt.y = my + 21 - 32/2 - 16 + 7 + 2;
        btnStartTxt.setFormat("assets/kanit-regular.ttf", 52, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnStartTxt.text = "Start Game";
        this.add(btnStartTxt);

        var offY = 8;

        // Make our buttons: 2
        btnHowTo = new FlxSprite();
        btnHowTo.loadGraphic(AssetPaths.title_buttons_mid__png, true, 217, 64);
        btnHowTo.animation.add("up", [0], 5, false);
        btnHowTo.animation.add("down", [1], 5, false);
        btnHowTo.animation.play("up");
        mx = 100 + 200 + 5;
        my = 100 + 100 - 16 + offY;
        btn2X = mx;
        btnHowTo.x = mx;
        btnHowTo.y = my;
        this.add(btnHowTo);
        //
        btn2XTxt = mx + 18;
        btnHowToTxt = new FlxText();
        btnHowToTxt.x = btn2XTxt;
        btnHowToTxt.y = my + 21 - 32/2;
        btnHowToTxt.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnHowToTxt.text = "How to Play";
        this.add(btnHowToTxt);

        // Make our buttons: 3
        btnContent = new FlxSprite();
        btnContent.loadGraphic(AssetPaths.title_buttons_small__png, true, 217, 52);
        btnContent.animation.add("up", [0], 5, false);
        btnContent.animation.add("down", [1], 5, false);
        btnContent.animation.play("up");
        mx = 100 + 200 + 5;
        my = 100 + 2*100 - 16 - 36 + 2*offY;
        btn3X = mx;
        btnContent.x = mx;
        btnContent.y = my;
        this.add(btnContent);
        //
        btn3XTxt = mx + 18 - 4;
        btnContentTxt = new FlxText();
        btnContentTxt.x = btn3XTxt;
        btnContentTxt.y = my + 21 - 32/2;
        btnContentTxt.setFormat("assets/kanit-regular.ttf", 24, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnContentTxt.text = "Content Warning";
        this.add(btnContentTxt);

        // Make our buttons: 3
        btnCredits = new FlxSprite();
        btnCredits.loadGraphic(AssetPaths.title_buttons_small_half__png, true, 120, 52);
        btnCredits.animation.add("up", [0], 5, false);
        btnCredits.animation.add("down", [1], 5, false);
        btnCredits.animation.play("up");
        mx = 100 + 200 + (217-120) + 5;
        my = 100 + 2*100 + 52 - 16 - 36 + 3*offY;
        btn4X = mx;
        btnCredits.x = mx;
        btnCredits.y = my;
        this.add(btnCredits);
        //
        btn4XTxt = mx + 18 - 4;
        btnCreditsTxt = new FlxText();
        btnCreditsTxt.x = btn4XTxt;
        btnCreditsTxt.y = my + 21 - 32/2;
        btnCreditsTxt.setFormat("assets/kanit-regular.ttf", 24, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnCreditsTxt.text = "Credits";
        this.add(btnCreditsTxt);

        // Tutorial window
        tutorialWindow = new PopupWinTutorial();
        tutOffscreenY = Math.floor(-2 - tutorialWindow.height);
        tutOffscreenY2 = 960 + 64;
        tutorialWindow.x = 540/2 - tutorialWindow.width/2;
        tutorialWindow.y = tutOffscreenY;
        tutY = Math.floor(960/2 - tutorialWindow.height/2);
        this.add(tutorialWindow);
    
        // Content warning window
        contentWindow = new PopupWinContentWarning();
        contentOffscreenY = Math.floor(-2 - contentWindow.height);
        contentOffscreenY2 = 960 + 64;
        contentWindow.x = 540/2 - contentWindow.width/2;
        contentWindow.y = contentOffscreenY;
        contentY = Math.floor(960/2 - contentWindow.height/2);
        this.add(contentWindow);

        // Same for credits
        creditsWindow = new PopupWinCredits();
        creditsOffscreenY = Math.floor(-2 - tutorialWindow.height);
        creditsOffscreenY2 = 960 + 64;
        creditsWindow.x = 540/2 - creditsWindow.width/2;
        creditsWindow.y = creditsOffscreenY;
        creditsY = Math.floor(960/2 - creditsWindow.height/2);
        this.add(creditsWindow);


        // Make our close button
        btnClose = new FlxSprite();
        btnClose.loadGraphic(AssetPaths.title_buttons_close__png, true, 67, 64);
        btnClose.animation.add("up", [0], 5, false);
        btnClose.animation.add("down", [1], 5, false);
        btnClose.animation.play("up");
        mx = 489 - Math.floor(btnClose.width) + 1 - 18 + 2;
        my = 75 - Math.floor(btnClose.height) - 1 + 18 + Math.floor(btnClose.height) - 2;
        closeTutorialPt = new FlxPoint(mx,my);
        btnClose.x = -999;
        btnClose.y = -999;
        this.add(btnClose);
        //
        btnCloseImg = new FlxSprite();
        btnCloseImg.loadGraphic(AssetPaths.items__png, true, 32, 32);
        Ball.AddAnimations(btnCloseImg.animation);
        btnCloseImg.animation.play("cancel");
        closeTutorialImgPt = new FlxPoint(mx + btnCloseImg.width - btnCloseImg.width - 18 - 2 + 38, my + 15 + 2);
        btnCloseImg.x = -999;
        btnCloseImg.y = -999;
        this.add(btnCloseImg);

        // The other point.
        mx = 489 - Math.floor(btnClose.width) + 1 - 18 + 2;
        my = 75 - Math.floor(btnClose.height) - 1 + 18 + Math.floor(btnClose.height) - 2 + 189;
        closeContentPt = new FlxPoint(mx,my);
        closeContentImgPt = new FlxPoint(mx + btnCloseImg.width - btnCloseImg.width - 18 - 2 + 38, my + 15 + 2);


        // Our numbered buttons
        var amtY = 33 - 17;
        btnCredPage1 = new FlxSprite();
        btnCredPage1.loadGraphic(AssetPaths.title_buttons_num__png, true, 67, 51);
        btnCredPage1.animation.add("up", [0], 5, false);
        btnCredPage1.animation.add("down", [1], 5, false);
        btnCredPage1.animation.play("up");
        mx = 100 - 35;
        my = 50 - 38 + 12 + amtY;
        credPage1Pt = new FlxPoint(mx,my);
        btnCredPage1.x = -999;
        btnCredPage1.y = -999;
        this.add(btnCredPage1);
        //
        btnCredPage1Txt = new FlxText(0,0,  btnCredPage1.width);
        btnCredPage1Txt.setFormat("assets/kanit-regular.ttf", 24, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnCredPage1Txt.text = "1";
        btnCredPage1Txt.x = -999;
        btnCredPage1Txt.y = -999;
        this.add(btnCredPage1Txt);

        // Our numbered buttons
        btnCredPage2 = new FlxSprite();
        btnCredPage2.loadGraphic(AssetPaths.title_buttons_num__png, true, 67, 51);
        btnCredPage2.animation.add("up", [0], 5, false);
        btnCredPage2.animation.add("down", [1], 5, false);
        btnCredPage2.animation.play("up");
        mx = 100 - 35 + 1*72;
        my = 50 - 38 + 12 + amtY;
        credPage2Pt = new FlxPoint(mx,my);
        btnCredPage2.x = -999;
        btnCredPage2.y = -999;
        this.add(btnCredPage2);
        //
        btnCredPage2Txt = new FlxText(0,0,  btnCredPage2.width);
        btnCredPage2Txt.setFormat("assets/kanit-regular.ttf", 24, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnCredPage2Txt.text = "2";
        btnCredPage2Txt.x = -999;
        btnCredPage2Txt.y = -999;
        this.add(btnCredPage2Txt);

        // Our numbered buttons
        btnCredPage3 = new FlxSprite();
        btnCredPage3.loadGraphic(AssetPaths.title_buttons_num__png, true, 67, 51);
        btnCredPage3.animation.add("up", [0], 5, false);
        btnCredPage3.animation.add("down", [1], 5, false);
        btnCredPage3.animation.play("up");
        mx = 100 - 35 + 2*72;
        my = 50 - 38 + 12 + amtY;
        credPage3Pt = new FlxPoint(mx,my);
        btnCredPage3.x = -999;
        btnCredPage3.y = -999;
        this.add(btnCredPage3);
        //
        btnCredPage3Txt = new FlxText(0,0,  btnCredPage3.width);
        btnCredPage3Txt.setFormat("assets/kanit-regular.ttf", 24, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnCredPage3Txt.text = "3";
        btnCredPage3Txt.x = -999;
        btnCredPage3Txt.y = -999;
        this.add(btnCredPage3Txt);


        // Mouse always goes on top
        mouseSprite = new FlxSprite();
        MultiPlatformUtils.InitMouse(mouseSprite);
        this.add(mouseSprite);

        // Load our sound effects
        clickSound = FlxG.sound.load(AssetPaths.press_button__ogg);

        // Start/restart music.
        FlxG.sound.playMusic(AssetPaths.Title__ogg, 1, true);



// TEMP
//showDebugBoxes();
    }

    private function showDebugBoxes():Void
    {
        var numX = Math.ceil(540 / 200);
        var numY = Math.ceil(960 / 200);
        for (tileY in 0...numY) {
            var startAmt = tileY%2;
            for (tileX in 0...numX) {
                startAmt += 1;
                var dbg = new DebugOverlay(debugScreenWidth, debugScreenHeight, debugCameraWidth, debugCameraHeight, debugCameraZoom, startAmt%2==0);
                dbg.setPosition(tileX*200,tileY*200);
                this.add(dbg);
                debugBoxes.push(dbg);
            }
        }
    }

    private function hideDebugBoxes():Void
    {
        for (box in debugBoxes) {
            this.remove(box);
        }
        debugBoxes = new Array<DebugOverlay>();
    }

    override public function update(elapsed:Float):Void
    {
        if (firstTurn) {
            firstTurn = false;
            MultiPlatformUtils.CenterFullscreenMobile(this);
        }

        this.handleMouseInput();
        super.update(elapsed);
    }

    private function handleMouseInput():Void
    {
	var inputStuff = MultiPlatformUtils.ComputePseudoMouse();

        var mousePos:FlxPoint = inputStuff.mousePos;
        var wasPressed:Bool = inputStuff.justPressed;
        var wasReleased:Bool = inputStuff.justReleased;

	// Avoid nulls; just put the mouse offscreen in that case and be done with it.
	if (mousePos == null) {
		mouseSprite.setPosition(-999,-999);
		return;
	}

        // Keep the mouse in line.
        mouseSprite.setPosition(mousePos.x-19, mousePos.y-19);

        // Skip the rest if input is disabled
        // TODO: It seems that mouse input should really only be disabled on "release".
        if (disableInput) { return; }


        // Update our highlight of "what link can we click?"
        if (currentWin == 4) {
            if (mousePos.y >= 160 ) {
                if (handleCreditsClick(mousePos, false)) {
                    mouseSprite.animation.play("mouse_url"); // TODO: Some other icon
                } else {
                    mouseSprite.animation.play("mouse");
                }
            } else {
                mouseSprite.animation.play("mouse");
            }
        } else {
            mouseSprite.animation.play("mouse");
        }


        if (wasPressed) {
            // Optimizations!
            btnDown = 0;
            if (mousePos.y < 160 || currentWin!=4) {
                if (mousePos.x < 326 && currentWin==4) {
                    if (btnCredPage1.visible && PlayState.rectangleCursorOverlay(btnCredPage1, mousePos, 2) && creditsWindow.currPage != 0) {
                        btnDown = 11;
                        btnCredPage1.animation.play("down");
                        btnCredPage1Txt.scale.set(0.9, 0.9);
                        btnCredPage1Txt.alpha = 0.5;
                    } else if (btnCredPage2.visible && PlayState.rectangleCursorOverlay(btnCredPage2, mousePos, 2)  && creditsWindow.currPage != 1) {
                        btnDown = 12;
                        btnCredPage2.animation.play("down");
                        btnCredPage2Txt.scale.set(0.9, 0.9);
                        btnCredPage2Txt.alpha = 0.5;
                    } else if (btnCredPage3.visible && PlayState.rectangleCursorOverlay(btnCredPage3, mousePos, 2) && creditsWindow.currPage != 2) {
                        btnDown = 13;
                        btnCredPage3.animation.play("down");
                        btnCredPage3Txt.scale.set(0.9, 0.9);
                        btnCredPage3Txt.alpha = 0.5;
                    }
                } else {
                    // Check our buttons
                    if (btnStart.visible && PlayState.rectangleCursorOverlay(btnStart, mousePos, 2)) {
                        btnDown = 1;
                        btnStart.animation.play("down");
                        btnStartTxt.scale.set(0.9, 0.9);
                        btnStartTxt.alpha = 0.5;
                    } else if (btnHowTo.visible && PlayState.rectangleCursorOverlay(btnHowTo, mousePos, 2)) {
                        btnDown = 2;
                        btnHowTo.animation.play("down");
                        btnHowToTxt.scale.set(0.9, 0.9);
                        btnHowToTxt.alpha = 0.5;
                    } else if (btnContent.visible && PlayState.rectangleCursorOverlay(btnContent, mousePos, 2)) {
                        btnDown = 3;
                        btnContent.animation.play("down");
                        btnContentTxt.scale.set(0.9, 0.9);
                        btnContentTxt.alpha = 0.5;
                    } else if (btnCredits.alpha>=1.0 && PlayState.rectangleCursorOverlay(btnCredits, mousePos, 2)) {
                        btnDown = 4;
                        btnCredits.animation.play("down");
                        btnCreditsTxt.scale.set(0.9, 0.9);
                        btnCreditsTxt.alpha = 0.5;
                    } else if (btnClose.alpha>=1.0 && currentWin>0 && PlayState.rectangleCursorOverlay(btnClose, mousePos, 2)) {
                        btnDown = 99; // NOTE: Cancel
                        btnClose.animation.play("down");
                        btnCloseImg.scale.set(0.9, 0.9);
                        btnCloseImg.alpha = 0.5;
                    }
                }
            } else {
                if (currentWin == 4) {
                    if (handleCreditsClick(mousePos, true)) {
                        return;
                    }
                }
            }

            // Did we just click a button?
            if (btnDown != 0) {
                clickSound.play();
                numClicks = 0;
            } else {
                numClicks += 1;
                if (numClicks >= 30) {
                    hideDebugBoxes();
                    numClicks = 0;
                } else if (numClicks == 20) {
                    showDebugBoxes();
                    //DON'T reset count here
                }
            }

        }

        if (wasReleased) {
            // These don't need to be optimized, since we only ever check one
            if (btnDown == 1) {
                btnStart.animation.play("up");
                btnStartTxt.scale.set(1, 1);
                btnStartTxt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnStart, mousePos, 2)) {
                    showStartGame();
                }
            } else if (btnDown == 2) {
                btnHowTo.animation.play("up");
                btnHowToTxt.scale.set(1, 1);
                btnHowToTxt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnHowTo, mousePos, 2)) {
                    showHowTo();
                }
            } else if (btnDown == 3) {
                btnContent.animation.play("up");
                btnContentTxt.scale.set(1, 1);
                btnContentTxt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnContent, mousePos, 2)) {
                    showContentWarn();
                }
            } else if (btnDown == 4) {
                btnCredits.animation.play("up");
                btnCreditsTxt.scale.set(1, 1);
                btnCreditsTxt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnCredits, mousePos, 2)) {
                    showContentCredits();
                }
            } else if (btnDown == 11) {
                btnCredPage1.animation.play("up");
                btnCredPage1Txt.scale.set(1, 1);
                btnCredPage1Txt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnCredPage1, mousePos, 2)) {
                    creditsWindow.currPage = 0;
                    creditsWindow.redrawPage();
                }
            } else if (btnDown == 12) {
                btnCredPage2.animation.play("up");
                btnCredPage2Txt.scale.set(1, 1);
                btnCredPage2Txt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnCredPage2, mousePos, 2)) {
                    creditsWindow.currPage = 1;
                    creditsWindow.redrawPage();
                }
            } else if (btnDown == 13) {
                btnCredPage3.animation.play("up");
                btnCredPage3Txt.scale.set(1, 1);
                btnCredPage3Txt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnCredPage3, mousePos, 2)) {
                    creditsWindow.currPage = 2;
                    creditsWindow.redrawPage();
                }
            } else if (btnDown == 99) {
                btnClose.animation.play("up");
                btnCloseImg.scale.set(1, 1);
                btnCloseImg.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnClose, mousePos, 2)) {
                    cancelWindow();
                }
            }

            if (btnDown == 11 || btnDown==12 || btnDown==13) {
                refreshButtonPushDown();
            }

            btnDown = 0;
        }
    }

    private function refreshButtonPushDown()
    {
        if (creditsWindow.currPage == 0) {
            btnCredPage1.animation.play("down");
            btnCredPage1Txt.scale.set(0.9, 0.9);
            btnCredPage1Txt.alpha = 0.5;
            //
            btnCredPage2.animation.play("up");
            btnCredPage2Txt.scale.set(1.0, 1.0);
            btnCredPage2Txt.alpha = 1.0;
            //
            btnCredPage3.animation.play("up");
            btnCredPage3Txt.scale.set(1.0, 1.0);
            btnCredPage3Txt.alpha = 1.0;
        } else if (creditsWindow.currPage == 1) {
            btnCredPage2.animation.play("down");
            btnCredPage2Txt.scale.set(0.9, 0.9);
            btnCredPage2Txt.alpha = 0.5;
            //
            btnCredPage1.animation.play("up");
            btnCredPage1Txt.scale.set(1.0, 1.0);
            btnCredPage1Txt.alpha = 1.0;
            //
            btnCredPage3.animation.play("up");
            btnCredPage3Txt.scale.set(1.0, 1.0);
            btnCredPage3Txt.alpha = 1.0;
        } else if (creditsWindow.currPage == 2) {
            btnCredPage3.animation.play("down");
            btnCredPage3Txt.scale.set(0.9, 0.9);
            btnCredPage3Txt.alpha = 0.5;
            //
            btnCredPage2.animation.play("up");
            btnCredPage2Txt.scale.set(1.0, 1.0);
            btnCredPage2Txt.alpha = 1.0;
            //
            btnCredPage1.animation.play("up");
            btnCredPage1Txt.scale.set(1.0, 1.0);
            btnCredPage1Txt.alpha = 1.0;
        }
    }




    // Helper: Start the game
    private function showStartGame()
    {
        disableInput = true;

        // Move them at the same speed, but give the favorite one a boost
        var maxW = btnStart.width;
        for (win in [btnStart, btnStartTxt]) {
            FlxTween.tween(win, { x: 540+maxW }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        }
        for (win in [btnHowTo , btnHowToTxt, btnContent, btnContentTxt, btnCredits, btnCreditsTxt]) {
            FlxTween.tween(win, { x: 540+maxW }, 0.8, { type: FlxTweenType.ONESHOT, startDelay: 0.3 });
        }

        // Fade out the BG music
        FlxG.sound.music.fadeOut(1.0);

        // Fade out our backgrounds/titles
        FlxTween.tween(bgSpr, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay:0.6 });
        FlxTween.tween(titleSpr, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay:0.6 });
        FlxTween.tween(bgScenicSpr, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT, onComplete: showStartGameAnimDone, startDelay:0.6 });
    }

    private function showStartGameAnimDone(tween:FlxTween):Void
    {
        // Kind of dumb
        FlxTween.tween(bgScenicSpr, { alpha: 0.0 }, 0.3, { type: FlxTweenType.ONESHOT, onComplete: showStartGameAnimDoneReally});
    }


    // Open a URL
    // "Do Action" does not perform any processing if false
    private function handleCreditsClick(pos:FlxPoint, doAction:Bool):Bool
    {
        var urls = creditsWindow.getUrlBoxes();
        for (rect=>url in urls) {
            if (PlayState.rectangleCursorOverlayRaw(rect, pos, 2)) {
                if (doAction) {
                    FlxG.openURL(url);
                }
                return true;
            }
        }
        return false;
    }


    private function showStartGameAnimDoneReally(tween:FlxTween):Void
    {
        FlxG.switchState(new LevelSelect(CharacterMaker.Mia_Story1()));
    }


    // Helper: Show the "How To" window
    private function showHowTo()
    {
        disableInput = true;
        currentWin = 2;

        // Move them at the same speed, but give the favorite one a boost
        var maxW = btnStart.width;
        for (win in [btnHowTo , btnHowToTxt]) {
            FlxTween.tween(win, { x: 540+maxW }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        }
        for (win in [btnStart, btnContent, btnStartTxt, btnContentTxt, btnCredits, btnCreditsTxt]) {
            FlxTween.tween(win, { x: 540+maxW }, 0.8, { type: FlxTweenType.ONESHOT, startDelay: 0.3 });
        }

        // Fade our close button in.
        btnClose.x = closeTutorialPt.x;
        btnClose.y = closeTutorialPt.y;
        btnClose.alpha = 0.0;
        btnCloseImg.x = closeTutorialImgPt.x;
        btnCloseImg.y = closeTutorialImgPt.y;
        btnCloseImg.alpha = 0.0;
        FlxTween.tween(btnClose, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });
        FlxTween.tween(btnCloseImg, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });

        // Move our tutorial window into place.
        tutorialWindow.y = tutOffscreenY;
        FlxTween.tween(tutorialWindow, { y: tutY }, 1.0, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: showHowToAnimDone, startDelay:0.6 });
    }

    private function showHowToAnimDone(tween:FlxTween):Void
    {
        disableInput = false;
    }



    // Helper: Show the "Content Warning" window
    // TODO: For now we just show the tutorial again.
    private function showContentWarn()
    {
        disableInput = true;
        currentWin = 3;

        // Move them at the same speed, but give the favorite one a boost
        var maxW = btnStart.width;
        for (win in [btnContent , btnContentTxt]) {
            FlxTween.tween(win, { x: 540+maxW }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        }
        for (win in [btnHowTo , btnHowToTxt, btnStart, btnStartTxt, btnCredits, btnCreditsTxt]) {
            FlxTween.tween(win, { x: 540+maxW }, 0.8, { type: FlxTweenType.ONESHOT, startDelay: 0.3 });
        }

        // Fade our close button in.
        btnClose.x = closeContentPt.x;
        btnClose.y = closeContentPt.y;
        btnClose.alpha = 0.0;
        btnCloseImg.x = closeContentImgPt.x;
        btnCloseImg.y = closeContentImgPt.y;
        btnCloseImg.alpha = 0.0;
        FlxTween.tween(btnClose, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });
        FlxTween.tween(btnCloseImg, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });

        // Move our tutorial window into place.
        contentWindow.y = contentOffscreenY;
        FlxTween.tween(contentWindow, { y: contentY }, 1.0, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: showContentWarnAnimDone, startDelay:0.6 });
    }

    private function showContentWarnAnimDone(tween:FlxTween):Void
    {
        disableInput = false;
    }


    private function showContentCredits()
    {
        disableInput = true;
        currentWin = 4;

        // Move them at the same speed, but give the favorite one a boost
        var maxW = btnStart.width;
        for (win in [btnCredits, btnCreditsTxt]) {
            FlxTween.tween(win, { x: 540+maxW }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        }
        for (win in [btnStart, btnContent, btnStartTxt, btnContentTxt, btnHowTo , btnHowToTxt]) {
            FlxTween.tween(win, { x: 540+maxW }, 0.8, { type: FlxTweenType.ONESHOT, startDelay: 0.3 });
        }

        // Make sure it's pressed right.
        creditsWindow.currPage = 0;
        creditsWindow.redrawPage();
        refreshButtonPushDown();

        // Fade our close button in.
        btnClose.x = closeTutorialPt.x;
        btnClose.y = closeTutorialPt.y;
        btnClose.alpha = 0.0;
        btnCloseImg.x = closeTutorialImgPt.x;
        btnCloseImg.y = closeTutorialImgPt.y;
        btnCloseImg.alpha = 0.0;
        FlxTween.tween(btnClose, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });
        FlxTween.tween(btnCloseImg, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });

        // Fade our non-close buttons in
        btnCredPage1.x = credPage1Pt.x;
        btnCredPage1.y = credPage1Pt.y;
        btnCredPage1.alpha = 0;
        btnCredPage1Txt.x = credPage1Pt.x - 4 + 2;
        btnCredPage1Txt.y = credPage1Pt.y + 9 - 4;
        btnCredPage1Txt.alpha = 0;
        FlxTween.tween(btnCredPage1, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });
        FlxTween.tween(btnCredPage1Txt, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });
        //
        btnCredPage2.x = credPage2Pt.x;
        btnCredPage2.y = credPage2Pt.y;
        btnCredPage2.alpha = 0;
        btnCredPage2Txt.x = credPage2Pt.x - 4 + 2;
        btnCredPage2Txt.y = credPage2Pt.y + 9 - 4;
        btnCredPage2Txt.alpha = 0;
        FlxTween.tween(btnCredPage2, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });
        FlxTween.tween(btnCredPage2Txt, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });
        //
        btnCredPage3.x = credPage3Pt.x;
        btnCredPage3.y = credPage3Pt.y;
        btnCredPage3.alpha = 0;
        btnCredPage3Txt.x = credPage3Pt.x - 4 + 2;
        btnCredPage3Txt.y = credPage3Pt.y + 9 - 4;
        btnCredPage3Txt.alpha = 0;
        FlxTween.tween(btnCredPage3, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });
        FlxTween.tween(btnCredPage3Txt, { alpha: 1.0 }, 1.0, { type: FlxTweenType.ONESHOT, startDelay: 1.6 });

        // Move our tutorial window into place.
        creditsWindow.y = creditsOffscreenY;
        FlxTween.tween(creditsWindow, { y: creditsY }, 1.0, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, onComplete: showCreditsAnimDone, startDelay:0.6 });
    }

    private function showCreditsAnimDone(tween:FlxTween):Void
    {
        disableInput = false;
    }


    // Cancel whatever we were showing
    private function cancelWindow():Void
    {
        disableInput = true;

        // Put the window back
        if (currentWin == 2) {
            FlxTween.tween(tutorialWindow, { y: tutOffscreenY2 }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        } else if (currentWin == 3) {
            FlxTween.tween(contentWindow, { y: contentOffscreenY2 }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        } else if (currentWin == 4) {
            FlxTween.tween(creditsWindow, { y: creditsOffscreenY2 }, 0.5, { type: FlxTweenType.ONESHOT, ease: FlxEase.backIn });
        }
        currentWin = 0;

        // Move all buttons back
        FlxTween.tween(btnStart, { x: btn1X }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, startDelay:0.2, onComplete: cancelAnimDone });
        FlxTween.tween(btnStartTxt, { x: btn1XTxt }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, startDelay:0.2 });
        FlxTween.tween(btnHowTo, { x: btn2X }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, startDelay:0.2 });
        FlxTween.tween(btnHowToTxt, { x: btn2XTxt }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, startDelay:0.2 });
        FlxTween.tween(btnContent, { x: btn3X }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, startDelay:0.2 });
        FlxTween.tween(btnContentTxt, { x: btn3XTxt }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, startDelay:0.2 });
        FlxTween.tween(btnCredits, { x: btn4X }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, startDelay:0.2 });
        FlxTween.tween(btnCreditsTxt, { x: btn4XTxt }, 0.8, { type: FlxTweenType.ONESHOT, ease: FlxEase.bounceOut, startDelay:0.2 });

        // And fade
        FlxTween.tween(btnClose, { alpha: 0.0 }, 0.2, { type: FlxTweenType.ONESHOT});
        FlxTween.tween(btnCloseImg, { alpha: 0.0 }, 0.2, { type: FlxTweenType.ONESHOT});

        // Also these
        FlxTween.tween(btnCredPage1, { alpha: 0 }, 0.2, { type: FlxTweenType.ONESHOT, startDelay: 0.2 });
        FlxTween.tween(btnCredPage1Txt, { alpha: 0 }, 0.2, { type: FlxTweenType.ONESHOT, startDelay: 0.2 });
        FlxTween.tween(btnCredPage2, { alpha: 0 }, 0.2, { type: FlxTweenType.ONESHOT, startDelay: 0.2 });
        FlxTween.tween(btnCredPage2Txt, { alpha: 0 }, 0.2, { type: FlxTweenType.ONESHOT, startDelay: 0.2 });
        FlxTween.tween(btnCredPage3, { alpha: 0 }, 0.2, { type: FlxTweenType.ONESHOT, startDelay: 0.2 });
        FlxTween.tween(btnCredPage3Txt, { alpha: 0 }, 0.2, { type: FlxTweenType.ONESHOT, startDelay: 0.2 });
    }

    private function cancelAnimDone(tween:FlxTween):Void
    {
        // Grumble again
        FlxTween.tween(btnClose, { alpha: 0.0 }, 0.1, { type: FlxTweenType.ONESHOT, onComplete: cancelAnimDoneForReal});
    }

    private function cancelAnimDoneForReal(tween:FlxTween):Void
    {
        disableInput = false;
        btnClose.x = -999;
        btnClose.y = -999;
        btnCloseImg.x = -999;
        btnCloseImg.y = -999;
        btnCredPage1.x = -999;
        btnCredPage1.y = -999;
        btnCredPage1Txt.x = -999;
        btnCredPage1Txt.y = -999;
        btnCredPage2.x = -999;
        btnCredPage2.y = -999;
        btnCredPage2Txt.x = -999;
        btnCredPage2Txt.y = -999;
        btnCredPage3.x = -999;
        btnCredPage3.y = -999;
        btnCredPage3Txt.x = -999;
        btnCredPage3Txt.y = -999;
    }


}


