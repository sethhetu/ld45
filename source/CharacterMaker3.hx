package;

// Used for constructing a character for each story element
class CharacterMaker3 
{
	// Our first character
	public static function Mia_Story2():Character
	{
		var res = new Character();
		res.name = "Mia";
		res.age = 32;
		res.pnhe = "she";
		res.pnHe = "She";
		res.pnhis = "her";
		res.pnHis = "Her";
		res.pnhim = "her";
		res.pnHim = "Her";

		res.chapter = 3;
		res.level_music = AssetPaths.EasyLevel__ogg;

		res.intro_lines = [
			"Mia has grown up fast; she",
			"has a good job and lots of",
			"friends. Five years ago,",
			"however, she lost her",
			"sister to suicide.",
			"",
			"Mia took a long time to recover, and",
			"eventually moved to the city to get",
			"away from all the memories at home.",
			"She is doing better, but is also lonely.",
			"",
			"Mia is at a high risk of suicide, due",
			"to the upcoming holidays (for which",
			"she will travel home). Help Mia",
			"make it through two holidays by",
			"collecting 2 cupcakes.",
		];

		res.scenic_png = AssetPaths.mia2_scenic__png;

		res.short_desc = [
			"Mia is a 32 year old lady who just moved to the",
			"city and got a new job. She is troubled by the",
			"loss of her sister to suicide five years ago.",
			"This is especially difficult around the holidays.",
		];

		res.face_anim = "char1_older_neutral";

		res.goal = "Two Holidays";
		res.goal_anim = "cake";
		res.goal_description = [
			"Mia knows that she will be fine if she can",
			"make it through the next two holidays",
			"(Thanksgiving and Christmas). Doing so",
			"will be very challenging, since everything",
			"she sees in her home town reminds her of",
			"the sister she lost. To complete",
			"this chapter, collect two cupcakes.",
		];

		res.duration = "the holiday season";

		res.ball_advance_modulo = 3;

		res.reRollChance = 50;

		res.reRollTimes = 3;

		res.lifelines = [ 2,2,1,1 ];

		res.ballOdds = [
			"red" => 50,
			"green" => 20,
			"pink" => 50,
			"yellow" => 20,
		];

		res.ballNames = [
			"red" => "Work",
			"green" => "Going Out",
			"pink" => "Family Interactions",
			"yellow" => "Down Time",
		];

		res.ball_descriptions = [
			"red" => [
				"Mia's new job is much better than her",
				"previous one, but it still frustrates",
				"her sometimes. She mostly took the job",
				"just to get away from her home town.",
				"",
				"",
				"",
			],

			"green" => [
				"Mia is a workaholic so she doesn't have",
				"much time to spend with co-workers.",
				"When her schedule permits, though, she",
				"truly cherishes these moments.",
				"",
				"",
				"",
			],

			"pink" => [
				"Mia's family is sensitive to her loss, and she",
				"has made peace with them about the move to",
				"a new school during her childhood.",
				"Nowadays, she draws hope from her family",
				"interactions --although she'd be lying if",
				"she said her parents didn't frustrate her",
				"sometimes."
			],

			"yellow" => [
				"Mia still doesn't like down time, and has",
				"worked to minimize how often her schedule",
				"is free. Although somewhat effective, this",
				"means that every time she is alone she",
				"sinks quickly into introspective",
				"melancholy.",
				"",
			],
		];

		res.goal_descriptions = [
			"Mia's goal is to make it through two",
			"holidays, each of which is represented",
			"by a cupcake.",
			"",
			"",
			"",
			"",
		];

		res.modulate_goal = 0;
		res.goal_frequency = 130;
		res.bad_events = [
			35 => 1,
			80 => 1,
			92 => 1,
			100 => 1,
			120 => 1,
			150 => 1,
			200 => 1,
			220 => 1,
			250 => 1,
		];

		res.event_count = 0;

		res.event_descriptions = [
			"As the holiday season progresses, Mia",
			"may experience some events that",
			"drastically affect her emotional",
			"well-being. Events may be planned",
			"or spontaneous, and offer a potential",
			"risk or reward.",
			"",
		];

		res.event_titles = [
			"Thanksgiving",
			"Christmas",
		];

		res.event_stories = [
			[
				"Despite her best efforts, Mia",
				"gets dragged into an argument with",
				"one of her uncles.",
				"The argument escalates and draws",
				"in even more people, before Mia",
				"realizes that she is subconsciously",
				"using the fight as a proxy for her",
				"grief. This only makes her feel",
				"worse, however.",
				"",
				"Effect: Suicide Risk increases.",
			],

			[
				"The end of the year approaches, and",
				"with it the Christmas season.",
				"Mia finds herself spending hours",
				"lost in her memories of her sister",
				"and other scenes from her childhood.",
				"She begins to feel trapped by the",
				"past, and idly wonders if her life",
				"will ever pick up again.",
				"",
				"Effect: Suicide Risk increases.",
				"",
			],
		];

		res.therapist = "therapist";

		res.intro_obj_offset = 0;

		res.nextChara = 4;

		res.event_is_bad =[true, true];

		res.goal_count = 0;

		res.goal_requirement = 2;

		res.victory_lines = [
			"Mia managed to make it",
			"through the holidays,",
			"and returned to the",
			"city to decompress.",
			"",
			"Although it was difficult to see",
			"all the locations from her youth,",
			"she feels more at peace with her",
			"sister than ever before.",
			"",
			"Mia's story is over for now, but we may",
			"visit her again in the future.",
			"",
		];

		res.loss_lines = [
			"Mia had a series of",
			"nervous breakdowns,",
			"and nearly attempted",
			"suicide. She went to",
			"the hospital on her own.",
			"",
			"Mia is feeling hopeless and depressed.",
			"She has taken some important steps",
			"towards resolution, but she needs time.",
			"",
			"Mia's story is over for now, but we may",
			"visit her again in the future.",
		];

		res.ballEffects = [
			"red" => ["\"Frustration\" will increase a little",""],
			"green" => ["\"Hopelessness\" will decrease a little",""],
			"pink" => ["\"Hopelessness\" will decrease a little", "\"Frustration\" will increase a little"],
			"yellow" => ["\"Loneliness\" will increase a great deal",""],
		];

		res.emotions = [
			"Loneliness",
			"Hopelessness",
			"Frustration",
		];

		res.emotionalState = [ 100, 50, 30 ];
		res.riskLevel = 60;

		res.basic_response = [
			"red" => [
				new TurnEffect("emotion", 2, 1),
			],

			"green" => [
				new TurnEffect("emotion", 1, -1),
			],

			"pink" => [
				new TurnEffect("emotion", 1, -1),
				new TurnEffect("emotion", 2, 1),
			],

			"yellow" => [
				new TurnEffect("emotion", 0, 4),
			],
		];

		res.hopeful = false;


		res.properties = ["Kindred", "Depressed", "Low Motivation"];
		
		res.property_anims = ["purple_flower", "sad", "energy"];

		res.property_bonuses = [0, 0, -3];

		// WARNING: Make sure these are all the same length
		res.property_descriptions = [
			[
				"Mia is on good terms with her family,",
				"and she draws her strength from them.",
				"Even though they still sometimes irritate",
				"her, she finds the good outweighs the bad.",
				"",
				"Effect:",
				"       (Family Interactions) 2x Positive Effect",
			],

			[
				"Mia is no longer hopeful, and has sunk",
				"into a state of undiagnosed depression.",
				"The holidays are only making things worse.",
				"",
				"",
				"Effect:",
				"    Suicide Risk Factors +25% effect",
			],

			[
				"Mia has had a hard time motivating herself",
				"since the move. She has more friends now,",
				"but her busy schedule often leaves her",
				"drained of energy.",
				"Effect:",
				"       (Going Out) appears less often",
				"",
			],
		];

		res.property_balls = [
			"pink",
			"",
			"green",
		];

		res.who_suggests_help = "Her mother suggests:";
		res.match_recommends = "pink";
		res.avoid_recommends = "yellow";

		res.suicide_risk = "High";

		return res;
	}



}