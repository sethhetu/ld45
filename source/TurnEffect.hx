package;

// Simple class for holding the "effect" of a given turn.
// Points increase or decrease; very loosely typed
class TurnEffect
{
    public var target:String; // "emotion" or "risk"
    public var id:Int;  // which emotion slot, or 0 if unused
    public var amt:Int; // how much to affect this by

    public function new(target:String, id:Int, amt:Int)
    {
        this.target = target;
        this.id = id;
        this.amt = amt;
    }

}


