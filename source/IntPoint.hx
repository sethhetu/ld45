package;

// Simple class for holding an x,y value (and an equality operator)
class IntPoint
{
    public var x:Int;
    public var y:Int;

    public function new(x:Int, y:Int)
    {
        this.x = x;
        this.y = y;
    }

    public static function Min(a:Int, b:Int):Int
    {
        return a<b ? a : b;
    }

    public static function Max(a:Int, b:Int):Int
    {
        return a>b ? a : b;
    }

    public function equals(other:IntPoint):Bool
    {
        return this.x==other.x && this.y==other.y;
    }
}


