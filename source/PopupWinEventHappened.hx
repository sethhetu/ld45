package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows our tutorial
class PopupWinEventHappened extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 404;

	// BG is shown as a child of this window
	private var bgImg:FlxSprite;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	private var face:FlxSprite;

	// Current ball
	//private var obj1:FlxSprite;
	//private var obj2:FlxSprite;


	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines():Array<String>
	{
		return character.event_stories[character.event_count % character.event_stories.length];
	}


	public function new(character:Character)
	{
		super();

		this.character = character;

        bgImg = new FlxSprite();
        bgImg.loadGraphic(AssetPaths.content_window__png);
        bgImg.origin.set(0,0);
        this.add(bgImg);

		title = new FlxText(0,20,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Event";
		this.add(title);

/*
		var myX = fullWidth/2 - (4*96+3*2)/2 + 22;
		var myY = 200 - 60 + 4 - 72;
		face = new FlxSprite();
	    face.loadGraphic(AssetPaths.faces__png, true, 96, 96);
        face.animation.add("char1_neutral", [27], 5, false);
        face.animation.add("char1_older_neutral", [25], 5, false);
        face.animation.add("char2_neutral", [24], 5, false);
        face.animation.add("char3_neutral", [28], 5, false);
        face.animation.add("ambulance", [0], 5, false);
        face.animation.play(character.face_anim);
	    face.x = myX;
	    face.y = myY;
	    this.add(face);
*/

/*		obj1 = new FlxSprite();
		obj1.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj1.animation);
		obj1.x = fullWidth/2 - 200;
		obj1.y = 16;
		this.add(obj1);

		obj2 = new FlxSprite();
		obj2.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj2.animation);
		obj2.x = fullWidth/2 + 200;
		obj2.y = 16;
		this.add(obj2);

*/

		var myX = 42;
		var myY = 62;
		var id = 0;
		textLines = new Array<FlxText>();
		for (line in get_lines()) {
			var xOff = 0;
			var lineTxt = new FlxText(myX+xOff,myY,fullWidth-myX-xOff);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
			id += 1;
		}
	}

/*
	public function makeLoss():Void
	{
		title.text = "Lifeline Used";

		face.animation.play("ambulance");

		var id = 0;
		for (line in get_lines(true)) {
			textLines[id].text = line;
			id += 1;
		}
	}*/

	// NOTE: This only works if we call it after new()
	public function touch():Void
	{
		title.text = "Event: " + character.event_titles[character.event_count % character.event_titles.length];
		var id = 0;
		for (line in get_lines()) {
			textLines[id].text = line;
			id += 1;
		}
	}

}
