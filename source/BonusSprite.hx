package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Simple class that allows "bonus" chevrons to be applied.
class BonusSprite extends FlxSprite
{
	// Our bonus chevron value
	private var bonusMarker:FlxSprite; //Borrowed from the "chevrons" passed in to it.
	private var bonusValue:Int;

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);

		// Keep our bonus child in sync
		if (bonusMarker != null) {
			updateBonusLocation();
		}
	}

	private function updateBonusLocation():Void
	{
		bonusMarker.x = this.x - 1;
		bonusMarker.y = this.y + 22;
	}


	// Set an icon based on the chevrons
	public function setBonus(chevrons:FlxSpriteGroup, val:Int):Void
	{
		// Safety
		if (val<-4 || val>4) {
			trace("Invalid setBonus value: " + val);
			val = 0;
		}

		// Avoid redundant work
		if (val == bonusValue) {
			return;
		}

		bonusValue = val;
		if (bonusValue==0) {
			if (bonusMarker != null) {
				bonusMarker.kill();
				bonusMarker = null;
			}
		} else {
			if (bonusMarker == null) {
				createChevron(chevrons);
			}

			// Now clip it correctly.
			var key = ((bonusValue<0) ? "down" : "up") + "_" + Math.abs(bonusValue);
			bonusMarker.animation.play(key);

			// Tick early.
			updateBonusLocation();
		}
	}

	public function getBonus():Int
	{
		return bonusValue;
	}

	private function createChevron(chevrons:FlxSpriteGroup):Void
	{
		// Safety
		//if (bonusMarker != null) { bonusMarker.kill();}

		bonusMarker = chevrons.recycle(FlxSprite);
        bonusMarker.loadGraphic(AssetPaths.chevrons__png, true, 35, 13);
        bonusMarker.animation.add("down_4", [0], 5, false);
        bonusMarker.animation.add("up_4", [1], 5, false);
        bonusMarker.animation.add("down_3", [2], 5, false);
        bonusMarker.animation.add("up_3", [3], 5, false);
        bonusMarker.animation.add("down_2", [4], 5, false);
        bonusMarker.animation.add("up_2", [5], 5, false);
        bonusMarker.animation.add("down_1", [6], 5, false);
        bonusMarker.animation.add("up_1", [7], 5, false);
        chevrons.add(bonusMarker);
    }
	
}