package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text when the description window first appears
class DescWinHelp extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;
	
	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Object to show for comparison
	private var dispObj:FlxSprite;

	// Lines to show
	private function get_lines():Array<String>
	{
		return [
			'You can inspect any item by pressing it.',
			'This will help you understand ${character.name}\'s',
			"current mental state, and the effects any ",
			'Action may have on this.',
			'Take time to understand what is happening ',
			'in ${character.name}\'s life --there is no time limit.',
		];
	}

	private var lastLine = "Click \"Cancel\" when you are done.";


	public function new(character:Character)
	{
		super();

		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Inspection Mode";
		this.add(title);

		dispObj = new FlxSprite();
		dispObj.loadGraphic(AssetPaths.win_obj_help__png);
		dispObj.origin.set(0,0);
		dispObj.x = fullWidth/2 - dispObj.width/2;
		dispObj.y = 50 + 20*7 - 5;
		this.add(dispObj);

		var myX = 60;
		var myY = 50;
		for (line in get_lines()) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			this.add(lineTxt);
			myY += 20;
		}

		// Last line is centered
		var lineTxt = new FlxText(0,50+20*10,fullWidth);
		lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		lineTxt.text = lastLine;
		this.add(lineTxt);
	}

	
}