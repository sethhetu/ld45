package;

// Used for constructing a character for each story element
class CharacterMaker 
{
	public static function GameDone():Character
	{
		var res = new Character();

		res.name = "DONE";

		res.intro_lines = [
			"Thanks for playing our",
			"Ludum Dare 45 entry!",
			"",
			"",
			"            -- Seth & Nathan",
		];

		return res;
	}

	// Our first character
	public static function Mia_Story1():Character
	{
		var res = new Character();
		res.name = "Mia";
		res.age = 15;
		res.pnhe = "she";
		res.pnHe = "She";
		res.pnhis = "her";
		res.pnHis = "Her";
		res.pnhim = "her";
		res.pnHim = "Her";

		res.chapter = 1;
		res.level_music = AssetPaths.EasyLevel__ogg;

		res.intro_obj_offset = 16;

		res.intro_lines = [
			"Mia is a bright, young",
			"girl whose family just moved",
			"to a new town. Mia recently",
			"transfered to a private school",
			"to prepare for college.",
			"",
			"Mia enjoys her schoolwork, but feels",
			"lonely at the new school. She yearns",
			"for summer, when she can see her old",
			"friends again.",
			"",
			"Mia is at a low risk of suicide, but her",
			"environment and her actions will still",
			"affect her mental well-being. Help Mia",
			"make it through the school year by",
			"collecting 4 books, each representing",
			"a quarter of the year.",
		];

		res.scenic_png = AssetPaths.mia1_scenic__png;

		res.short_desc = [
			"Mia is a cheerful 15 year old who just transferred",
			"to a new school at her parents' urging.",
			"She really misses her friends, and is struggling",
			"to make it through the school year until summer.",
		];

		res.face_anim = "char1_neutral";

		res.goal = "Finish School";
		res.goal_anim = "book";
		res.goal_description = [
			"Mia is just trying to make it through the",
			"school year. Once it's summer, she can see",
			"her friends again. The stress of the move,",
			"the new school, and the social isolation will",
			"make this challenging.",
			"To complete this chapter, collect four Books,",
			"each of which represents 1/4 of the school year.",
		];

		res.duration = "the school year";

		res.ball_advance_modulo = 5;

		res.reRollChance = 10;

		res.reRollTimes = 1;

		res.lifelines = [ 2,2,1,1 ];

		res.ballOdds = [
			"red" => 50,
			"green" => 30,
			"pink" => 50,
			"yellow" => 50,
		];

		res.ballNames = [
			"red" => "Schoolwork",
			"green" => "Meeting Friends",
			"pink" => "Family Interactions",
			"yellow" => "Down Time",
		];

		res.ball_descriptions = [
			"red" => [
				"Mia has always been good at schoolwork,",
				"which is why her parents pushed her to",
				"apply to a private school in order to",
				"prepare her for college.",
				"Since moving to the new town, she is",
				"increasingly finding solace in the routine",
				"of getting schoolwork done.",
			],

			"green" => [
				"Mia is quite gregarious, and loves to hang out",
				"with friends. Although she doesn't get the",
				"opportunity very often these days, she still",
				"cherishes these rare moments.",
				"",
				"",
				"",
			],

			"pink" => [
				"Interactions with her parents and siblings have",
				"always been hit-or-miss. These days, Mia feels",
				"a small amount of resentment at having been",
				"forced to move to a new town --although she",
				"accepts that her parents are trying to do what's",
				"best for her.",
				"",
			],

			"yellow" => [
				"As an extrovert, Mia has never placed a strong",
				"emphasis on down time. Nowadays, though,",
				"every time she is alone, her thoughts wander,",
				"and she feels alone and lost.",
				"",
				"",
				"",
			],
		];

		res.goal_descriptions = [
			"Mia's goal is to make it through the",
			"school year, which is represented as",
			"four books. Once she collects all four,",
			"the school year ends.",
			"",
			"",
			"",
		];

		res.modulate_goal = 0;
		res.goal_frequency = 70;
		res.bad_events = [
			69 => 1,
			211 => 1,
		];

		res.event_count = 0;

		res.event_descriptions = [
			"As the school year progresses, Mia",
			"may experience some events that",
			"drastically affect her emotional",
			"well-being. Events may be planned",
			"or spontaneous, and offer a potential",
			"risk or reward.",
			"",
		];

		res.event_titles = [
			"School Bullying",
			"School Dance",
		];

		res.event_stories = [
			[
				"One day at lunch, Mia is singled",
				"out and ridiculed for some minute",
				"detail of her appearance.",
				"Although she never felt self-conscious",
				"about this before, the shock of the",
				"unprovoked attack, combined with the",
				"ongoing stress of the worst school",
				"year of her life leads Mia to break",
				"down in tears.",
				"",
				"Effect: Suicide Risk increases.",
			],

			[
				"The end-of-the-year prom approaches.",
				"Mia was not planning to go, but is",
				"convinced at the last minute by a",
				"small group of girls who just might",
				"be the beginning of a new friend circle.",
				"They have a fantastic time laughing the",
				"night away and gossiping about the",
				"dates everyone else brought.",
				"",
				"Effect: Suicide Risk decreases.",
				"",
			],
		];

		res.nextChara = 2;

		res.event_is_bad =[true, false];

		res.goal_count = 0;

		res.goal_requirement = 4;

		res.victory_lines = [
			"It was tough at times, but",
			"Mia finally made it through",
			"the school year!",
			"She has the whole summer",
			"ahead of her to catch up",
			"with her friends.",
			"",
			"Mia's story is over for now, but we may",
			"visit her again in the future.",
			"",
			"",
			"",
			"",
			"",
		];

		res.therapist = "school guidance counselor";

		res.loss_lines = [
			"Unfortunately, Mia fell into",
			"despair and attempted",
			"suicide before summer.",
			"Her parents acted fast and",
			"got her to a hospital.",
			"",
			"Mia was lucky to be found in time.",
			"Her college plans are now in disarray,",
			"but she is a survivor, and will live on.",
			"",
			"Mia's story is over for now, but we may",
			"visit her again in the future.",
		];

		res.ballEffects = [
			"red" => ["\"Frustration\" will decrease a little",""],
			"green" => ["\"Feeling Empty\" will decrease a lot",""],
			"pink" => ["\"Feeling Empty\" will decrease a little", "\"Frustration\" will increase a little"],
			"yellow" => ["\"Loneliness\" will increase a lot",""],
		];

		res.emotions = [
			"Loneliness",
			"Feeling Empty",
			"Frustration",
		];

		res.emotionalState = [ 80, 90, 90 ];
		res.riskLevel = 20;

		res.basic_response = [
			"red" => [
				new TurnEffect("emotion", 2, -1),
			],

			"green" => [
				new TurnEffect("emotion", 1, -2),
			],

			"pink" => [
				new TurnEffect("emotion", 1, -1),
				new TurnEffect("emotion", 2, 1),
			],

			"yellow" => [
				new TurnEffect("emotion", 0, 2),
			],
		];

		res.hopeful = true;


		res.properties = ["Outgoing", "Hopeful", "Low Motivation"];
		
		res.property_anims = ["flower", "rainbow_star", "energy"];

		res.property_bonuses = [0, 0, -2];

		// WARNING: Make sure these are all the same length
		res.property_descriptions = [
			[
				"Mia has an extroverted personality. She",
				"loves hanging out with her friends",
				"more than anything. As a result of the move,",
				"she now finds her free time to be",
				"increasingly isolating.",
				"Effect:",
				"       (Down Time) increases Loneliness",
			],

			[
				"Mia is hopeful and optimistic. She is",
				"always able to find the silver lining to",
				"every storm cloud, and she bounces back",
				"quickly from sad times.",
				"Effect:",
				"    Emotions stabilize over time",
				"",
			],

			[
				"Mia has had a hard time motivating herself",
				"since the move. She has mostly stopped",
				"looking for new social activities or friends",
				"to hang out with. Instead, she usually stays",
				"home and passes the time doing nothing much.",
				"Effect:",
				"       (Meeting Friends) appears less often",
			],
		];

		res.property_balls = [
			"yellow",
			"",
			"green",
		];

		res.who_suggests_help = "Her friends suggest:";
		res.match_recommends = "green";
		res.avoid_recommends = "yellow";

		res.suicide_risk = "Low";

		return res;
	}



}