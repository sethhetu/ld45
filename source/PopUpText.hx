package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.util.FlxColor;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Very simple pop-up "+X" message
class PopUpText extends FlxText
{
	public function new(txt:String, clr:FlxColor)
	{
		super();
		this.setFormat("assets/kanit-regular.ttf", 22, clr, FlxTextAlign.LEFT, FlxTextBorderStyle.OUTLINE, 0xFF000000, true);
		this.bold = true;
		this.text = txt;
	}

}
