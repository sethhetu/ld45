package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text for the character
class DescWinEmotionBar extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Current goal
	private var ex1:FlxSprite;
	private var ex2:FlxSprite;

	// Which emotion are we shoing?
	private var emotionBarId:Int = 0;

	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines():Array<String>
	{
		return [
			'This meter shows one of ${character.name}\'s emotions.',
			'Actions can raise or lower ${character.pnhis} emotional state,',
			'affecting ${character.pnhis} level of "${character.emotions[emotionBarId]}".',
			'Some people may slowly recover from negative',
			'emotions on their own as time passes.',
			'In general, keep an eye on your emotions,',
			'since they can affect the Suicide Risk meter.',
			'Examples Levels:',
			'         Dangerous                  Safe',
		];
	}


	public function new(character:Character)
	{
		super();
		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		this.add(title);

		ex1 = new FlxSprite();
		ex1.loadGraphic(AssetPaths.emotion_examples__png, true, 134, 46);
		ex1.animation.add("neg", [0], 5, false);
		ex1.animation.play('neg');
		ex1.x = fullWidth - ex1.width - 228 + 100 - 105 - 111 + 80 - 6;
		ex1.y = 50 + 20*2 - 15 + 142 + 8;
		this.add(ex1);

		ex2 = new FlxSprite();
		ex2.loadGraphic(AssetPaths.emotion_examples__png, true, 134, 46);
		ex2.animation.add("pos", [1], 5, false);
		ex2.animation.play('pos');
		ex2.x = fullWidth - ex2.width - 228 + 100 - 105 + 140 + 15;
		ex2.y = 50 + 20*2 - 15 + 142 + 8;
		this.add(ex2);

		var myX = 60;
		var myY = 50;
		textLines = new Array<FlxText>();
		for (line in get_lines()) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}

		setBarId(0); // for now
	}

	public function setBarId(id:Int):Void
	{
		emotionBarId = id;

		// Update title text
		title.text = "Emotion: " + character.emotions[emotionBarId];

		// Update the example text
		var i = 0;
		for (line in get_lines()) {
			textLines[i].text = line;
			i += 1;
		}

		// TODO: maybe more examples?
	}


	
}