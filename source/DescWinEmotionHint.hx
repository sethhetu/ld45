package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows the default "help" text for the character
class DescWinEmotionHint extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 504;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Current ball
	private var dispObj:FlxSprite;

	// Which emotion are we shoing?
	private var emotionBarId:Int = 0;

	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines(ballColor:String, bonus:Int):Array<String>
	{
		var strongWeak = Math.abs(bonus) > 2 ? 'massive' : (Math.abs(bonus) > 1 ? 'strong' : 'weak');
		var bonusPenalty = bonus<0 ? 'negative' : 'positive';
		var res = [
			'Every time ${character.name} matches 3+ of these:',
			'${character.pnhe} will experience a ${bonusPenalty} emotion',
			'to ${character.pnhis} associated mental state.',
			'This only applies to balls that match; if a',
			'ball falls off the stage as part of a combo,',
			'it does not affect ${character.name}\'s emotions.',
		];
		return res;
	}


	public function new(character:Character)
	{
		super();
		this.character = character;

		title = new FlxText(0,0,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Emotion Hint";
		this.add(title);

		dispObj = new FlxSprite();
		dispObj.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(dispObj.animation);
		dispObj.x = fullWidth - dispObj.width - 220 + 70 + 42 + 80 - 2;
		dispObj.y = 50 + 20*2 - 15 - 23;
		this.add(dispObj);

		var myX = 60;
		var myY = 50;
		textLines = new Array<FlxText>();
		for (line in get_lines('red', -2)) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}
	}

	public function setBallAndBonus(ballColor:String, bonus:Int):Void
	{
		// Update the example text
		var i = 0;
		for (line in get_lines(ballColor, bonus)) {
			textLines[i].text = line;
			i += 1;
		}

		// Update the sample ball
		dispObj.animation.play(ballColor + '_static');
	}

}