package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows our tutorial
class PopupWinContentWarning extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 404;

	// BG is shown as a child of this window
	private var bgImg:FlxSprite;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Current ball
	//private var obj1:FlxSprite;
	//private var obj2:FlxSprite;


	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines(propId:Int):Array<String>
	{
		return [
			"This is a game about Suicide.",
			"It does not contain any visualizations",
			"or descriptions of suicide attempts.",
			"However, it explores the lives of",
			"characters who are at risk of suicide.",
			"",
			"If this game is not for you, we",
			"understand! It is a tough subject, ",
			"and this game is just one attempt to",
			"help people undrestand it.",
			"",
			"For more official information on suicide",
			"and suicide prevention, please see:",
			"   https://suicidepreventionlifeline.org/",
			"or call:",
			"   1-800-273-8255",
			"(Note: We are not affiliated with them)",
		];
	}


	public function new()
	{
		super();

        bgImg = new FlxSprite();
        bgImg.loadGraphic(AssetPaths.content_window__png);
        bgImg.origin.set(0,0);
        this.add(bgImg);

		title = new FlxText(0,20,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Content Warning";
		this.add(title);

		var myX = 0;
		var myY = 0;

/*		obj1 = new FlxSprite();
		obj1.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj1.animation);
		obj1.x = fullWidth/2 - 200;
		obj1.y = 16;
		this.add(obj1);

		obj2 = new FlxSprite();
		obj2.loadGraphic(AssetPaths.items__png, true, 32, 32);
		Ball.AddAnimations(obj2.animation);
		obj2.x = fullWidth/2 + 200;
		obj2.y = 16;
		this.add(obj2);

*/

		myX = 42;
		myY = 62;
		textLines = new Array<FlxText>();
		for (line in get_lines(0)) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}
	}

	// NOTE: This only works if we call it after new()
	/*
	public function touch():Void
	{
		// Note: This is probably the worst way to do this...
		var throwAway = new FlxText(0,100);
		throwAway.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
		throwAway.text = title.text;
		var offsetX = throwAway.width/2 + 16;

		// Update Icons
		for (obj in [obj1,obj2]) {
			obj.animation.play(character.goal_anim);

			obj.x = Math.floor(fullWidth/2 - offsetX);
			offsetX *= -1;
			offsetX -= 9; // Not sure why...
		}
	}
	*/

}
