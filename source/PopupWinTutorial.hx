package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows our tutorial
class PopupWinTutorial extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 404;

	// BG is shown as a child of this window
	private var bgImg:FlxSprite;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Some character faces
	private var faces:Array<FlxSprite>;

	// Helper objects
	private var obj1:FlxSprite;
	private var obj2:FlxSprite;


	// Lines to show
	private var textLines:Array<FlxText>;
	private function get_lines(propId:Int):Array<String>
	{
		return [
			'This game follows several people',
			'as they struggle through their daily',
			'lives and work towards their goals.',
			'',
			'',
			'',
			'',
			'',
			'',
			'Each person\'s life is viewed as a series',
			'of activities, represented by different',
			'balls. When your turn starts, you can',
			'click "Action" then pull back on the next',
			'ball to launch it forward. If it forms a',
			'group of three or more, it will match,',
			'and the balls will be eliminated.',
			'',
			'',
			'',
			'',
			'The other button, "Inspect", can be used',
			'to get information on anything currently',
			'on screen. This is crucial, since each',
			'person you play as will respond to',
			'Actions differently. This is due to their',
			'different life circumstances, personalities,',
			'and mental state and potential illness.',
			'',
			'',
			'',
			'',
			'Try to complete each person\'s Goal while',
			'managing their suicide risk and keeping',
			'them safe.',
		];
	}


	public function new()
	{
		super();

        bgImg = new FlxSprite();
        bgImg.loadGraphic(AssetPaths.tutorial_window__png);
        bgImg.origin.set(0,0);
        this.add(bgImg);

		title = new FlxText(0,20,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "How to Play";
		this.add(title);

		var margin = 2;
		var myX = fullWidth/2 - (4*96+3*margin)/2 + 22;
		var myY = 200 - 60 + 4;
		var face_anims = [27, 24, 25, 28];
		faces = new Array<FlxSprite>();
		for (face in face_anims) {
	        var spr = new FlxSprite();
	        spr.loadGraphic(AssetPaths.faces__png, true, 96, 96);
	        spr.animation.add("person", [face], 5, false);
	        spr.animation.play("person");
	        spr.x = myX;
	        spr.y = myY;
	        faces.push(spr);
	        this.add(spr);
	        myX += spr.width + margin;
		}

		obj1 = new FlxSprite();
		obj1.loadGraphic(AssetPaths.tutorial_fg_1__png);
		obj1.origin.set(0,0);
		obj1.x = 50 + 35;
		obj1.y = 382 + 11;
		this.add(obj1);

		obj2 = new FlxSprite();
		obj2.loadGraphic(AssetPaths.win_obj_help__png);
		Ball.AddAnimations(obj2.animation); // ??? this seems wrong.
		obj2.x = 34 + 24;
		obj2.y = 616;
		this.add(obj2);


		myX = 42;
		myY = 62;
		textLines = new Array<FlxText>();
		for (line in get_lines(0)) {
			var lineTxt = new FlxText(myX,myY,fullWidth-myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
		}
	}

	// NOTE: This only works if we call it after new()
	/*
	public function touch():Void
	{
		// Note: This is probably the worst way to do this...
		var throwAway = new FlxText(0,100);
		throwAway.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
		throwAway.text = title.text;
		var offsetX = throwAway.width/2 + 16;

		// Update Icons
		for (obj in [obj1,obj2]) {
			obj.animation.play(character.goal_anim);

			obj.x = Math.floor(fullWidth/2 - offsetX);
			offsetX *= -1;
			offsetX -= 9; // Not sure why...
		}
	}
	*/

}
