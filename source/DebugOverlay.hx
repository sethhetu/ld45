package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Helpful for tracking down some issues
class DebugOverlay extends FlxSpriteGroup
{
	private static var fullWidth:Int = 200;
	private static var fullHeight:Int = 200;

	private var bgImg:FlxSprite;
	private var title:FlxText;

	private var textLines:Array<FlxText>;


	public function new(screenWidth:Int, screenHeight:Int, cameraWidth:Int, cameraHeight:Int, cameraZoom:Float, invertColors:Bool)
	{
		super();

		var color1 = invertColors ? 0xFF000000:0xFFFFFFFF;
		var color2 = invertColors ? 0xFFFFFFFF:0xFF000000;

        bgImg = new FlxSprite();
        if (invertColors) {
        	bgImg.loadGraphic(AssetPaths.dbg_black__png);
        } else {
			bgImg.loadGraphic(AssetPaths.dbg_white__png);
        }
        bgImg.origin.set(0,0);
        this.add(bgImg);

		title = new FlxText(0,10,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, color2, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, color2, true);
		title.text = "Debug Mode";
		this.add(title);

		var myLines:Array<String> = [
			'Stage Size',
			'${screenWidth} x ${screenHeight}',
			'Camera Size/Zoom',
			'${cameraWidth}x${cameraHeight}, ${cameraZoom}x',
			'To Exit:',
			'Touch Screen 10x',
		];

		var myX = 10;
		var myY = 50;
		var id = 0;
		textLines = new Array<FlxText>();
		for (line in myLines) {
			var lineTxt = new FlxText(myX,myY,fullWidth-2*myX);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, color2, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, color2, true);
			lineTxt.text = line;
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 20;
			id += 1;
		}
	}

}
