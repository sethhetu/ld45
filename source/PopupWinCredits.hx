package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.math.FlxRect;
import flixel.util.FlxSpriteUtil;
import flixel.group.FlxSpriteGroup;
import flixel.animation.FlxAnimationController;


// Shows our credits!
class PopupWinCredits extends FlxSpriteGroup
{
	// TODO: Global
	private static var fullWidth:Int = 404;

	// BG is shown as a child of this window
	private var bgImg:FlxSprite;

	// Title of this window
	private var title:FlxText;

	private var character:Character;

	// Some character faces
	private var faces:Array<FlxSprite>;

	// Helper objects
	private var obj1:FlxSprite;
	private var obj2:FlxSprite;

	// Which page of the credits are we on?
	public var currPage:Int = 0;
	public var maxPages:Int = 3;


	// Constructed on first use
	// TODO: Can we cache these further, with a y-position "hint" ?
	private var collisions:Array<Map<FlxRect,String>> = [
		null,
		null,
		null,
	];


	// Our titles
	private var titles:Array<String> = [
		"Credits",
		"Attribution",
		"Attribution",
	];

	// Our pages
	private var pages:Array<Array<String>> = [
		[
			"Created By", 
			"    Seth & Nathan", 
			"...for the Ludum Dare 45 game jam.",

			"",
			"Attribution",
			"Graphics",

			"",
			"Crosshairs Pack by LoneCoder",
			"License: CC0 - Public Domain",
			"https://opengameart.org/content/64-crosshairs-pack-split",

			"",
			"Game Icons by Kenney",
			"License: CC0 - Public Domain",
			"https://opengameart.org/content/game-icons-expansion",

			"",
			"48x48 Face Template by CharlesGabriel",
			"License: CC-BY 3.0",
			"https://opengameart.org/content/48x48-face-template",

			"",
			"Pixel Adventure 1 by Pixel Frog",
			"License: CC-BY 4.0",
			"https://opengameart.org/content/pixel-adventure-1",

			"",
			"Lemcraft by richtaur",
			"License: CC0 - Public Domain",
			"https://opengameart.org/content/lemcraft",

			"",
			"MostRichGrinser by needcoffee",
			"License: CC0 - Public Domain",
			"https://opengameart.org/content/needcoffee%E2%80%99s",
			"   -mostrichgrinser",

			"",
			"Various Icons by qubodup",
			"License: CC0 - Public Domain",
			"https://opengameart.org/content/various-icons",


		],

		[
			"Graphics",

			"",
			"Arcade Item Pack",
			"License (Pro):",
			"   https://static.gamedevmarket.net/terms-conditions",
			"Purchased as part of the:",
			"   Humble 8-bit Pixel Game Dev Bundle",

			"",
			"Fonts",

			"",
			"Kanit",
			"License: Open Font License",
			"https://fonts.google.com/specimen/Kanit",
			"   ?selection.family=Kanit",

			"",
			"Photos",

			"",
			"Title Screen",
			"License: CC0 - Public Domain",
			"https://pxhere.com/en/photo/784138",	

			"",
			"Level 1: St. Mary's School",
			"License: CC-BY 2.0",
			"https://upload.wikimedia.org/wikipedia/commons/a/",
			"   a8/Campus_scene%2C_St._Mary%27s_School%2C_Raleigh",
			"   %2C_N._C._%285812046232%29.jpg",		

			"",
			"Level 2: Kleines Phi Cocktail Bar",
			"License: CC-BY 4.0 (International)",
			"https://upload.wikimedia.org/wikipedia/commons/d/",
			"   dc/Cocktail-Bar_%28Kleines_Phi%29_in_Hamburg.jpg",

			"",
			"Level 3: Spray Lakes, Alberta CA",
			"License: CC-BY 2.0",
			"https://upload.wikimedia.org/wikipedia/commons/c/",
			"   c3/Mount_Turner_winter.jpg",

			"",
			"Audio",

			"The SFXR sound generator",
			"http://www.drpetter.se/project_sfxr.html",
		],


		[
			"Audio",

			"",
			"512 Sound Effects by SubspaceAudio",
			"License: CC0 - Public Domain",
			"https://opengameart.org/content/512-sound-effects-8-bit-style",

			"",
			"Title: Monplaisir Loyalty Freak Music",
			"License: CC0 - Public Domain",
			"https://opengameart.org/content/remember-the-time",
			"   -we-use-to-play",

			"",
			"EasyLvl: White by Kevin MacLeod",
			"License: CC BY 4.0",
			"https://incompetech.com",

			"",
			"HardLvl: Skye Cuillin by Kevin MacLeod",
			"License: CC BY 4.0",
			"https://incompetech.com",

			"",
			"Victory: Music by Matthew Pablo",
			"License: CC BY 3.0",
			"https://opengameart.org/content/pleasant-creek",
			"http://www.matthewpablo.com",

			"",
			"StoryOver: Music by İlker Yalçıner",
			"License: CC BY 4.0",
			"https://opengameart.org/content/one-last-try",

			"",
			"Thank You!",
		],

	];

	// Font size for each line; 0 means "default"
	private var fontSizes:Array<Array<Int>> = [
		[24,20,0, 10,32,30, 10,20,0,12, 10,20,0,12, 10,20,0,12, 10,20,0,12, 10,20,0,12, 10,20,0,12,12, 10,20,0,12,],
		[30, 10,20,0,12,0,12, 10,30, 10,20,0,12,12, 10,30, 10,20,0,12, 10,20,0,12,12,12, 10,20,0,12,12, 10,20,0,12,12, 10,30, 20,12, ],
		[30,  10,20,0,12,  10,20,0,12,12,  10,20,0,12,  10,20,0,12,  10,20,0,12,12, 10,20,0,12, 10,32],
	];


	// URLs for clicking
	private static var N:String = "";
	private static var U0:String = "https://ldjam.com/events/ludum-dare/45/starting-from-nothing";
	private static var U1:String = "https://opengameart.org/content/64-crosshairs-pack-split";
	private static var U2:String = "https://opengameart.org/content/game-icons-expansion";
	private static var U3:String = "https://opengameart.org/content/48x48-face-template";
	private static var U4:String = "https://opengameart.org/content/pixel-adventure-1";
	private static var U5:String = "https://opengameart.org/content/lemcraft";
	private static var U6:String = "https://opengameart.org/content/needcoffee%E2%80%99s-mostrichgrinser";
	private static var U7:String = "https://opengameart.org/content/various-icons";
	private static var U8:String = "https://static.gamedevmarket.net/terms-conditions";
	private static var U9:String = "https://fonts.google.com/specimen/Kanit?selection.family=Kanit";
	private static var UA:String = "https://pxhere.com/en/photo/784138";
	private static var UB:String = "https://upload.wikimedia.org/wikipedia/commons/a/a8/Campus_scene%2C_St._Mary%27s_School%2C_Raleigh%2C_N._C._%285812046232%29.jpg";
	private static var UC:String = "https://upload.wikimedia.org/wikipedia/commons/d/dc/Cocktail-Bar_%28Kleines_Phi%29_in_Hamburg.jpg";
	private static var UD:String = "https://upload.wikimedia.org/wikipedia/commons/c/c3/Mount_Turner_winter.jpg";
	private static var UE:String = "http://www.drpetter.se/project_sfxr.html";
	private static var UF:String = "https://opengameart.org/content/512-sound-effects-8-bit-style";
	private static var UG:String = "https://opengameart.org/content/remember-the-time-we-use-to-play";
	private static var UH:String = "https://incompetech.com";
	private static var UJ:String = "https://incompetech.com";
	private static var UKa:String = "https://opengameart.org/content/pleasant-creek";
	private static var UKb:String = "http://www.matthewpablo.com";
	private static var UL:String = "https://opengameart.org/content/one-last-try";

	private var urls:Array<Array<String>> = [
		[N,U0,N,  N,N,N,  N,N,N,U1, N,N,N,U2,  N,N,N,U3,  N,N,N,U4,  N,N,N,U5,  N,N,N,U6,U6,  N,N,N,U7, ],
		[N,  N,N,N,U8,N,N,  N,N,  N,N,N,U9,U9,  N,N,  N,N,N,UA,  N,N,N,UB,UB,UB,  N,N,N,UC,UC,  N,N,N,UD,UD,  N,N, N,UE],
		[N,  N,N,N,UF,  N,N,N,UG,UG,  N,N,N,UH,  N,N,N,UJ,  N,N,N,UKa,UKb,  N,N,N,UL,  N,N,],
	];


	// Max lines on any page
	public function maxPageLines():Int
	{
		var maxPg = 0;
		for (lines in pages) {
			maxPg = IntPoint.Max(maxPg, lines.length);
		}
		return maxPg;
	}


	// Lines to show
	private var textLines:Array<FlxText>;


	public function new()
	{
		super();

        bgImg = new FlxSprite();
        bgImg.loadGraphic(AssetPaths.tutorial_window__png);
        bgImg.origin.set(0,0);
        this.add(bgImg);

		title = new FlxText(0,20,fullWidth);
		title.setFormat("assets/kanit-regular.ttf", 32, 0xFF000000, FlxTextAlign.CENTER, FlxTextBorderStyle.NONE, 0xFF000000, true);
		title.text = "Placeholder";
		this.add(title);


		// First, make the max line count
		var myX = 0;
		var myY = 0;
		textLines = new Array<FlxText>();
		for (line in 0...maxPageLines()) {
			var lineTxt = new FlxText(myX,myY,fullWidth);
			lineTxt.setFormat("assets/kanit-regular.ttf", 18, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
			lineTxt.text = "Temp";
			textLines.push(lineTxt);
			this.add(lineTxt);
			myY += 1;
		}

		// And test update
		redrawPage();
	}

	// Get our set of links as HitBox => URL
	public function getUrlBoxes():Map<FlxRect, String>
	{
		if (collisions[currPage] == null) {
			var res = new Map<FlxRect, String>();

			var lines = pages[currPage];
			var urlLines = urls[currPage];
			for (id in 0...urlLines.length) {
				var line = lines[id];
				var fontSizes = fontSizes[currPage];
				var txt = textLines[id];
				var url = urlLines[id];

				if (url.length>0) {
					// We need to do the dumb trick again
					var throwAway = new FlxText();
					throwAway.setFormat("assets/kanit-regular.ttf", fontSizes[id], 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
					throwAway.text = line;

					//...and a slightly less dumb one
					var numSpaces = 0;
					for (myId in 0...line.length) {
						if (line.charAt(myId) == ' ') {
							numSpaces += 1;
						} else {
							break;
						}
					}

					var myX = numSpaces * 8; // NOTE: Assumes code point width.
					var myWidth = throwAway.width;
					var rect = new FlxRect(txt.x+myX,txt.y+4,myWidth-myX,txt.height-4);
					res[rect] = url;
				}
			}

			collisions[currPage] = res;
		}

		return collisions[currPage];
	}

	public function redrawPage():Void
	{
		title.text = titles[currPage];

		var lines = pages[currPage];
		var fontSizes = fontSizes[currPage];
		var urlLines = urls[currPage];

		// Flixel is really weird about layouts happening "before" and after certain things are created...
		var defaultSize = 18;
		var myX = 38;
		var myY = 62;
		for (id in 0...maxPageLines()) {
			if (id < lines.length) {
				this.remove(textLines[id]);

				var sz = fontSizes[id];
				if (sz == 0) {
					sz = defaultSize;
				}

				// Messing with urls
				var myUrl = urlLines[id];

				var myColor = myUrl.length>0 ? 0xFF0000CC : 0xFF000000;

				// Hack
				var myAlign = sz>=30 ? FlxTextAlign.CENTER : FlxTextAlign.LEFT;

				textLines[id].setFormat("assets/kanit-regular.ttf", sz, myColor, myAlign, FlxTextBorderStyle.NONE, 0xFF000000, true);

				textLines[id].text = lines[id];

				textLines[id].x = myX + (myAlign==FlxTextAlign.CENTER?-38:0);

				textLines[id].y = myY;

				myY += 20 + (sz-defaultSize);

				this.add(textLines[id]);
			} else {
				textLines[id].text = "";
				myY += 20;
			}
		}
	}



}
