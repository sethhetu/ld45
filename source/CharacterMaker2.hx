package;

// Used for constructing a character for each story element
class CharacterMaker2 
{
	// Our first character
	public static function Francis_Story1():Character
	{
		var res = new Character();
		res.name = "Francis";
		res.age = 30;
		res.pnhe = "he";
		res.pnHe = "He";
		res.pnhis = "his";
		res.pnHis = "His";
		res.pnhim = "him";
		res.pnHim = "Him";

		res.chapter = 2;
		res.level_music = AssetPaths.HardLevel__ogg;

		res.intro_lines = [
			"Francis is a bartender with",
			"mild ADHD. He is talented",
			"with music, and often hangs",
			"out at the bar he works at.",
			"He is living paycheck to",
			"paycheck.",
			"",
			"Francis is trying to save money to",
			"go back to music school. He would like",
			"to get a job teaching music.",
			"",
			"Francis is at a medium risk of suicide,",
			"due to his mental illness, combined",
			"with his tough financial circumstances.",
			"Help him collect enough money to pay",
			"his bills, and watch his risk level.",
		];

		res.scenic_png = AssetPaths.francis1_scenic__png;

		res.short_desc = [
			"Francis is a 30 year old man with a steady job",
			"bartending and mild ADHD. He also enjoys",
			"playing the guitar. Francis is struggling",
			"to pay off his bills."
		];

		res.face_anim = "char2_neutral";

		res.goal = "Pay Bills";
		res.goal_anim = "coin";
		res.goal_description = [
			"Francis has to pay off his bills, or else",
			"he will go into debt. Living one paycheck",
			"to the next is causing him to become",
			"anxious; he drinks and plays guitar to",
			"cope with this.",
			"To complete this chapter, collect 12",
			"coins, each of which represent a month's",
			"salary."
		];

		res.duration = "the next paycheck";

		res.ball_advance_modulo = 2;

		res.reRollChance = 20;

		res.reRollTimes = 1;

		res.lifelines = [ 1,1,1,1 ];

		res.ballOdds = [
			"red" => 50,
			"green" => 50,
			"pink" => 50,
			"yellow" => 50,
		];

		res.ballNames = [
			"red" => "Bartending",
			"green" => "Social Drinking",
			"pink" => "Family Interactions",
			"yellow" => "Guitar Playing",
		];

		res.ball_descriptions = [
			"red" => [
				"Francis loves his job bartending, since",
				"he gets to meet lots of people, and his",
				"ADHD does not interfere with it too much.",
				"The only downside is that his job",
				"tends to play up his anxiousness.",
				"",
				"",
			],

			"green" => [
				"Francis has learned to mix some tasty",
				"cocktails, which help him to wind down",
				"after a stressful day at work.",
				"He usually drinks in moderation, though,",
				"since too much drinking can leave him",
				"feeling depressed",
				"",
			],

			"pink" => [
				"Talking to his family members makes Francis",
				"feel that all is right in the world.",
				"With good family support, he can take",
				"on any challenge --although he still",
				"worries about money.",
				"",
				"",
			],

			"yellow" => [
				"Nothing can calm Francis down quite like",
				"playing the guitar. It's the one thing",
				"in his life that has always \"worked\"",
				"when he is feeling stressed out or just",
				"downright sad. Francis plays the guitar",
				"any chance he can get.",
				"",
			],
		];

		res.goal_descriptions = [
			"Francis's goal is to pay off his",
			"bills as the year progresses.",
			"To do this, he must collect 12",
			"coins, one for each month",
			"",
			"",
			"",
		];

		res.modulate_goal = 10;
		res.goal_frequency = 30;
		res.bad_events = [
			69 => 1,
			170 => 1,
		];

		res.event_count = 0;

		res.event_descriptions = [
			"As the year progresses, Francis",
			"may experience some events that",
			"drastically affect his emotional",
			"well-being. Events may be planned",
			"or spontaneous, and offer a potential",
			"risk or reward.",
			"",
		];

		res.event_titles = [
			"Personal Illness",
			"Family Illness",
		];

		res.event_stories = [
			[
				"One day, through no fault of his",
				"own, Francis has to call in sick",
				"at the last minute. His manager",
				"tries to be compasionate, but",
				"the bar's profits for the night",
				"were directly affected, and the",
				"owner is furious.",
				"Francis's manager informs him that",
				"if he takes another sick day, he",
				"wiill be fired.",
				"",
				"Effect: Suicide Risk increases.",
			],

			[
				"Francis's mother suddenly falls ill",
				"and he is too busy with work to",
				"visit her as often as he'd like",
				"Francis's strong family foundation",
				"feels like it is crumbling, and he",
				"is terrified that he won't be there",
				"when his mother needs him. Francis",
				"also feels extremely guilty that",
				"he has to work so much, even though",
				"he knows it is necessary.",
				"",
				"Effect: Suicide Risk increases.",
			],
		];

		res.nextChara = 3; // TODO

		res.event_is_bad =[true, true];

		res.goal_count = 0;

		res.goal_requirement = 12;

		res.victory_lines = [
			"Francis barely made it,",
			"but in the end he was",
			"able to pay off his",
			"bills and keep",
			"visiting his family.",
			"",
			"Francis's story is over for now, but we",
			"may hear from him again in the future.",
			"",
			"",
			"",
			"",
			"",
			"",
		];

		res.therapist = "therapist";

		res.intro_obj_offset = 0;

		res.loss_lines = [
			"Francis hit a rough patch,",
			"and attempted suicide.",
			"His best friend saw a",
			"worrying post on social",
			"media and rushed over.",
			"",
			"Francis is currently in critical condition,",
			"but the doctors are cautiously optimistic.",
			"",
			"Francis's story is over for now, but there",
			"is still a chance we will see him again.",
			"",
			"",
			"",
		];

		res.ballEffects = [
			"red" => ["\"Worry\" will decrease a lot", "\"Anxious\" will increase a lot"],
			"green" => ["\"Hopeless\" will increase a little", "\"Anxious\" will decrease a little"],
			"pink" => ["\"Hopeless\" will decrease a little", "\"Worry\" will increase a little"],
			"yellow" => ["\"Worry\" will decrease a little", "\"Anxious\" will decrease a little"],
		];

		res.emotions = [
			"Worry",
			"Anxiousness",
			"Hopelessness",
		];

		res.emotionalState = [ 161, 110, 90 ];
		res.riskLevel = 90;

		res.basic_response = [
			"red" => [
				new TurnEffect("emotion", 0, -2),
				new TurnEffect("emotion", 1, 2),
			],

			"green" => [
				new TurnEffect("emotion", 2, 1),
				new TurnEffect("emotion", 1, -1),
			],

			"pink" => [
				new TurnEffect("emotion", 2, -1),
				new TurnEffect("emotion", 0, 1),
			],

			"yellow" => [
				new TurnEffect("emotion", 0, -1),
				new TurnEffect("emotion", 1, -1),
			],
		];

		res.hopeful = false;


		res.properties = ["Talented", "Sociable", "Chronic: ADHD"];
		
		res.property_anims = ["talent", "beer", "coffee"];

		res.property_bonuses = [0, 0, 0];

		// WARNING: Make sure these are all the same length
		res.property_descriptions = [
			[
				"Francis is a talented musician. He",
				"has been playing the guitar for as",
				"long as he can remember.",
				"Effect:",
				"       (Guitar Playing) 2x effect",
				"",
				"",
			],

			[
				"Francis meets a lot of great people",
				"as part of either his bartending job",
				"or his music gigs. He's generally",
				"well regarded, and has made some",
				"close friends.",
				"Effect:",
				"    Extra Lifelines",
			],

			[
				"Francis has chronic Attention-Deficit",
				"Hyperactivity Disorder (ADHD), which",
				"can make it hard for him to focus on",
				"certain tasks. His medication works",
				"well, but he can't always afford it.",
				"Effect:",
				"    Tasks Accumulate Faster",
			],
		];

		res.property_balls = [
			"yellow",
			"",
			"",
		];

		res.who_suggests_help = "His therapist suggests:";
		res.match_recommends = "yellow";
		res.avoid_recommends = "";

		res.suicide_risk = "Moderate";

		return res;
	}

}