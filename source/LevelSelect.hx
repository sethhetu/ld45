package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.math.FlxRect;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.system.FlxSound;
import flixel.tweens.FlxTween;
import flixel.group.FlxSpriteGroup;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;


class LevelSelect extends FlxState
{
    // Our scenic background
    //var bgScenicSpr:FlxSprite;

    // Our backgrounds sprite (overlay on scenic)
    //var bgSpr:FlxSprite;

    // Our title
    var titleSpr:FlxSprite;

    // We only have 1 button
    var btnStart:FlxSprite;
    var btnStartTxt:FlxText;

    // Ok, make that 2
    var btnColorBlind:FlxSprite;
    var btnColorBlindTxt:FlxText;

    // Ok this one also makes sense
    var btnSkipLevel:FlxSprite;
    var btnSkipLevelTxt:FlxText;

    var character:Character;

    // Where do they belong (after tweening, etc).
/*    var btn1X:Int;
    var btn2X:Int;
    var btn3X:Int;
    var btn1XTxt:Int;
    var btn2XTxt:Int;
    var btn3XTxt:Int;
    */

    // Control for them
    var btnDown:Int = 0;


    // The current character window
    var charaWindow:PopupWinCharacter;

    // And of course...
    var mouseSprite:FlxSprite;

    // Disable input?
    var disableInput:Bool = false;

    // Our clicking sound
    var clickSound:FlxSound;

    var firstTurn = true;

    override public function new(?character:Character)
    {
        super();
        if (character==null) {
            this.character = CharacterMaker.Mia_Story1(); // Whatever we're testing these days
        } else {
            this.character = character;
        }
    }

    override public function create():Void
    {
        super.create();

        // Wheeee!!!!
        MultiPlatformUtils.ZoomToMax();

        this.bgColor = FlxColor.fromRGB(0x00, 0x00, 0x00);

        // Make a BG scene
        /*
        bgScenicSpr = new FlxSprite();
        bgScenicSpr.loadGraphic(AssetPaths.title_scenic__png);
        bgScenicSpr.origin.set(0,0);
        this.add(bgScenicSpr);

        // Make a BG sprite
        bgSpr = new FlxSprite();
        bgSpr.loadGraphic(AssetPaths.level_bg__png);
        bgSpr.origin.set(0,0);
        this.add(bgSpr);
        */


        // Chara select window
        charaWindow = new PopupWinCharacter(character);
        charaWindow.x = 540/2 - charaWindow.width/2;
        charaWindow.y = Math.floor(960/2 - charaWindow.height/2);
        this.add(charaWindow);


        // Make our buttons: 1
        btnStart = new FlxSprite();
        btnStart.loadGraphic(AssetPaths.title_buttons_big_2__png, true, 310, 84);
        btnStart.animation.add("up", [0], 5, false);
        btnStart.animation.add("down", [1], 5, false);
        btnStart.animation.play("up");
        var mx = 540/2 - btnStart.width/2;
        var my = 100 + 500 + 80 - 5;
        btnStart.x = mx;
        btnStart.y = my;
        this.add(btnStart);
        //
        btnStartTxt = new FlxText();
        btnStartTxt.x = mx + 18 - 2;
        btnStartTxt.y = my + 21 - 32/2 - 16 + 7 + 2;
        btnStartTxt.setFormat("assets/kanit-regular.ttf", 52, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnStartTxt.text = 'Begin Story';
        this.add(btnStartTxt);

        // Make our buttons: 2
        btnColorBlind = new FlxSprite();
        btnColorBlind.loadGraphic(AssetPaths.title_buttons_small_3__png, true, 275, 52);
        btnColorBlind.animation.add("up", [0], 5, false);
        btnColorBlind.animation.add("down", [1], 5, false);
        btnColorBlind.animation.play("up");
        var mx = 16;
        var my = 16;
        btnColorBlind.x = mx;
        btnColorBlind.y = my;
        this.add(btnColorBlind);
        //
        btnColorBlindTxt = new FlxText();
        btnColorBlindTxt.x = mx + 18 - 2;
        btnColorBlindTxt.y = my + 21 - 24/2 - 16 + 7 + 2 + 3;
        btnColorBlindTxt.setFormat("assets/kanit-regular.ttf", 24, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnColorBlindTxt.text = 'Color Blind Mode: ' + (Ball.ColorDiscrepancy ? "ON" : "Off");
        this.add(btnColorBlindTxt);

        // Again!
        btnSkipLevel = new FlxSprite();
        btnSkipLevel.loadGraphic(AssetPaths.title_buttons_small_4__png, true, 210, 52);
        btnSkipLevel.animation.add("up", [0], 5, false);
        btnSkipLevel.animation.add("down", [1], 5, false);
        btnSkipLevel.animation.play("up");
        var mx = 540 - 16 - btnSkipLevel.width;
        var my = 16;
        btnSkipLevel.x = mx;
        btnSkipLevel.y = my;
        this.add(btnSkipLevel);
        //
        btnSkipLevelTxt = new FlxText();
        btnSkipLevelTxt.x = mx + 18 - 2;
        btnSkipLevelTxt.y = my + 21 - 24/2 - 16 + 7 + 2 + 3;
        btnSkipLevelTxt.setFormat("assets/kanit-regular.ttf", 24, 0xFF000000, FlxTextAlign.LEFT, FlxTextBorderStyle.NONE, 0xFF000000, true);
        btnSkipLevelTxt.text = 'Skip This Level';
        this.add(btnSkipLevelTxt);

        // Special case
        if (character.nextChara == 4) {
            btnSkipLevel.visible = false;
            btnSkipLevelTxt.visible = false;
        }


        // When the game is done
        if (character.name == "DONE") {
            btnStart.visible = false;
            btnStartTxt.visible = false;
            btnColorBlind.visible = false;
            btnColorBlindTxt.visible = false;
            btnSkipLevel.visible = false;
            btnSkipLevelTxt.visible = false;
            FlxG.sound.playMusic(AssetPaths.Title__ogg, 1, true);
        }

        // Mouse always goes on top
        mouseSprite = new FlxSprite();
        MultiPlatformUtils.InitMouse(mouseSprite);
        this.add(mouseSprite);

        // Load our sound effects
        clickSound = FlxG.sound.load(AssetPaths.press_button__ogg);

        // Start/restart music.
        //FlxG.sound.playMusic(AssetPaths.Title__ogg, 1, true);
    }


    override public function update(elapsed:Float):Void
    {
        if (firstTurn) {
            firstTurn = false;
            MultiPlatformUtils.CenterFullscreenMobile(this);
        }

    
        this.handleMouseInput();
        super.update(elapsed);
    }

    private function handleMouseInput():Void
    {
        var inputStuff = MultiPlatformUtils.ComputePseudoMouse();

        var mousePos:FlxPoint = inputStuff.mousePos;
        var wasPressed:Bool = inputStuff.justPressed;
        var wasReleased:Bool = inputStuff.justReleased;

        // Avoid nulls; just put the mouse offscreen in that case and be done with it.
        if (mousePos == null) {
                mouseSprite.setPosition(-999,-999);
                return;
        }

        // Keep the mouse in line.
        mouseSprite.setPosition(mousePos.x-19, mousePos.y-19);

        // Skip the rest if input is disabled
        // TODO: It seems that mouse input should really only be disabled on "release".
        if (disableInput) { return; }


        if (wasPressed) {
            // Check our buttons
            if (btnStart.visible && PlayState.rectangleCursorOverlay(btnStart, mousePos, 2)) {
                btnDown = 1;
                btnStart.animation.play("down");
                btnStartTxt.scale.set(0.9, 0.9);
                btnStartTxt.alpha = 0.5;
            } else if (btnColorBlind.visible && PlayState.rectangleCursorOverlay(btnColorBlind, mousePos, 2)) {
                btnDown = 2;
                btnColorBlind.animation.play("down");
                btnColorBlindTxt.scale.set(0.9, 0.9);
                btnColorBlindTxt.alpha = 0.5;
            } else if (btnSkipLevel.visible && PlayState.rectangleCursorOverlay(btnSkipLevel, mousePos, 2)) {
                btnDown = 3;
                btnSkipLevel.animation.play("down");
                btnSkipLevelTxt.scale.set(0.9, 0.9);
                btnSkipLevelTxt.alpha = 0.5;
            } else {
                btnDown = 0;
            }

            // Did we just click a button?
            if (btnDown != 0) {
                clickSound.play();
            }

        }

        if (wasReleased) {
            // Make sure we're still over the button on release.
            if (btnDown == 1) {
                btnStart.animation.play("up");
                btnStartTxt.scale.set(1, 1);
                btnStartTxt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnStart, mousePos, 2)) {
                    showStartGame();
                }
            } else if (btnDown == 2) {
                btnColorBlind.animation.play("up");
                btnColorBlindTxt.scale.set(1, 1);
                btnColorBlindTxt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnColorBlind, mousePos, 2)) {
                    Ball.ColorDiscrepancy = !Ball.ColorDiscrepancy;
                    btnColorBlindTxt.text = 'Color Blind Mode: ' + (Ball.ColorDiscrepancy ? "ON" : "Off");
                }
            } else if (btnDown == 3) {
                btnSkipLevel.animation.play("up");
                btnSkipLevelTxt.scale.set(1, 1);
                btnSkipLevelTxt.alpha = 1;

                if (PlayState.rectangleCursorOverlay(btnSkipLevel, mousePos, 2)) {
                    showNextLevel();
                }
            }


            btnDown = 0;
        }
    }




    // Helper: Start the game
    private function showStartGame()
    {
        disableInput = true;

        // Fade out our backgrounds/titles
        FlxTween.tween(charaWindow, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT, onComplete: showStartGameAnimDone });
        FlxTween.tween(btnStart, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnStartTxt, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnColorBlind, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnColorBlindTxt, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnSkipLevel, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnSkipLevelTxt, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });

        
    }

    private function showStartGameAnimDone(tween:FlxTween):Void
    {
        // Kind of dumb
        FlxTween.tween(charaWindow, { alpha: 0.0 }, 0.3, { type: FlxTweenType.ONESHOT, onComplete: showStartGameAnimDoneReally});
    }

    private function showStartGameAnimDoneReally(tween:FlxTween):Void
    {
        FlxG.switchState(new PlayState(this.character));
    }



    private function showNextLevel()
    {
        disableInput = true;

        // Fade out our backgrounds/titles
        FlxTween.tween(charaWindow, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT, onComplete: showNextLevelAnimDone });
        FlxTween.tween(btnStart, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnStartTxt, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnColorBlind, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnColorBlindTxt, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnSkipLevel, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });
        FlxTween.tween(btnSkipLevelTxt, { alpha: 0.0 }, 1.0, { type: FlxTweenType.ONESHOT });

        
    }
    

    private function showNextLevelAnimDone(tween:FlxTween):Void
    {
        // Kind of dumb
        FlxTween.tween(charaWindow, { alpha: 0.0 }, 0.3, { type: FlxTweenType.ONESHOT, onComplete: showNextLevelAnimDoneReally});
    }

    private function showNextLevelAnimDoneReally(tween:FlxTween):Void
    {
        // NOTE: This is now in 2 places
        if (character.nextChara == 1) {
            FlxG.switchState(new LevelSelect(CharacterMaker.Mia_Story1()));
        } else if (character.nextChara == 2) {
            FlxG.switchState(new LevelSelect(CharacterMaker2.Francis_Story1()));
        } else if (character.nextChara == 3) {
            FlxG.switchState(new LevelSelect(CharacterMaker3.Mia_Story2()));
        } else {
            FlxG.switchState(new LevelSelect(CharacterMaker.GameDone()));
        }
    }



}


