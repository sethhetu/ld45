package;

import flixel.FlxG;
import flixel.math.FlxPoint;
import flixel.FlxSprite;
import flixel.FlxState;



// Return type
class PseudoMouse {
	public var mousePos:FlxPoint = null;
	public var justPressed:Bool = false;
	public var justReleased:Bool = false;
	public function new() {}
}



// Useful class for dealing with multiplatform concerns like
//   zoom and mouse vs. touch events.
class MultiPlatformUtils
{
	// Set up our mouse for android vs. desktop
	public static function InitMouse(mouseSprite:FlxSprite):Void
	{
#if FLX_NO_MOUSE
#else
        FlxG.mouse.visible = false;
#end
        
        mouseSprite.loadGraphic(AssetPaths.crosshair__png, true, 64, 64);

#if FLX_NO_MOUSE
        mouseSprite.animation.add("mouse_help", [7], 5, false);
        mouseSprite.animation.add("mouse", [7], 5, false);
        mouseSprite.animation.add("mouse_grab", [7], 5, false);
        mouseSprite.animation.add("mouse_help_info", [7], 5, false);
        mouseSprite.animation.add("mouse_url", [7], 5, false);
#else
        mouseSprite.animation.add("mouse_help", [2], 5, false);
        mouseSprite.animation.add("mouse", [3], 5, false);
        mouseSprite.animation.add("mouse_grab", [4], 5, false);
        mouseSprite.animation.add("mouse_help_info", [5], 5, false);
        mouseSprite.animation.add("mouse_url", [6], 5, false);
#end

        mouseSprite.animation.play("mouse");
	}

	// Set the zoom mode correctly based on the device properties.
	public static function ZoomToMax():Void
	{
#if FORCE_2X_ZOOM
		var myZoom = Math.floor(Math.min(
			Math.floor(FlxG.stage.stageWidth/540),
			Math.floor(FlxG.stage.stageHeight/960)
		));

		FlxG.camera.zoom = myZoom;          // Do this first.
		FlxG.camera.setPosition(0,0);       // This should have no effect. For some reason, though...
		FlxG.camera.scroll.set(-540/myZoom,-960/myZoom);  // Change what the camera is looking at
		trace('Forcing zoom to: ${myZoom}x');
#end
	}


	// Helper
	private static function MakeBlackRectanlge(state:FlxState, x:Float, y:Float, w:Int, h:Int)
	{
		var res = new FlxSprite();
		res.origin.set(0,0);
		res.makeGraphic(w, h, 0xFF000000);
		res.setPosition(x, y);
		state.add(res);
		return res;
	}


	// Move the camera and set black bars if we're not 100% fullscreen on mobile
	public static function CenterFullscreenMobile(state:FlxState):Void
	{
#if FORCE_2X_ZOOM
		// Compute the width of the border rectangles, if applicable. (Half the distance modified by the zoom.)
		var bordWidth = Math.floor((FlxG.stage.stageWidth - FlxG.camera.zoom*540)/2 / FlxG.camera.zoom);
		if (bordWidth >= 2) {
			var fullHeight = Math.floor(FlxG.stage.stageHeight/FlxG.camera.zoom) + 2;
			FlxG.camera.x = FlxG.camera.zoom*bordWidth;
			FlxG.camera.scroll.set(FlxG.camera.scroll.x-bordWidth, FlxG.camera.scroll.y);
			MakeBlackRectanlge(state, -bordWidth-2, 0, bordWidth+2, fullHeight);
			MakeBlackRectanlge(state, 540, 0, bordWidth+2, fullHeight);
		}

		var bordHeight = Math.floor((FlxG.stage.stageHeight - FlxG.camera.zoom*960)/2 / FlxG.camera.zoom);
		if (bordHeight >= 2) {
			var fullWidth = Math.floor(FlxG.stage.stageWidth/FlxG.camera.zoom) + 2;
			FlxG.camera.y = FlxG.camera.zoom*bordHeight;
			FlxG.camera.scroll.set(FlxG.camera.scroll.x, FlxG.camera.scroll.y-bordHeight);
			MakeBlackRectanlge(state, 0, -bordHeight-2, fullWidth, bordHeight+2);
			MakeBlackRectanlge(state, 0, 960, fullWidth, bordHeight+2);
		}
#end
	}
	

	// Compute inputs as if there was a mouse involved
	public static function ComputePseudoMouse():PseudoMouse
	{
		var res = new PseudoMouse();

#if FLX_NO_MOUSE
#if FLX_NO_TOUCH
#else
 	       // We only respond to the first touch for now, since
	        // there seems to be no need to multi-touch.
	        // Note that touches are consistent; if you press, press, release, the "first" press is still held
	        for (touch in FlxG.touches.list) {
	                // These are world coords; should be fine.
	                res.mousePos = new FlxPoint(touch.x, touch.y);

        	        // These are easier
	                if (touch.justPressed) {
	                        res.justPressed = true;
	                } else if (touch.justReleased) {
	                        res.justReleased = true;
	                }
	                break;
	        }
#end

#else
	        res.mousePos = FlxG.mouse.getWorldPosition();
	        res.justPressed = FlxG.mouse.justPressed;
	        res.justReleased = FlxG.mouse.justReleased;
#end
		return res;
	}


}




