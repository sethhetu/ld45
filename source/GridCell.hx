package;

import flixel.math.FlxRect;

// Simple class for holding "stuff that's in a grid cell"
class GridCell 
{
    public var rect:FlxRect;
    public var ball:Ball;

    public function new(rect:FlxRect)
    {
        this.rect = rect;
        this.ball = null;
    }
}


